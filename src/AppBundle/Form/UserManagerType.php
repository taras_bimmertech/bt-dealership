<?php

namespace AppBundle\Form;

use libphonenumber\PhoneNumberFormat;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserManagerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array(
                'label' => 'Name',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Name'
                )
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Email',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Email'
                )
            ))
            ->add('phoneNumber', TextType::class, array(
                'label' => 'Phone number',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Phone number'
                )
            ))
            ->add('password', PasswordType::class, array(
                'invalid_message' => 'The password fields must match.',
                'required' => true,
                'label' => 'Password',
                'attr' => array(
                    'placeholder' => 'Password',
                    'class' => 'form-control required'
                )
            ))
            ->add('file', FileType::class, array(
                'label' => ' ',
                'required' => false,
                'attr' => array(
                    'accept' => 'image/png, image/jpeg, image/gif',
                    'class' => 'photo'
                )
            ));


//            ->add('phoneNumber', PhoneNumberType::class, array(
//                'default_region' => 'PL',
//                'format' => PhoneNumberFormat::INTERNATIONAL
//            ));


    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\UserManager'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_usermanager';
    }


}
