<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Post;
use AppBundle\Form\PostType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller used to manage blog contents in the backend.
 *
 * Please note that the application backend is developed manually for learning
 * purposes. However, in your real Symfony application you should use any of the
 * existing bundles that let you generate ready-to-use backends without effort.
 *
 * See http://knpbundles.com/keyword/admin
 *
 * @Route("/admin/post")
 * @Security("has_role('ROLE_ADMIN')")
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class BlogController extends Controller
{
    /**
     * Lists all Post entities.
     *
     * This controller responds to two different routes with the same URL:
     *   * 'admin_post_index' is the route with a name that follows the same
     *     structure as the rest of the controllers of this class.
     *   * 'admin_index' is a nice shortcut to the backend homepage. This allows
     *     to create simpler links in the templates. Moreover, in the future we
     *     could move this annotation to any other controller while maintaining
     *     the route name and therefore, without breaking any existing link.
     *
     * @Route("/", name="admin_index")
     * @Route("/", name="admin_post_index")
     */
    public function indexAction(Request $request)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $helper = $this->get('app_helper');
        $userEnter = $this->getUser();
        $editForm = $helper->getUserFormAction($request, $this);
        $formPass = $helper->getUserPassformAction($request, $this);

        $posts = $entityManager->getRepository(Post::class)->findBy([], ['publishedAt' => 'DESC']);

        return $this->render('admin/blog/index.html.twig',
            ['posts' => $posts,
                'user' => $userEnter,
                'edit_form' => $editForm->createView(),
                'form_pass' => $formPass->createView()
            ]);
    }

    /**
     * Creates a new Post entity.
     *
     * @Route("/new", name="admin_post_new")
     * @Method({"GET", "POST"})
     *
     */
    public function newAction(Request $request)
    {
        $post = new Post();
        $entityManager = $this->getDoctrine()->getManager();
        $helper = $this->get('app_helper');
        $userEnter = $this->getUser();
        $editForm = $helper->getUserFormAction($request, $this);
        $formPass = $helper->getUserPassformAction($request, $this);
        $form = $this->createForm('AppBundle\Form\PostType', $post);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $post->setSlug($this->get('slugger')->slugify($post->getTitle()));
            if (isset($form['file'])) {
                $postFoto = $form['file']->getData();
                $post->setFile($postFoto);
                $post->preUpload();
                $post->upload();
//
            }
            $summary = $form['summary']->getData();
            $post->setSummary($summary);
            $category = $form['category']->getData();
            $post->setCategory($category);
            $author = $form['author']->getData();
            $post->setAuthor($author);
            $entityManager->persist($post);
            $entityManager->flush();

            // Flash messages are used to notify the user about the result of the
            // actions. They are deleted automatically from the session as soon
            // as they are accessed.

//            $this->addFlash('success', 'post.created_successfully');

//            if ($form->get('saveAndCreateNew')->isClicked()) {
//                return $this->redirectToRoute('admin_post_new');
//            }

            return $this->redirectToRoute('admin_post_index');
        }

        return $this->render('admin/blog/new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
            'user' => $userEnter,
            'edit_form' => $editForm->createView(),
            'form_pass' => $formPass->createView()
        ]);
    }

    /**
     * Finds and displays a Post entity.
     *
     * @Route("/{id}", requirements={"id": "\d+"}, name="admin_post_show")
     * @Method("GET")
     */
    public function showAction(Post $post, Request $request)
    {
        // This security check can also be performed
        // using an annotation: @Security("is_granted('show', post)")
        $back = $request->headers->get('referer');
        $helper = $this->get('app_helper');
        $userEnter = $this->getUser();
        $editForm = $helper->getUserFormAction($request, $this);
        $formPass = $helper->getUserPassformAction($request, $this);
        $this->denyAccessUnlessGranted('show', $post, 'Posts can only be shown to their authors.');

        return $this->render('admin/blog/show.html.twig', [
            'post' => $post,
            'back' => $back,
            'user' => $userEnter,
            'edit_form' => $editForm->createView(),
            'form_pass' => $formPass->createView()
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", requirements={"id": "\d+"}, name="admin_post_edit")
     */
    public function editAction(Post $post, Request $request)
    {
        $this->denyAccessUnlessGranted('edit', $post, 'Posts can only be edited by their authors.');
        $entityManager = $this->getDoctrine()->getManager();
        $helper = $this->get('app_helper');
        $userEnter = $this->getUser();
        $editForm = $helper->getUserFormAction($request, $this);
        $formPass = $helper->getUserPassformAction($request, $this);
//        $form = $this->createForm(PostType::class, $post);
        $form = $this->createForm('AppBundle\Form\PostEditType', $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post->setSlug($this->get('slugger')->slugify($post->getTitle()));


//            if (isset($form['file'])) {
            if ($postFoto = $form['file']->getData()) {
                $post->setFile($postFoto);
                $post->preUpload();
                $post->upload();

            }

            $entityManager->persist($post);
            $entityManager->flush();
            $this->addFlash('success', 'post.updated_successfully');

            return $this->redirectToRoute('admin_post_edit', ['id' => $post->getId()]);
        }

        return $this->render('admin/blog/edit.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
            'user' => $userEnter,
            'edit_form' => $editForm->createView(),
            'form_pass' => $formPass->createView()
        ]);
    }

    /**
     * Deletes a Post entity.
     *
     * @Route("/{id}/delete", name="admin_post_delete")
     * @Method("POST")
     * @Security("is_granted('delete', post)")
     *
     * The Security annotation value is an expression (if it evaluates to false,
     * the authorization mechanism will prevent the user accessing this resource).
     */
    public function deleteAction(Request $request, Post $post)
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('admin_post_index');
        }

        $entityManager = $this->getDoctrine()->getManager();

        // Delete the tags associated with this blog post. This is done automatically
        // by Doctrine, except for SQLite (the database used in this application)
        // because foreign key support is not enabled by default in SQLite


        $entityManager->remove($post);
        $entityManager->flush();

        $this->addFlash('success', 'post.deleted_successfully');

        return $this->redirectToRoute('admin_post_index');
    }


    /**
     * @param Request $request
     * @Route("/post/delete/{id}", name="admin_post_delete_by_id")
     * @return JsonResponse
     */
    public function deleteAjaxAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('AppBundle:Post')->find($id);
        $post->removeUpload();
        $em->remove($post);
        $em->flush();
//        $downloadFile->removeUpload();
        return new JsonResponse('success delete');
    }
}
