<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Status
 *
 * @ORM\Table(name="status")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StatusRepository")
 */
class Status
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Discount", mappedBy="status", cascade={"persist","remove"})
     */
    private $discount;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Benefit", mappedBy="status",cascade={"persist"})
     */
    private $benefit;


    public function __construct($name)
    {
        $this->name = $name;
        $this->discount = new ArrayCollection();
        $this->benefit = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Status
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }


    /**
     * Add discount
     *
     * @param \AppBundle\Entity\Discount $discount
     *
     * @return Status
     */
    public function addDiscount(Discount $discount)
    {
        $this->discount[] = $discount;

        return $this;
    }

    /**
     * Remove discount
     *
     * @param \AppBundle\Entity\Discount $discount
     */
    public function removeDiscount(Discount $discount)
    {
        $this->discount->removeElement($discount);
    }


    /**
     * @return mixed
     */
    public function getBenefit()
    {
        return $this->benefit;
    }


    /**
     * @param mixed $benefit
     */
    public function setBenefit($benefit)
    {
        $this->benefit = $benefit;
    }


    /**
     * Add benefit
     *
     * @param \AppBundle\Entity\Benefit $benefit
     *
     * @return Status
     */
    public function addBenefit(Benefit $benefit)
    {
        $this->benefit[] = $benefit;

        return $this;
    }

    /**
     * Remove benefit
     *
     * @param \AppBundle\Entity\Benefit $benefit
     */
    public function removeBenefit(Benefit $benefit)
    {
        $this->benefit->removeElement($benefit);
    }
}

