<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class ResetPasswordType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('oldPassword', PasswordType::class, array(
            'invalid_message' => 'The password fields must match.',
            'required' => false,
            'label' => 'Old password',
            'attr' => array(
//                'placeholder' => 'Old password'
            )

        ));
        $builder->add('plainPassword', RepeatedType::class, array(
            'type' => PasswordType::class,
            'invalid_message' => 'validation.message.resetting.not_match',
            'options' => array('attr' => array('class' => 'password-field')),
            'required' => true,
            'first_options' => array('label' =>  'New password',
                'attr' => array(
//                    'placeholder' => 'New password'
                )),
            'second_options' => array('label' => 'Repeat new password',
                'attr' => array(
//                    'placeholder' => 'Repeat new password'
                )
            ),
        ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
        ));
    }

    public function getName()
    {
        return 'reset_password';
    }
}