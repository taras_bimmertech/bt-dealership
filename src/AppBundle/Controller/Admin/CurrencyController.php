<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Currency;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Currency controller.
 * @Security("has_role('ROLE_ADMIN')")
 * @Route("setting/currency")
 */
class CurrencyController extends Controller
{
    /**
     * Lists all currency entities.
     *
     * @Route("/", name="setting_currency_index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('app_helper');
        $userEnter = $this->getUser();
        $editForm = $helper->getUserFormAction($request, $this);
        $formPass = $helper->getUserPassformAction($request, $this);
        $currencies = $em->getRepository('AppBundle:Currency')->findAll();

        return $this->render('currency/index.html.twig', array(
            'currencies' => $currencies,
            'user' => $userEnter,
            'edit_form' => $editForm->createView(),
            'form_pass' => $formPass->createView()
        ));
    }

    /**
     * Creates a new currency entity.
     *
     * @Route("/new", name="setting_currency_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {

        $helper = $this->get('app_helper');
        $userEnter = $this->getUser();
        $editForm = $helper->getUserFormAction($request, $this);
        $formPass = $helper->getUserPassformAction($request, $this);

        $currency = new Currency();
        $form = $this->createForm('AppBundle\Form\CurrencyType', $currency);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($currency);
            $em->flush();

            return $this->redirectToRoute('setting_currency_show', array('id' => $currency->getId()));
        }

        return $this->render('currency/new.html.twig', array(
            'currency' => $currency,
            'form' => $form->createView(),
            'user' => $userEnter,
            'edit_form' => $editForm->createView(),
            'form_pass' => $formPass->createView()
        ));
    }

    /**
     * Finds and displays a currency entity.
     *
     * @Route("/{id}", name="setting_currency_show")

     */
    public function showAction(Currency $currency)
    {

        $helper = $this->get('app_helper');
        $userEnter = $this->getUser();
        $editForm = $helper->getUserFormAction($request, $this);
        $formPass = $helper->getUserPassformAction($request, $this);
        $deleteForm = $this->createDeleteForm($currency);

        return $this->render('currency/show.html.twig', array(
            'currency' => $currency,
            'delete_form' => $deleteForm->createView(),
            'user' => $userEnter,
            'edit_form' => $editForm->createView(),
            'form_pass' => $formPass->createView()
        ));
    }

    /**
     * Displays a form to edit an existing currency entity.
     *
     * @Route("/{id}/edit", name="setting_currency_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Currency $currency)
    {
        $deleteForm = $this->createDeleteForm($currency);
        $editForm = $this->createForm('AppBundle\Form\CurrencyType', $currency);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('setting_currency_edit', array('id' => $currency->getId()));
        }

        return $this->render('currency/edit.html.twig', array(
            'currency' => $currency,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a currency entity.
     *
     * @Route("/{id}", name="setting_currency_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Currency $currency)
    {
        $form = $this->createDeleteForm($currency);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($currency);
            $em->flush();
        }

        return $this->redirectToRoute('setting_currency_index');
    }

    /**
     * Creates a form to delete a currency entity.
     *
     * @param Currency $currency The currency entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Currency $currency)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('setting_currency_delete', array('id' => $currency->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
