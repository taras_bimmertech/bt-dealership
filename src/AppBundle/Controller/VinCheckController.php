<?php


namespace AppBundle\Controller;

use AppBundle\Entity\Discount;
use AppBundle\Entity\Status;
use AppBundle\Entity\User;
use AppBundle\Entity\UserDealer;
use AppBundle\Form\ResetPasswordType;
use AppBundle\Utils\LoggerMessage;
use AppBundle\Utils\SendNotification;
use AppBundle\Utils\Util;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @property  container
 * @Route("/vincheck")
 * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER') or has_role('ROLE_USER') ")
 */
class VinCheckController extends Controller
{

    /**
     * @Route("/", name="vincheck_index")
     */
    public function indexAction(Request $request)
    {

        $userEnter = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $currentCurrency = ['name' => "US Dollar", 'code' => 'USD'];
        $exchangeFee = 0;
        if ($userEnter->getRoles()[0] === 'ROLE_USER') {
            /** @var UserDealer $user */
            $user = $em->getRepository('AppBundle:UserDealer')->find($userEnter->getId());
            if ($user->getCurrency()) {
                $currentCurrencyCode = ($user->getCurrency()->getCode()) ? $user->getCurrency()->getCode() : $currentCurrency['code'];
                $currentCurrencyName = ($user->getCurrency()->getName()) ? $user->getCurrency()->getName() : $currentCurrency['name'];
                $currentCurrency = ['name' => $currentCurrencyName, 'code' => $currentCurrencyCode];
            }
            if ($user->getExchangeFee()) {
                $exchangeFee = ($user->getExchangeFee()) ? $user->getExchangeFee() : $exchangeFee;
            }
            $editForm = $this->createForm('AppBundle\Form\UserDealerType', $user);
        } else if ($userEnter->getRoles()[0] === 'ROLE_MANAGER') {
            $editForm = $this->createForm('AppBundle\Form\EditUserManagerWithFotoType', $userEnter);
        } else {
            $editForm = $this->createForm('AppBundle\Form\UserType', $userEnter);
        }

        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if ($userEnter->getRoles()[0] === 'ROLE_USER') {
                $sendNotification = new SendNotification();
                /** @var User $admin */
                $admin = $em->getRepository('AppBundle:User')->findByRole('["ROLE_ADMIN"]');
                $notification = $sendNotification->send($admin[0]->getNotificationToken(), $admin[0]->getId(), $user->getCompany(), 'Profile information has been updated.');
                $notification->setUsers($admin[0]);
                $em->persist($notification);
            }
            $em->persist($userEnter);
            $em->flush();
            return $this->redirectToRoute('vincheck_index');
        }
        $form = $this->createForm(ResetPasswordType::class, $userEnter);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $oldPassword = $userEnter->getOldPassword();
            $encoder = $this->get('security.password_encoder');
            if (!$encoder->isPasswordValid($userEnter, $oldPassword)) {
                return new JsonResponse('The old password you have given is incorrect. Please try again.');
            }
            $em = $this->getDoctrine()->getManager();
            $encoder = $this->get('security.password_encoder');
            $userEnter->setPassword($encoder->encodePassword($userEnter, $userEnter->getPlainPassword()));
            $em->persist($userEnter);
            $em->flush();
            return new JsonResponse('Your password has been successfully changed.');
        }

        $coupons = $em->getRepository('AppBundle:Coupon')->findAll();
        $currencies = $em->getRepository('AppBundle:Currency')->findAll();
        return $this->render('vincheck/index.html.twig', array(
            'user' => $userEnter,
            'edit_form' => $editForm->createView(),
            'form_pass' => $form->createView(),
            'coupons' => $coupons,
            'currencies' => $currencies,
            'currentCurrency' => $currentCurrency,
            'exchangeFee' => $exchangeFee
        ));
    }


    /**
     *
     * @Route("/check/products/vin", name="vincheck_product_vin")
     */
    public function checkProductsAction(Request $request)
    {
        $vin = $request->get('vin');
        $company = $request->get('company');
        $util = new Util();
        $url = $this->container->getParameter('url_for_product');
        $key = $this->container->getParameter('product_key');
        $vincheckKey = ["key: " . $key];
        $vincheckVin = ["vin" => $vin, 'service' => 'Dealer Portal products'];
        $sendReques = $util->HTTPQuery($url, $vincheckVin, $vincheckKey);
        $loger = LoggerMessage::getInstance();
        $loger->logVinCheck('VinCheck: ', [$company, $vin]);
        return new JsonResponse($sendReques);
    }

    /**
     *
     * @Route("/check/mmi/vin", name="vincheck_mmi_vin")
     */
    public function checkMMIAction(Request $request)
    {
        $vin = $request->get('vin');

        $util = new Util();
        $url = $this->container->getParameter('url_for_mmi');
        $key = $this->container->getParameter('mmi_key');
        $productId = $this->container->getParameter('mmi_product');
        $vincheckKey = ["key: " . $key];
        $vincheckVin = ["vin" => $vin, "productId" => $productId, 'service' => 'Dealer Portal MMI'];
        $sendReques = $util->HTTPQuery($url, $vincheckVin, $vincheckKey);


        return new JsonResponse($sendReques);
    }

    /**
     *
     * @Route("/get/currency", name="vincheck_get_currency")
     */
    public function getCurrencyAction(Request $request)
    {
        $eurRate = $this->get('session')->get('eurRate');
        if (!$eurRate) {
            $util = new Util();
            $exchangeUrl = 'http://api.fixer.io/latest?base=USD';
            $sendReques = $util->HTTPQuery($exchangeUrl);
            $sendReques = json_decode($sendReques, true);
            $eurRate = $sendReques['rates']['EUR'];
            $this->get('session')->set('eurRate', $eurRate);
        }
        return new JsonResponse($eurRate);
    }


    /**
     *
     * @Route("/send/mistake", name="vincheck_send_mistake")
     */
    public function sendMistakeAction(Request $request)
    {
        $vin = $request->get('vin');
        $message = $request->get('message');
        $name = $request->get('name');
        $mistakeEmail = $this->container->getParameter('mistake_email');
        /** @var UserDealer $user */
        $user = $this->getUser();
        $roles = $user->getRoles();
        $replyMail = $user->getEmail();
        try {
            $this->get('app_mailer')->sendMistake($mistakeEmail, $name, $vin, $message, $replyMail);
            if ($roles[0] === "ROLE_USER") {
                $managerEmail = $user->getUserManager()->getEmail();
                $this->get('app_mailer')->sendMistake($managerEmail, $name, $vin, $message, $replyMail);
            }
            return new JsonResponse('Report sent. Thank you for helping us improve this tool.');
        } catch (Exception $e) {
            return new JsonResponse('Error sending report. Please try again.');
        }

    }

    /**
     *
     * @Route("/send/orders", name="vincheck_send_orders")
     */
    public function sendOrdersAction(Request $request)
    {
        $vin = $request->get('vin');
        $products = $request->get('products');
        $comment = $request->get('comment');
        /** @var UserDealer $user */
        $user = $this->getUser();
        $company = $user->getCompany();
        $address = $user->getAddress();
        $email = $user->getEmail();
        $phone = $user->getPhone();
        $toManager = $user->getUserManager()->getEmail();

        $loger = LoggerMessage::getInstance();
        $loger->logOrder('Order: ', [$company, $vin]);
        try {
            $this->get('app_mailer')->sendOrders($email, $company, $address, $email, $phone, $vin, $products, $comment);
            $this->get('app_mailer')->sendOrders($toManager, $company, $address, $email, $phone, $vin, $products, $comment);
            return new JsonResponse('We received your request', 200);
        } catch (Exception $e) {
            return new JsonResponse('Sending error', 500);
        }

    }

    /**
     *
     * @Route("/get/discount", name="vincheck_get_discount")
     */
    public function getDiscountAction(Request $request)
    {
        $discount = $request->get('discount');
        $em = $this->getDoctrine()->getManager();
        /** @var UserDealer $dealer */
        $dealer = $this->getUser();
        $value = null;

        if ($dealer->getRoles()[0] === "ROLE_USER") {
            $dealerStatus = $dealer->getStatus();
            /** @var Status $status */
            $status = $em->getRepository('AppBundle:Status')->findOneBy(array('name' => $dealerStatus));

            $statusDiscounts = $status->getDiscount();
            /** @var Discount $disc */
            foreach ($statusDiscounts as $disc) {
                if ($disc->getVincheckname() == $discount) {
                    $value = $disc->getValue();
                }
            }
        }


        return new JsonResponse($value);
    }

    /**
     *
     * @Route("/log/print", name="vincheck_log_print")
     */
    public function logPrintAction(Request $request)
    {

        $vin = $request->get('vin');
        $company = $request->get('company');

        $loger = LoggerMessage::getInstance();
        $loger->LogPrint('Orders print: ', [$company, $vin]);

        return new JsonResponse('success', 200);
    }
}

