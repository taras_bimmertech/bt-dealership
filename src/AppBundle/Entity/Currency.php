<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Currency
 *
 * @ORM\Table(name="currency")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CurrencyRepository")
 */
class Currency
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=3)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="symbol", type="string", length=3,  nullable=true)
     */
    private $symbol;

    /**
     * @var string
     *
     * @ORM\Column(name="rate", type="float")
     */
    private $rate;

    /**
     * @ORM\Column(name="priority", type="decimal")
     */
    private $priority;


    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserDealer", mappedBy="currency", cascade={"persist","remove"})
     * @Serializer\Exclude
     */
    private $dealer;

    /**
     * Currency constructor.
     *
     */
    public function __construct()
    {
        $this->dealer = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Currency
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Currency
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set symbol
     *
     * @param string $symbol
     *
     * @return Currency
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;

        return $this;
    }

    /**
     * Get symbol
     *
     * @return string
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * @return mixed
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param mixed $rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param mixed $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }


    /**
     * @return mixed
     */
    public
    function getDealer()
    {
        return $this->dealer;
    }


    /**
     * @param mixed $dealer
     */
    public
    function setDealer($dealer)
    {
        $this->dealer = $dealer;
    }


    /**
     * Add dealer
     *
     * @param \AppBundle\Entity\UserDealer $dealer
     *
     * @return UserDealer
     */
    public
    function addDealer(UserDealer $dealer)
    {
        $this->dealer[] = $dealer;

        return $this;
    }

    /**
     * Remove dealer
     *
     * @param \AppBundle\Entity\UserDealer $dealer
     */
    public
    function removeDealer(UserDealer $dealer)
    {
        $this->dealer->removeElement($dealer);
    }


}

