<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;

/**
 * Dealer
 *
 * @ORM\Table(name="user_manager")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserManagerRepository")
 */
class UserManager extends User
{

    /**
     * @ORM\Column(name="phone", type="string")
     */
    protected $phoneNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $path;
    /**
     * @Assert\File(maxSize="6000000000000000",
     *     mimeTypes = {"image/png", "image/jpeg", "image/gif"},
     *      mimeTypesMessage = "Please upload a valid image file format: png, jpg, gif")
     *
     */
    private $file;
    private $temp;

    /**
     * @ORM\OneToMany(targetEntity="UserDealer", mappedBy="userManager", cascade={"persist"})
     * @ORM\OrderBy({"createAt" = "DESC"})
     */
    protected $userDealer;


    public function __construct()
    {
        parent::__construct();
        $this->userDealer = new ArrayCollection();
        $this->roles[] = 'ROLE_MANAGER';
        $this->password = $random = random_int(1, 10);
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param mixed $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return mixed
     */
    public function getUserDealer()
    {
        return $this->userDealer;
    }

    /**
     * @param mixed $userDealer
     */
    public function setUserDealer($userDealer)
    {
        $this->userDealer = $userDealer;
    }


    /**
     * Add userDealer
     *
     * @param \AppBundle\Entity\UserDealer $userDealer
     *
     * @return UserManager
     */
    public function addUserDealer(\AppBundle\Entity\UserDealer $userDealer)
    {
        $this->userDealer[] = $userDealer;

        return $this;
    }

    /**
     * Remove userDealer
     *
     * @param \AppBundle\Entity\UserDealer $userDealer
     */
    public function removeUserDealer(\AppBundle\Entity\UserDealer $userDealer)
    {
        $this->userDealer->removeElement($userDealer);
    }


    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;

    }


    public function getWebPath()
    {
        return null === $this->path
            ? null
            : '/' . $this->getUploadDir() . '/' . $this->path;
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/images';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            // do whatever you want to generate a unique name
            $filename = $this->email;

//            $filename = sha1(uniqid(mt_rand(), true));
            $this->path = $filename . '.' . $this->getFile()->guessExtension();
//            $this->path = $filename ;
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;

        //check if we have an old image path
        if (isset($this->path)) {
            //  store the old name to delete after the update
            $this->temp = $this->path;
            $this->path = null;
        }
//        else {
//            $this->path = 'initial';
//        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }


        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->path);


        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
//            unlink($this->getUploadRootDir() . '/' . $this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->file = null;
    }

    public function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved

        return __DIR__ . '/../../../web/' . $this->getUploadDir();
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        $file = $this->getAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }

    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir() . '/' . $this->path;
    }
}