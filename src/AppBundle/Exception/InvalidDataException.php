<?php


namespace AppBundle\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

class InvalidDataException extends HttpException
{

    /**
     * Constructor.
     *
     * @param string     $message  The internal exception message
     * @param \Exception $previous The previous exception
     * @param int        $code     The internal exception code
     */
    public function __construct($message = 'message.wrong_data', \Exception $previous = null, $code = 0)
    {
        parent::__construct(400, $message, $previous, array(), $code);
    }
}