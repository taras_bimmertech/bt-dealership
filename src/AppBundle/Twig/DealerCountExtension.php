<?php

namespace AppBundle\Twig;

use AppBundle\Entity\UserDealer;
use AppBundle\Entity\UserManager;

class DealerCountExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('dealer_count', array($this, 'dealerCount')),
        );
    }


    public function dealerCount($dealers, UserManager $manager)
    {
        $count = 0;
        /** @var UserDealer $dealer */
        foreach ($dealers as $dealer) {
            if ($dealer->getUserManager() == $manager) {
                $count++;
            }
        }

        return $count;
    }

    public function getName()
    {
        return "dealer_count_extension";
    }

}