<?php


namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *     "user" = "User",
 *     "manager" = "UserManager",
 *     "dealer" = "UserDealer"
 *      })
 */
class User implements UserInterface
{
    const NUMBER_OF_STATUS = ['BRONZE' => 0, 'SILVER' => 3, 'GOLD' => 4, 'PLATINUM' => 7];
    const DISCOUNT_OF_STATUS = ['BRONZE' => 15, 'SILVER' => 20, 'GOLD' => 25, 'PLATINUM' => 30];
    const STATUS_BRONZE = 'BRONZE';
    const STATUS_SILVER = 'SILVER';
    const STATUS_GOLD = 'GOLD';
    const STATUS_PLATINUM = 'PLATINUM';



    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $username;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string" )
     * @Serializer\Exclude
     */
    protected $password;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @var array
     *
     * @ORM\Column(type="json_array")
     * @Serializer\Exclude
     */
    protected $roles = [];


    /**
     * @ORM\Column(name="confirmation_token", type="string", length=255, nullable=true)
     * @Serializer\Exclude
     */
    protected $confirmationToken;

    /**
     * @Serializer\Exclude
     */
    protected $oldPassword;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     * @Serializer\Exclude
     * @var string
     */
    protected $plainPassword;
    /**
     * @ORM\Column(name="notification_token", type="string", nullable=true)
     * @Serializer\Exclude
     */
    protected $notificationToken;


    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Notification", mappedBy="users", cascade={"persist","remove"})
     * @ORM\OrderBy({"createAt" = "DESC"})
     */
    protected $notifications;


    public function __construct()
    {
        $this->notifications = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getNotificationToken()
    {
        return $this->notificationToken;
    }

    /**
     * @param mixed $notificationToken
     */
    public function setNotificationToken($notificationToken)
    {
        $this->notificationToken = $notificationToken;
    }

    public function getId()
    {
        return $this->id;
    }


    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Returns the roles or permissions granted to the user for security.
     */
    public function getRoles()
    {
        $roles = $this->roles;

        // guarantees that a user always has at least one role for security
        if (empty($roles)) {
            $roles[] = 'ROLE_USER';
        }

        return array_unique($roles);
    }

    public function setRoles(array $roles)
    {
        $this->roles = $roles;
    }

    /**
     * @return mixed
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * @param mixed $confirmationToken
     */
    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;
    }

    /**
     * @return mixed
     */
    public function getNotifications()
    {
        return $this->notifications;
    }


    /**
     * @param mixed $notifications
     */
    public function setNotifications($notifications)
    {
        $this->notifications = $notifications;
    }


    /**
     * Add notifications
     *
     * @param \AppBundle\Entity\Notification $notifications
     *
     * @return User
     */
    public function addNotifications(\AppBundle\Entity\Notification $notifications)
    {
        $this->notifications[] = $notifications;

        return $this;
    }

    /**
     * Remove notifications
     *
     * @param \AppBundle\Entity\Notification $notifications
     */
    public function removeNotifications(\AppBundle\Entity\Notification $notifications)
    {
        $this->notifications->removeElement($notifications);
    }


    /**
     * @return string
     *
     */
    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    /**
     * @param string $oldPassword
     *
     * @return User
     */
    public function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;
    }

    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;

        return $this;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * {@inheritdoc}
     */
    public function getSalt()
    {
        // See "Do you need to use a Salt?" at http://symfony.com/doc/current/cookbook/security/entity_provider.html
        // we're using bcrypt in security.yml to encode the password, so
        // the salt value is built-in and you don't have to generate one
    }

    /**
     * Removes sensitive data from the user.
     *
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        // if you had a plainPassword property, you'd nullify it here
        // $this->plainPassword = null;
    }
}
