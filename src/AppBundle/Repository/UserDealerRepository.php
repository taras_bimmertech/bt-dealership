<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * This custom Doctrine repository is empty because so far we don't need any custom
 * method to query for application user information. But it's always a good practice
 * to define a custom repository that will be used when the application grows.
 *
 */
class UserDealerRepository extends EntityRepository
{
    public function findAllTen()
    {
        return $this->findBy(array(), array('createAt' => 'DESC'), 10);
    }

    public function findAll()
    {
        return $this->findBy(array(), array('createAt' => 'DESC'));
    }

    public function findAllByCompany()
    {
        return $this->findBy(array(), array('company' => 'ASC'));
}


    public function findAllManager($manager)
    {
        $query = $this->createQueryBuilder('c')
            ->select('c')
            ->leftJoin('AppBundle:UserDealer', 'co', 'WITH', 'co.company = c.company')
            ->where('c.userManager= :manager')
            ->setParameter('manager', $manager)
            ->orderBy('co.company', 'ASC');

        return $query->getQuery()->getResult();




    }
}
