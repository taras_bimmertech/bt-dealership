<?php

namespace AppBundle\Form;

use libphonenumber\PhoneNumberFormat;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DownloadFileType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'file name'
                )
            ))
            ->add('dealers', EntityType::class, [
                'class' => 'AppBundle:UserDealer',
                'choice_label' => 'company',
                'label' => false,
                'placeholder' => 'Choose dealers',
                'required' => false,
                'empty_data' => null,
                'expanded' => true,
                'multiple' => true,

            ])
            ->add('file', FileType::class, array(
                'label' => false,
                'required' => true,
                'attr' => array(
                    'accept' => 'application/pdf, application/x-pdf',
                    'class' => 'btn-file'
                )
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\DownloadFile'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_download_file';
    }


}
