<?php

namespace AppBundle\Utils;

class UniqueNameGenerator
{

    public static function generate()
    {
        return sha1(uniqid(mt_rand(), true));
    }

}
