<?php


namespace AppBundle\Controller;

use AppBundle\Entity\DealerOrder;
use AppBundle\Entity\DownloadFile;
use AppBundle\Entity\User;
use AppBundle\Entity\UserManager;
use AppBundle\Form\ResetPasswordType;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\UserDealer;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/download")
 * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER') or has_role('ROLE_USER') ")
 */
class DownloadController extends Controller
{

    /**
     * @Route("/admin", name="download_admin_index")
     */
    public function indexAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        if ($user->getRoles()[0] === 'ROLE_USER') {
            /** @var UserDealer $user */
            $user = $em->getRepository('AppBundle:UserDealer')->find($user->getId());
            $files = $user->getFiles();
            $editForm = $this->createForm('AppBundle\Form\UserDealerType', $user);
        } else if ($user->getRoles()[0] === 'ROLE_MANAGER') {
            $files = $em->getRepository('AppBundle:DownloadFile')->findAll();
            $editForm = $this->createForm('AppBundle\Form\EditUserManagerWithFotoType', $user);
        } else {
            $files = $em->getRepository('AppBundle:DownloadFile')->findAll();
            $editForm = $this->createForm('AppBundle\Form\UserType', $user);
        }

        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }

        $downloadFile = new DownloadFile();
        $files_form = $this->createForm('AppBundle\Form\DownloadFileType', $downloadFile);
        $files_form->handleRequest($request);
        if ($files_form->isSubmitted() && $files_form->isValid()) {
            $filesDealer = $files_form['dealers']->getData();
            foreach ($filesDealer as $dealerID) {
                $dealer = $em->getRepository('AppBundle:UserDealer')->find($dealerID);
                $dealer->addFiles($downloadFile);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($downloadFile);
            $em->flush();
            return $this->redirectToRoute('download_admin_index');
        }

        $form = $this->createForm(ResetPasswordType::class, $user);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $oldPassword = $user->getOldPassword();
            $encoder = $this->get('security.password_encoder');
            if (!$encoder->isPasswordValid($user, $oldPassword)) {
                return new JsonResponse('The old password you have given is incorrect. Please try again.');
            }
            $em = $this->getDoctrine()->getManager();
            $encoder = $this->get('security.password_encoder');
            $user->setPassword($encoder->encodePassword($user, $user->getPlainPassword()));
            $em->persist($user);
            $em->flush();
            return new JsonResponse('Your password has been successfully changed.');
        }
        return $this->render('download/index.html.twig', array(
            'user' => $user,
            'files' => $files,
            'edit_form' => $editForm->createView(),
            'form_pass' => $form->createView(),
            'files_form' => $files_form->createView()

        ));
    }

    /**
     * @param Request $request
     * @Route("/admin/file/delete/{id}", name="admin_file_delete_by_id")
     * @return Response
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $downloadFile = $em->getRepository('AppBundle:DownloadFile')->find($id);

        $em->remove($downloadFile);
        $em->flush();
//        $downloadFile->removeUpload();
        return new JsonResponse('success delete');
    }


    /**
     * @Route("/add_new_file", name="download_add_new_file")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER') ")
     * @param Request $request
     * @return Response
     */
    public function addFileAction(Request $request)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        if ($user->getRoles()[0] === 'ROLE_USER') {
            /** @var UserDealer $user */
            $user = $em->getRepository('AppBundle:UserDealer')->find($user->getId());
//            $files = $user->getFiles();
            $editForm = $this->createForm('AppBundle\Form\UserDealerType', $user);
        } else if ($user->getRoles()[0] === 'ROLE_MANAGER') {
//            $files = $em->getRepository('AppBundle:DownloadFile')->findAll();
            $editForm = $this->createForm('AppBundle\Form\EditUserManagerWithFotoType', $user);
        } else {
//            $files = $em->getRepository('AppBundle:DownloadFile')->findAll();
            $editForm = $this->createForm('AppBundle\Form\UserType', $user);
        }

        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }


        $form = $this->createForm(ResetPasswordType::class, $user);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $oldPassword = $user->getOldPassword();
            $encoder = $this->get('security.password_encoder');
            if (!$encoder->isPasswordValid($user, $oldPassword)) {
                return new JsonResponse('The old password you have given is incorrect. Please try again.');
            }
            $em = $this->getDoctrine()->getManager();
            $encoder = $this->get('security.password_encoder');
            $user->setPassword($encoder->encodePassword($user, $user->getPlainPassword()));
            $em->persist($user);
            $em->flush();
            return new JsonResponse('Your password has been successfully changed.');
        }


        return $this->render('download/add_new_file.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'form_pass' => $form->createView(),
        ));
    }


    /**
     * @Route("/add_new_file/dealers", name="download_get_dealers")
     * @param Request $request
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER') ")
     * @return JsonResponse
     */
    public function getDealersAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        if ($user->getRoles()[0] === 'ROLE_MANAGER') {
//            $manager = $em->getRepository('AppBundle:UserManager')->find($user->getId());
//            $dealers = $manager->getUserDealer();
            $dealers = $em->getRepository('AppBundle:UserDealer')->findAllManager($user->getId());
        } else {
            $dealers = $em->getRepository('AppBundle:UserDealer')->findAllByCompany();
        }


        $returnDealers = null;
        /** @var UserDealer $dealer */
        foreach ($dealers as $key => $dealer) {
            $returnDealers[$key]['id'] = $dealer->getId();
            $returnDealers[$key]['dealer'] = $dealer->getCompany();
        }
        return new JsonResponse($returnDealers);
    }

    /**
     * @Route("/add_new_files/dealers", name="download_new_files")
     * @param Request $request
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER') ")
     * @return JsonResponse
     */
    public function newFilesAction(Request $request)
    {
        $data = $request->getContent();

        if (!empty($data)) {
            $params = json_decode($data, true);

        }
        $em = $this->getDoctrine()->getManager();
        foreach ($params as $downloadFile) {
            $dFile = new DownloadFile();
            $fileName = explode(".", $downloadFile['fileName']);
            $fileName = $fileName[0];
            $dFile->setName($fileName);
            $dFile->setPath($downloadFile['fileName']);
            foreach ($downloadFile['dealers'] as $dealerId) {
                $dealer = $em->getRepository('AppBundle:UserDealer')->find($dealerId);
                $dFile->addDealers($dealer);;
                $em->persist($dealer);
            }
            $em->persist($dFile);
            $em->flush();
            rename("uploads/download/files/" . $downloadFile['fileName'], "uploads/download/" . $downloadFile['fileName']);
            if (file_exists("uploads/download/files/thumbnail/" . $downloadFile['fileName'])) {
                unlink("uploads/download/files/thumbnail/" . $downloadFile['fileName']);
            }
        }
        return new JsonResponse(array('message' => 'files uploaded with success'), 200);
    }


    /**
     * @Route("/edi_file/dealers/{id}", name="download_edit_file")
     * @param Request $request
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER') ")
     * @return JsonResponse
     */
    public function editDownloadFileAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $downloadFile = $em->getRepository('AppBundle:DownloadFile')->find($id);
        /** @var User $user */
        $user = $this->getUser();
        $save = $request->request->get('save');
        $dealersAdd = $request->request->get('dealersAdd');
        $dealersRemove = $request->request->get('dealersRemove');

        if ($save and $dealersAdd != '' or $dealersRemove != '') {
            if ($dealersRemove != '') {
                foreach ($dealersRemove as $dealR) {
                    $editDealerR = $em->getRepository('AppBundle:UserDealer')->find($dealR);
                    if ($editDealerR->getFiles()->contains($downloadFile)) {
                        $editDealerR->removeFiles($downloadFile);
                        $em->persist($editDealerR);
                        $em->flush();
                    }
                }
            }

            if ($dealersAdd != '') {
                foreach ($dealersAdd as $dealA) {
                    $editDealerA = $em->getRepository('AppBundle:UserDealer')->find($dealA);
                    if (!$editDealerA->getFiles()->contains($downloadFile)) {
                        $editDealerA->addFiles($downloadFile);
                        $em->persist($editDealerA);
                        $em->flush();
                    }
                }
            }
        }
        $dealers = null;
        if ($user->getRoles()[0] === 'ROLE_MANAGER') {
            /** @var UserManager $manager */
            $dealers = $em->getRepository('AppBundle:UserDealer')->findAllManager($user->getId());
        } else {
            $dealers = $em->getRepository('AppBundle:UserDealer')->findAllByCompany();
        }
        $serializer = $this->get('jms_serializer');
        /** @var ArrayCollection $fileDealers */
        $fileDealers = $downloadFile->getDealers();
        if ($user->getRoles()[0] === 'ROLE_MANAGER') {
            $managerDealerFile = [];
            foreach ($dealers as $dealer) {
                if ($fileDealers->contains($dealer)) {
                    array_push($managerDealerFile, $dealer);
                }
            }
            $fileDealers = $managerDealerFile;
        }
        $fileDealers = $serializer->serialize($fileDealers, 'json');
        $fileDealers = $serializer->deserialize($fileDealers, 'ArrayCollection', 'json');
        $dealers = $serializer->serialize($dealers, 'json');
        $dealers = $serializer->deserialize($dealers, 'ArrayCollection', 'json');

        return new JsonResponse(array('fileDealers' => $fileDealers, 'userDealers' => $dealers), 200);
    }
}
