<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Notification;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;

/**
 * Notification controller.
 * @Route("notification")
 */
class NotificationController extends Controller
{
    /**
     * Lists all Notification entities by id.
     *
     * @Route("/all/{id}", name="notification_index")
     * @Method("GET")
     */
    public function indexAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('AppBundle:User')->find($id);

        $notifications = $users->getNotifications();
        $serializer = $this->get('jms_serializer');
        $notifications = $serializer->serialize($notifications, 'json');

        return new JsonResponse($notifications, 200);
    }

    /**
     * Count all Notification entities by id.
     *
     * @Route("/all/count/{id}", name="notification__count_index")
     */
    public function CountAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('AppBundle:User')->find($id);
        $count = 0;
        $notifications = $users->getNotifications();
        /** @var Notification $notification */
        foreach ($notifications as $notification) {
            if (!$notification->isHasRead()) {
                $count += 1;
            }
        }
        return new JsonResponse($count, 200);
    }

    /**
     * all user notifications.
     *
     * @Route("/all/notification", name="notification_list")
     */
    public function UserNotificationAction()
    {
        /** @var User $user */
        $user = $this->getUser();
        $notList = $user->getNotifications();
        $serializer = $this->container->get('jms_serializer');
        $notifications = $serializer->serialize($notList, 'json');
        return new Response($notifications, 200);
    }

    /**
     * read notification.
     *
     * @Route("/all/notification/read/{id}", name="notification_list_read")
     */
    public function UserNotificationReadAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Notification $not */
        $not = $em->getRepository('AppBundle:Notification')->find($id);
        $not->setHasRead(true);
        $em->persist($not);
        $em->flush();

        /** @var User $user */
        $user = $this->getUser();
        $count = 0;
        $notList = $user->getNotifications();
        /** @var Notification $not */
        foreach ($notList as $not) {
            if (!$not->isHasRead()) {
                $count += 1;
            }
        }

//        $serializer = $this->container->get('jms_serializer');
//        $notifications = $serializer->serialize($notList, 'json');
        return new JsonResponse($count, 200);
    }


}
