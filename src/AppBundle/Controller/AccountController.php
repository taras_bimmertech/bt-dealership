<?php


namespace AppBundle\Controller;

use AppBundle\Entity\DealerOrder;
use AppBundle\Entity\DownloadFile;
use AppBundle\Entity\User;
use AppBundle\Form\ResetPasswordType;
use AppBundle\Utils\SendNotification;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\UserDealer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @Route("/account")
 * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER') or has_role('ROLE_USER') ")
 */
class AccountController extends Controller
{

    /**
     * @Route("/", name="account_index")
     * @Security("has_role('ROLE_USER')")
     */
    public function indexAction(Request $request)
    {
        $userEnter = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $id = $userEnter->getId();
        $notTok = $request->cookies->get('notification_token');

        if ($notTok != null) {
            $userEnter->setNotificationToken($notTok);
            $em->persist($userEnter);
            $em->flush();
        }
        $userEnter->setNotificationToken($notTok);

        $user = $em->getRepository('AppBundle:UserDealer')->find($id);
        $editForm = $this->createForm('AppBundle\Form\UserDealerType', $user);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $sendNotification = new SendNotification();
            /** @var User $admin */
            $admin = $em->getRepository('AppBundle:User')->findByRole('["ROLE_ADMIN"]');
            $notification = $sendNotification->send($admin[0]->getNotificationToken(), $admin[0]->getId(), $user->getCompany(), 'Profile information has been updated.');
            $notification->setUsers($admin[0]);
            $em->persist($user);
            $em->persist($notification);
            $em->flush();
            return $this->redirectToRoute('vincheck_index');
        }
        $form = $this->createForm(ResetPasswordType::class, $user);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $oldPassword = $user->getOldPassword();
            $encoder = $this->get('security.password_encoder');
            if (!$encoder->isPasswordValid($user, $oldPassword)) {
                return new JsonResponse('The old password you have given is incorrect. Please try again.');
            }
            $em = $this->getDoctrine()->getManager();
            $encoder = $this->get('security.password_encoder');
            $user->setPassword($encoder->encodePassword($user, $user->getPlainPassword()));
            $em->persist($user);
            $em->flush();
            return new JsonResponse('Your password has been successfully changed.');
        }
        $status = $em->getRepository('AppBundle:Status')->findAll();
        $news = $em->getRepository('AppBundle:Post')->findAllNine();
        $coupons = $em->getRepository('AppBundle:Coupon')->findAll();
        return $this->render('account/index.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'form_pass' => $form->createView(),
            'news' => $news,
            'status' => $status,
            'coupons' => $coupons
        ));
    }

    /**
     * @Route("/profile/dealer{id}", name="account_dealer_profile")
     * @param $id
     * @return Response
     */
    public function dealerPageIndex(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $userEnter = $this->getUser();
        $idAdmin = $userEnter->getId();
        $user = $em->getRepository('AppBundle:User')->find($idAdmin);
        $dealer = $em->getRepository('AppBundle:UserDealer')->find($id);
        $editForm = $this->createForm('AppBundle\Form\UserType', $user);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('account_dealer_profile', array('id' => $id));
        }
        $form = $this->createForm('AppBundle\Form\ResetPasswordType', $user);
        $form->handleRequest($request);
        $order = new DealerOrder();
        $formOrder = $this->createForm('AppBundle\Form\NewOrderType', $order);
        $formOrder->handleRequest($request);
        //form form new order
        if ($formOrder->isSubmitted() && $formOrder->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $order->setDealer($dealer);
            $em->persist($order);
            $em->persist($dealer);
            $em->flush();
            return $this->redirectToRoute('account_dealer_profile', array('id' => $id));
        }
        $formChangeManager = $this->createForm('AppBundle\Form\ChangeManagerType', $dealer);
        $formChangeManager->handleRequest($request);

        if ($formChangeManager->isSubmitted() && $formChangeManager->isValid()) {
            $em->persist($dealer);
            $em->flush();
            return $this->redirectToRoute('account_dealer_profile', array('id' => $id));
        }
        //form for change password
        if ($form->isValid()) {
            $oldPassword = $user->getOldPassword();
            $encoder = $this->get('security.password_encoder');
            if (!$encoder->isPasswordValid($user, $oldPassword)) {
                return new JsonResponse('You wrote wrong old password');
            }
            $encoder = $this->get('security.password_encoder');
            $user->setPassword($encoder->encodePassword($user, $user->getPlainPassword()));
            $em->persist($user);
            $em->flush();
            return new JsonResponse('Your password has been successfully changed.');
        }
        return $this->render('admin/dealer_profile.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'form_pass' => $form->createView(),
            'dealer' => $dealer,
            'form_order' => $formOrder->createView(),
            'change_manager' => $formChangeManager->createView(),


        ));
    }


    /**
     * @param Request $request
     * @Route("/profile/order/{user}/edit/{id}", name="admin_order_edit_by_id")
     * @return Response
     */
    public function editOrderAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $dealerOrder = $em->getRepository('AppBundle:DealerOrder')->find($id);
        $formOrder = $this->createForm('AppBundle\Form\EditOrderType', $dealerOrder);
        $formOrder->handleRequest($request);
        return $this->render('default/edit.html.twig', array(
            'editCssId' => 'editOrder',
            'editTitle' => 'Edit Order',
            'editAjaxForm' => $formOrder->createView(),
        ));
    }


    /**
     * @param Request $request
     * @Route("/profile/order/{user}/edit/do/{id}", name="admin_order_do_edit_by_id")
     * @return Response
     */
    public function doEditOrderAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $editOrder = $request->get('id');
        $order = $em->getRepository('AppBundle:DealerOrder')->find($editOrder);
        $serializer = new Serializer(array(new DateTimeNormalizer()));

        $dateAsString = $serializer->denormalize($request->get('createAt'), \DateTime::class);
        $order->setCost($request->get('cost'));
        $order->setCreateAt($dateAsString);
        $order->setItemNumber($request->get('itemNumber'));
        $order->setValue($request->get('value'));
        $em->flush();
        return new JsonResponse('edit order');
    }

    /**
     * @param Request $request
     * @Route("/profile/order/{user}/delete/{id}", name="admin_order_delete_by_id")
     * @return Response
     */
    public
    function deleteOrderAction(Request $request, $user, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:UserDealer')->find($user);
        $UserOrder = $em->getRepository('AppBundle:DealerOrder')->find($id);
        /**@var  UserDealer $user */
        $user->removeOrders($UserOrder);
        $em->remove($UserOrder);
        $em->flush();
//        return $this->redirectToRoute('admin_account_index');
        return new JsonResponse('success delete');
    }


    /**
     *
     * @Route("/send/manager", name="send_to_manager_dealer_mail")
     */
    public function sendMistakeAction(Request $request)
    {
        /** @var UserDealer $dealer */
        $dealer = $this->getUser();
        $message = $request->get('message');
//        $mail = $request->get('mail');
        $mail = $dealer->getEmail();
        $toMail = $dealer->getUserManager()->getEmail();
        $dealerCompany = $dealer->getCompany();
        try {
            $this->get('app_mailer')->sendToManager($toMail, $dealerCompany, $message, $mail);
            return new JsonResponse('Message sent. Thank you for helping us improve this tool.');
        } catch (Exception $e) {
            return new JsonResponse('Error sending message. Please try again.');
        }

    }

}
