function login() {
    if (!IsSafari()) {
        var messaging;
        // Registration was successful
        var config = {
            apiKey: "AIzaSyBPA4DVLR0SALXN8TJOplwWVOKaBzokdgc",
            authDomain: "dealership-a2d80.firebaseapp.com",
            databaseURL: "https://dealership-a2d80.firebaseio.com",
            projectId: "dealership-a2d80",
            storageBucket: "dealership-a2d80.appspot.com",
            messagingSenderId: "181544635677"
        };
        firebase.initializeApp(config);
        messaging = firebase.messaging();
        messaging.requestPermission().then(function () {
            navigator.serviceWorker.register('/dealers/firebase-messaging-sw.js')
                .then(function (registration) {
                    messaging.useServiceWorker(registration);
                    console.log('ServiceWorker registration successful with scope: ', registration.scope);
                    return messaging.getToken();
                })
                .then(function (token) {
                    console.log(token);
                    document.cookie = "notification_token=" + token;
                })
                .catch(function (err) {
                    // registration failed :(
                    console.log('ServiceWorker registration failed: ', err);
                });
        });
        setTimeout(function () {
            messaging.onMessage(function (payload) {
                message = payload.notification.body;
                title = payload.notification.title;
                showToastr(title, message);
                getNotificationCount(payload.data.id);
            });
        }, 2000);

    } else {

    }

}