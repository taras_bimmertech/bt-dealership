<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * DealerOrder
 *
 * @ORM\Table(name="dealer_order")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DealerOrderRepository")
 */
class DealerOrder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $createdAt
     *
     * @Serializer\Exclude
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createAt;

    /**
     * @var int
     *
     * @ORM\Column(name="value", type="integer")
     */
    private $value;

    /**
     * @var int
     *
     * @ORM\Column(name="cost", type="integer")
     */
    private $cost;

    /**
     * @var int
     *
     * @ORM\Column(name="itemNumber", type="integer")
     */
    private $itemNumber;


    /**
     * @ORM\ManyToOne(targetEntity="UserDealer", inversedBy="orders", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id")
     * @Serializer\Exclude
     */
    protected $dealer;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set date
     *
     * @param \DateTime $createAt
     *
     * @return DealerOrder
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get date
     *
     * @return DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set value
     *
     * @param integer $value
     *
     * @return DealerOrder
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set cost
     *
     * @param integer $cost
     *
     * @return DealerOrder
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return int
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set itemNumber
     *
     * @param integer $itemNumber
     *
     * @return DealerOrder
     */
    public function setItemNumber($itemNumber)
    {
        $this->itemNumber = $itemNumber;

        return $this;
    }

    /**
     * Get itemNumber
     *
     * @return int
     */
    public function getItemNumber()
    {
        return $this->itemNumber;
    }

    /**
     * @return mixed
     */
    public function getDealer()
    {
        return $this->dealer;
    }

    /**
     * @param mixed $dealer
     */
    public function setDealer($dealer)
    {
        $this->dealer = $dealer;
    }

}

