<?php


namespace AppBundle\Controller\Admin;

use AppBundle\Entity\DealerOrder;
use AppBundle\Entity\UserManager;
use AppBundle\Form\ResetPasswordType;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\User;
use AppBundle\Entity\UserDealer;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * @Route("/admin/account")
 * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER') or has_role('ROLE_USER') ")
 */
class AccountController extends Controller
{

    /**
     * @Route("/", name="admin_account_index")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $us = $request->get('user');
        $userEnter = $this->getUser();
        $notTok = $request->cookies->get('notification_token');
        if ($notTok != null) {
            $userEnter->setNotificationToken($notTok);
            $em->persist($userEnter);
            $em->flush();
        }
        $id = $userEnter->getId();

        $user = $em->getRepository('AppBundle:User')->find($id);
        $dealers = $em->getRepository('AppBundle:UserDealer')->findAllTen();
        $lastDealers = $dealers;
        $totalValue = 0;
        for ($i = 0; $i < count($dealers); $i++) {
            $orderSum = $dealers[$i]->getOrders();

            foreach ($orderSum as $order) {
                $totalValue += $order->getCost();
            }
        }
        $editForm = $this->createForm('AppBundle\Form\UserType', $user);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }
        $userDealer = new UserDealer();
        $edit_dealer = $this->createForm('AppBundle\Form\EditUserDealerType', $userDealer);
        if (isset($us)) {
            $userForEdit = $em->getRepository('AppBundle:UserDealer')->find($us);
            $edit_dealer = $this->createForm('AppBundle\Form\EditUserDealerType', $userForEdit);
//            $userDealer = $userForEdit;

        }
        $userDealerDelete = new UserDealer();
        $delete_dealer = $this->createForm('AppBundle\Form\DeleteUserDealerType', $userDealerDelete);
        $delete_dealer->handleRequest($request);
        if ($delete_dealer->isSubmitted() && $delete_dealer->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $id = $delete_dealer['dealer']->getData()->getId();
            $single_dealer = $em->getRepository('AppBundle:UserDealer')->find($id);
            $em->remove($single_dealer);
            $em->flush();
            return $this->redirectToRoute('admin_account_index');
        }
        $userManagerDelete = new UserManager();
        $delete_manager = $this->createForm('AppBundle\Form\DeleteUserManagerType', $userManagerDelete);
        $delete_manager->handleRequest($request);

        if ($delete_manager->isSubmitted() && $delete_manager->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $id = $delete_manager['manager']->getData()->getId();
            $single_user = $em->getRepository('AppBundle:User')->find($id);
            $em->remove($single_user);
            $em->flush();
            return $this->redirectToRoute('admin_account_index');
        }
        $new_dealer = $this->createForm('AppBundle\Form\NewUserDealerType', $userDealer);
        $new_dealer->handleRequest($request);


        $form = $this->createForm('AppBundle\Form\ResetPasswordType', $user);
        $form->handleRequest($request);
        //form for change password
        if ($form->isValid()) {
            $oldPassword = $user->getOldPassword();
            $encoder = $this->get('security.password_encoder');
            if (!$encoder->isPasswordValid($user, $oldPassword)) {
                return new JsonResponse('The old password you have given is incorrect. Please try again.');
            }
            $encoder = $this->get('security.password_encoder');
            $user->setPassword($encoder->encodePassword($user, $user->getPlainPassword()));
            $em->persist($user);
            $em->flush();
            return new JsonResponse('Your password has been successfully changed.');
        }
        //form for new dealer
        if ($new_dealer->isSubmitted() && $new_dealer->isValid()) {
            $encoder = $this->get('security.password_encoder');
            $em->persist($userDealer);
            $em->flush();
            $userforPass = $em->getRepository('AppBundle:User')->find($userDealer->getId());
            $userforPass->setPassword($encoder->encodePassword($userforPass, $new_dealer['password']->getData()));
            $em->persist($userforPass);
            $em->flush();
            return $this->redirectToRoute('admin_account_index');

        }

        $manager = new UserManager();
        $formNewManager = $this->createForm('AppBundle\Form\UserManagerType', $manager);
        $formNewManager->handleRequest($request);
        if ($formNewManager->isSubmitted() && $formNewManager->isValid()) {
            $managerFoto = $formNewManager['file']->getData();

            if ($managerFoto == null) {
                copy('img/profile_default.png', 'img/profile_default-copy.png');
                $file = new UploadedFile('img/profile_default-copy.png', 'profile_default', null, null, null, true);
                $manager->upload();
                $manager->setFile($file);
            }

            $encoder = $this->get('security.password_encoder');
            $manager->setPassword($encoder->encodePassword($manager, $formNewManager['password']->getData()));

            $em->persist($manager);
            $em->flush();
            return $this->redirectToRoute('admin_account_index');
        }


        $edit_dealer->handleRequest($request);
        //for edit dealer
        if ($edit_dealer->isSubmitted()) {
            $editUser = $edit_dealer['id']->getData();
            $userForEdit = $em->getRepository('AppBundle:UserDealer')->find($editUser);

            $userForEdit->setCompany($edit_dealer['company']->getData());
            $userForEdit->setAddress($edit_dealer['address']->getData());

            $userForEdit->setCity($edit_dealer['city']->getData());
            $userForEdit->setZip($edit_dealer['zip']->getData());
            $userForEdit->setCountry($edit_dealer['country']->getData());
            $userForEdit->setState($edit_dealer['state']->getData());
            $userForEdit->setPhone($edit_dealer['phone']->getData());
            $userForEdit->setSupportEmail($edit_dealer['supportEmail']->getData());
            $userForEdit->setWebsite($edit_dealer['website']->getData());
            $userForEdit->setEmail($edit_dealer['email']->getData());
            $userForEdit->setUsername($edit_dealer['username']->getData());
            $usermanager = $em->getRepository('AppBundle:UserManager')->find($edit_dealer['userManager']->getData());
            $userForEdit->setUserManager($usermanager);

            $em->flush();

            return $this->redirectToRoute('admin_account_index');
        }

        $managers = $em->getRepository('AppBundle:UserManager')->findAll();
        return $this->render('admin/index.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'form_pass' => $form->createView(),
            'dealers' => $lastDealers,
            'new_dealer' => $new_dealer->createView(),
            'edit_dealer' => $edit_dealer->createView(),
            'form_new_manager' => $formNewManager->createView(),
            'delete_dealer' => $delete_dealer->createView(),
            'delete_manager' => $delete_manager->createView(),
            'totalValue' => $totalValue,
            'managers' => $managers
        ));
    }


    /**
     * @Route("/all/dealers", name="admin_account_index_all_Dealers")
     */
    public function indexAllDealersAction(Request $request)
    {
        $show_all = $request->get('resently');
        $em = $this->getDoctrine()->getManager();
        $managers = $em->getRepository('AppBundle:UserManager')->findAll();
        if ($show_all == 'true') {
            $dealers = $em->getRepository('AppBundle:UserDealer')->findAllTen();
        } else {
            $dealers = $em->getRepository('AppBundle:UserDealer')->findAllByCompany();
        }
        return $this->render('default/dealerTable.html.twig', array(
            'dealers' => $dealers,
            'managers' => $managers
        ));

    }


    /**
     *
     * @Route("/user/by/{id}", name="admin_account_by_id")
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function userByIdAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:UserDealer')->find($id);
        $serializer = $this->container->get('jms_serializer');
        $user = $serializer->serialize($user, 'json');

        return new JsonResponse($user);
    }

    /**
     * @param Request $request
     * @Route("/dealer/delete/{id}", name="admin_dealer_delete_by_id")
     * @return Response
     */
    public function deleteDealerAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:UserDealer')->find($id);
        $em->remove($user);
        $em->flush();
//        return $this->redirectToRoute('admin_account_index');
        return new JsonResponse('success delete');
    }

    /**
     * @param Request $request
     * @Route("/managers/delete/{id}", name="admin_manager_delete_by_id")
     * @return Response
     */
    public function deleteManagerAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:UserManager')->find($id);
        $em->remove($user);
        $em->flush();
        return $this->redirectToRoute('admin_manager_index');
    }

    /**
     * @param Request $request
     * @Route("/managers", name="admin_manager_index")
     * @return Response
     */
    public function indexManagersAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $managers = $em->getRepository('AppBundle:UserManager')->findAll();

        $userEnter = $this->getUser();
        $id = $userEnter->getId();
        $user = $em->getRepository('AppBundle:User')->find($id);
        //form for edit user
        $editForm = $this->createForm('AppBundle\Form\UserType', $user);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }
        //form for change password
        $form = $this->createForm('AppBundle\Form\ResetPasswordType', $user);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $oldPassword = $user->getOldPassword();
            $encoder = $this->get('security.password_encoder');
            if (!$encoder->isPasswordValid($user, $oldPassword)) {
                return new JsonResponse('The old password you have given is incorrect. Please try again.');
            }
            $encoder = $this->get('security.password_encoder');
            $user->setPassword($encoder->encodePassword($user, $user->getPlainPassword()));
            $em->persist($user);
            $em->flush();
            return new JsonResponse('Your password has been successfully changed.');
        }

        //form for new manager
        $manager = new UserManager();
        $formNewManager = $this->createForm('AppBundle\Form\UserManagerType', $manager);
        $formNewManager->handleRequest($request);
        if ($formNewManager->isSubmitted() && $formNewManager->isValid()) {
            $managerFoto = $formNewManager['file']->getData();

            if ($managerFoto == null) {
                copy('img/profile_default.png', 'img/profile_default-copy.png');
                $file = new UploadedFile('img/profile_default-copy.png', 'profile_default', null, null, null, true);
                $manager->upload();
                $manager->setFile($file);
            }

            $encoder = $this->get('security.password_encoder');
            $manager->setPassword($encoder->encodePassword($manager, $formNewManager['password']->getData()));

            $em->persist($manager);
            $em->flush();
            return $this->redirectToRoute('admin_manager_index');
        }


        return $this->render('admin/manager.html.twig', array(
            'user' => $user,
            'managers' => $managers,
            'edit_form' => $editForm->createView(),
            'form_pass' => $form->createView(),
            'form_new_manager' => $formNewManager->createView(),

        ));
    }


    /**
     * @param Request $request
     * @Route("/managers/{user}/edit/{id}", name="admin_manager_edit_by_id")
     * @return Response
     */
    public function editManagerAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $userManager = $em->getRepository('AppBundle:UserManager')->find($id);
        $formOrder = $this->createForm('AppBundle\Form\EditUserManagerType', $userManager);
        $formOrder->handleRequest($request);
        return $this->render('default/edit.html.twig', array(
            'editCssId' => 'editManager',
            'editTitle' => 'Edit Manager',
            'editAjaxForm' => $formOrder->createView(),
        ));
    }


    /**
     * @param Request $request
     * @Route("/managers/{user}/edit/do/{id}", name="admin_manager_do_edit_by_id")
     * @return Response
     */
    public function doEditManagerAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $userManager = $request->get('id');
        $manager = $em->getRepository('AppBundle:UserManager')->find($userManager);


        $manager->setEmail($request->get('email'));
        $manager->setPhoneNumber($request->get('phone'));
        $manager->setUsername($request->get('fullName'));
//        $manager->setFile($request->get('file'));
        $em->flush();
        return new JsonResponse('edit order');
    }


}
