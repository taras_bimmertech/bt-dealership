<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Exception\InvalidDataException;
use AppBundle\Form\ResetPasswordApi;
use AppBundle\Utils\TokenGenerator;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ResettingController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     *
     * @Route("/forgot/password/", name="forgot_password")
     */
    public function indexAction(Request $request)
    {
        return $this->render('security/forgot_pass.html.twig');
    }


    /**
     * @param Request $request
     * @return Response
     *
     * @Route("/api/resetting/request", name="api_resetting_request")
     */
    public function requestAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $email = $request->get('email');

        /* @var $user User */
//        try {
//            $user = $em->getRepository('AppBundle:User')->findByEmail($email);
        $user = $em->getRepository('AppBundle:User')->findOneBy(array('email' => $email));
//        } catch (Exception $ex) {
//            $ex->getMessage();
//        }

        if (!$user) {
            return new JsonResponse('No user with email address ' . $email . '  found. Please check your email address. ', 200);

        }

        $user->setConfirmationToken(TokenGenerator::generateToken());
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        $this->get('app_mailer')->sendResetPassword($user->getEmail(), $user->getConfirmationToken());

        return new JsonResponse('Instructions on how to reset your password have been sent to ' . $email, 200);

    }

    /**
     *
     * Reset user password
     *
     * @param Request $request
     * @param string $confirmationToken
     * @return Response
     *
     * @Route("/resetting/reset/{confirmationToken}", name="api_resetting_reset")
     */
    public function resetAction(Request $request, $confirmationToken)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $user User */
        $user = $em->getRepository('AppBundle:User')->findOneBy(['confirmationToken' => $confirmationToken]);
        if (!$user) {
            throw $this->createNotFoundException('User not found!');
        }
        $form = $this->createForm(ResetPasswordApi::class, $user);
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $encoder = $this->get('security.password_encoder');
                $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
                $user->setConfirmationToken(null);
                $em->persist($user);
                $em->flush();
                return $this->redirect($this->generateUrl('api_resetting_success'));
            }
        }
        return $this->render('resetting/reset.html.twig', [
            'form' => $form->createView(),
            'confirmationToken' => $confirmationToken
        ]);
    }

    /**
     *
     *
     * @param Request $request
     * @return Response
     *
     * @Route("/resetting/success", name="api_resetting_success")
     */
    public function successAction(Request $request)
    {
        return $this->render('resetting/success.html.twig');
    }

}
