<?php

namespace AppBundle\Mailer;

class Mailer extends BaseMailer
{

    public function sendRegisterConfirm($toEmail, $token)
    {
        $this->send('mail/register-confirm.html.twig', ['token' => $token], $toEmail);
    }

    public function sendResetPassword($toEmail, $token)
    {
        $this->send('mail/reset-password.html.twig', ['token' => $token], $toEmail);
    }

    public function sendTicketReplay($toEmail, $replay)
    {
        $this->send('mail/replay.html.twig', ['replay' => $replay], $toEmail);
    }

    public function sendMistake($toEmail, $name, $vin, $message, $mail)
    {
        $this->send('mail/spot-mistake.html.twig', ['name' => $name, 'vin' => $vin, 'message' => $message, 'mail' => $mail], $toEmail);
    }


    public function sendStatusChange($toEmail, $date, $name, $company, $status, $newstatus, $discount, $newdiscount)
    {
        $this->send('mail/status-changed.html.twig', ['name' => $name, 'date' => $date, 'company' => $company,
            'status' => $status, 'newstatus' => $newstatus, 'discount' => $discount, 'newdiscount' => $newdiscount], $toEmail);
    }

    public function sendCommentToArticle($toEmail, $article, $message, $mail)
    {
        $this->send('mail/reply-artice.html.twig', ['article' => $article, 'mail' => $mail, 'message' => $message], $toEmail);
    }

    public function sendToManager($toEmail, $dealer, $message, $mail)
    {
        $this->send('mail/send-manager.html.twig', ['dealer' => $dealer, 'mail' => $mail, 'message' => $message], $toEmail);
    }

    public function sendOrders($toEmail, $company, $address, $email, $phone, $vin, $products, $comment)
    {
        $this->send('mail/send-orders.html.twig', ['company' => $company, 'address' => $address,
            'email' => $email, 'phone' => $phone, 'vin' => $vin, 'products' => $products, 'comment' => $comment], $toEmail);
    }
}




