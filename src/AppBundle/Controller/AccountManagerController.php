<?php


namespace AppBundle\Controller;

use AppBundle\Entity\DealerOrder;
use AppBundle\Entity\DownloadFile;
use AppBundle\Entity\Notification;
use AppBundle\Entity\User;
use AppBundle\Entity\UserManager;
use AppBundle\Form\ResetPasswordType;
use AppBundle\Utils\SendNotification;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\UserDealer;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @Route("manager/account")
 * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER') or has_role('ROLE_USER') ")
 */
class AccountManagerController extends Controller
{

    /**
     * @Route("/", name="account_manager_index")
     * @Security("has_role('ROLE_MANAGER')")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $us = $request->get('user');
        /** @var UserManager $userEnter */
        $userEnter = $this->getUser();
        $id = $userEnter->getId();
        $notTok = $request->cookies->get('notification_token');
        if ($notTok != null) {
            $userEnter->setNotificationToken($notTok);
            $em->persist($userEnter);
            $em->flush();
        }
        $userEnter->setNotificationToken($notTok);

        /** @var UserManager $user */
        $user = $em->getRepository('AppBundle:UserManager')->find($id);
        /** @var ArrayCollection $dealers */
        $dealers = $user->getUserDealer();
        $lastDealers = null;
        if (count($dealers) > 10) {
            $dealers = $dealers->toArray();
            $dealers = array_slice($dealers, 0, 10);
        }
        if (count($dealers) > 1) {
            $lastDealers = $dealers;
        } else {
            $lastDealers = $dealers;
        }
        $totalValue = 0;
        for ($i = 0; $i < count($dealers); $i++) {
            $orderSum = $dealers[$i]->getOrders();

            foreach ($orderSum as $order) {
                $totalValue += $order->getCost();
            }
        }
        $editForm = $this->createForm('AppBundle\Form\EditUserManagerWithFotoType', $user);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
//            /** @var UploadedFile $managerFoto */
//            $managerFoto = $editForm['file']->getData();
//            if ($managerFoto != null) {
//                $path = $managerFoto->getPath() . "/" . $managerFoto->getFilename();
//                $mim = $managerFoto->getMimeType();
//                $file = new UploadedFile($path, $user->getEmail(), $mim, null, null, true);
//                $user->setFile($file);
//                $user->preUpload();
//                $user->upload();
//
//            }
            $em->persist($user);
            $em->flush();
        }
        $userDealer = new UserDealer();
        $userDealer->setUserManager($userEnter);
        $edit_dealer = $this->createForm('AppBundle\Form\EditUserDealerType', $userDealer);

        if (isset($us)) {
            $userForEdit = $em->getRepository('AppBundle:UserDealer')->find($us);
            $edit_dealer = $this->createForm('AppBundle\Form\EditUserDealerType', $userForEdit);

        }
        $new_dealer = $this->createForm('AppBundle\Form\NewUserDealerType', $userDealer);
        $new_dealer->handleRequest($request);
        $form = $this->createForm('AppBundle\Form\ResetPasswordType', $user);
        $form->handleRequest($request);
        //form for change password
        if ($form->isValid()) {
            $oldPassword = $user->getOldPassword();
            $encoder = $this->get('security.password_encoder');
            if (!$encoder->isPasswordValid($user, $oldPassword)) {
                return new JsonResponse('The old password you have given is incorrect. Please try again.');
            }
            $encoder = $this->get('security.password_encoder');
            $user->setPassword($encoder->encodePassword($user, $user->getPlainPassword()));
            $em->persist($user);
            $em->flush();
            return new JsonResponse('Your password has been successfully changed.');
        }
        //form for new dealer
        if ($new_dealer->isSubmitted() && $new_dealer->isValid()) {
            $encoder = $this->get('security.password_encoder');
            $em->persist($userDealer);
            $em->flush();
            $userforPass = $em->getRepository('AppBundle:User')->find($userDealer->getId());
            $userforPass->setPassword($encoder->encodePassword($userforPass, $new_dealer['password']->getData()));
            $em->persist($userforPass);
            $em->flush();
            return $this->redirectToRoute('account_manager_index');

        }
        $edit_dealer->handleRequest($request);
        //for edit dealer
        if ($edit_dealer->isSubmitted()) {
            $editUser = $edit_dealer['id']->getData();
            $userForEdit = $em->getRepository('AppBundle:UserDealer')->find($editUser);

            $userForEdit->setCompany($edit_dealer['company']->getData());
            $userForEdit->setAddress($edit_dealer['address']->getData());

            $userForEdit->setCity($edit_dealer['city']->getData());
            $userForEdit->setZip($edit_dealer['zip']->getData());
            $userForEdit->setCountry($edit_dealer['country']->getData());
            $userForEdit->setState($edit_dealer['state']->getData());
            $userForEdit->setPhone($edit_dealer['phone']->getData());
            $userForEdit->setSupportEmail($edit_dealer['supportEmail']->getData());
            $userForEdit->setWebsite($edit_dealer['website']->getData());
            $userForEdit->setEmail($edit_dealer['email']->getData());
            $userForEdit->setUsername($edit_dealer['username']->getData());
            $usermanager = $em->getRepository('AppBundle:UserManager')->find($edit_dealer['userManager']->getData());
            $userForEdit->setUserManager($usermanager);

            $em->flush();

            return $this->redirectToRoute('account_manager_index');
        }

        return $this->render('account/index-manager.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'form_pass' => $form->createView(),
            'dealers' => $lastDealers,
            'new_dealer' => $new_dealer->createView(),
            'edit_dealer' => $edit_dealer->createView(),
            'totalValue' => $totalValue,
        ));
    }

    /**
     * @Route("/all/dealers", name="manager_account_index_all_Dealers")
     */
    public function showManagerDealersAction(Request $request)
    {

        $show_all = $request->get('resently');
        $em = $this->getDoctrine()->getManager();
        /** @var UserManager $userEnter */
        $userEnter = $this->getUser();
        $id = $userEnter->getId();


        $dealers = $em->getRepository('AppBundle:UserDealer')->findAllManager($id);



        if ($show_all == 'true') {
            /** @var UserManager $user */
            $user = $em->getRepository('AppBundle:UserManager')->find($id);
            /** @var ArrayCollection $dealers */
            $dealers = $user->getUserDealer();
            $dealers = $dealers->toArray();
            if (count($dealers) > 10) {
                $dealers = array_slice($dealers, 0, 10);
            }
        }

        return $this->render('default/dealerTableM.html.twig', array(
            'dealers' => $dealers
        ));
    }

    /**
     * @Route("/profile/dealer{id}", name="account_dealer_profile")
     * @param $id
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER') ")
     * @return Response
     */
    public function dealerPageIndex(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $userEnter = $this->getUser();
        $idAdmin = $userEnter->getId();

        if ($userEnter->getRoles()[0] === 'ROLE_MANAGER') {
            $user = $em->getRepository('AppBundle:UserManager')->find($idAdmin);
            $editForm = $this->createForm('AppBundle\Form\EditUserManagerWithFotoType', $user);
        } else {
            $user = $em->getRepository('AppBundle:User')->find($idAdmin);
            $editForm = $this->createForm('AppBundle\Form\UserType', $user);
        }
        $dealer = $em->getRepository('AppBundle:UserDealer')->find($id);
        $this->changeSatus($dealer);
        $em->persist($dealer);
        $em->flush();

        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('account_dealer_profile', array('id' => $id));
        }
        $form = $this->createForm('AppBundle\Form\ResetPasswordType', $user);
        $form->handleRequest($request);
        $order = new DealerOrder();
        $formOrder = $this->createForm('AppBundle\Form\NewOrderType', $order);
        $formOrder->handleRequest($request);
        //form form new order
        if ($formOrder->isSubmitted() && $formOrder->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $order->setDealer($dealer);

//            $this->changeSatus($dealer);
            $sendNotification = new SendNotification();
            /** @var User $admin */
            $admin = $em->getRepository('AppBundle:User')->findByRole('["ROLE_ADMIN"]');
            // notification for admin that dealer change status
            $notificationA = $sendNotification->send($admin[0]->getNotificationToken(), $admin[0]->getId(), $dealer->getCompany(), 'Add new order');
            $notificationA->setUsers($admin[0]);

            $em->persist($order);
            $em->persist($dealer);
            $em->persist($notificationA);
            $em->flush();
            return $this->redirectToRoute('account_dealer_profile', array('id' => $id));
        }
        $formChangeManager = $this->createForm('AppBundle\Form\EditUserDealerType', $dealer);
        $formChangeManager->handleRequest($request);

        if ($formChangeManager->isSubmitted() && $formChangeManager->isValid()) {
            $em->persist($dealer);
            $em->flush();
            return $this->redirectToRoute('account_dealer_profile', array('id' => $id));
        }
        //form for change password
        if ($form->isValid()) {
            $oldPassword = $user->getOldPassword();
            $encoder = $this->get('security.password_encoder');
            if (!$encoder->isPasswordValid($user, $oldPassword)) {
                return new JsonResponse('The old password you have given is incorrect. Please try again.');
            }
            $encoder = $this->get('security.password_encoder');
            $user->setPassword($encoder->encodePassword($user, $user->getPlainPassword()));
            $em->persist($user);
            $em->flush();
            return new JsonResponse('Your password has been successfully changed.');
        }
        return $this->render('admin/dealer_profile.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'form_pass' => $form->createView(),
            'dealer' => $dealer,
            'form_order' => $formOrder->createView(),
            'change_manager' => $formChangeManager->createView(),


        ));
    }


    /**
     * @param Request $request
     * @Route("/profile/order/{user}/edit/{id}", name="admin_order_edit_by_id")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER')")
     * @return Response
     */
    public function editOrderAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $dealerOrder = $em->getRepository('AppBundle:DealerOrder')->find($id);
        $formOrder = $this->createForm('AppBundle\Form\EditOrderType', $dealerOrder);
        $formOrder->handleRequest($request);
        return $this->render('default/edit.html.twig', array(
            'editCssId' => 'editOrder',
            'editTitle' => 'Edit Order',
            'editAjaxForm' => $formOrder->createView(),
        ));
    }


    /**
     * @param Request $request
     * @Route("/profile/order/{user}/edit/do/{id}", name="admin_order_do_edit_by_id")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER')  ")
     * @return Response
     */
    public function doEditOrderAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $editOrder = $request->get('id');
        $order = $em->getRepository('AppBundle:DealerOrder')->find($editOrder);
        $serializer = new Serializer(array(new DateTimeNormalizer()));

        $dateAsString = $serializer->denormalize($request->get('createAt'), \DateTime::class);
        $order->setCost($request->get('cost'));
        $order->setCreateAt($dateAsString);
        $order->setItemNumber($request->get('itemNumber'));
        $order->setValue($request->get('value'));

        $em->flush();
        return new JsonResponse('edit order');
    }

    /**
     * @param Request $request
     * @Route("/profile/order/{user}/delete/{id}", name="admin_order_delete_by_id")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER') ")
     * @return Response
     */
    public
    function deleteOrderAction(Request $request, $user, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $dealer = $em->getRepository('AppBundle:UserDealer')->find($user);
        $UserOrder = $em->getRepository('AppBundle:DealerOrder')->find($id);
        /**@var  UserDealer $dealer */
        $dealer->removeOrders($UserOrder);
        $em->remove($UserOrder);
        $em->flush();
        $this->changeSatus($dealer);
//        return $this->redirectToRoute('admin_account_index');
        return new JsonResponse('success delete');
    }


    /**
     * @Route("/{id}", name="account_manager_profile")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER') ")
     * @param $id
     * @return Response
     */

    public function managerPageIndex(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $userEnter = $this->getUser();
        $idAdmin = $userEnter->getId();
        $user = $em->getRepository('AppBundle:User')->find($idAdmin);
        $manager = $em->getRepository('AppBundle:UserManager')->find($id);

        $editForm = $this->createForm('AppBundle\Form\UserType', $user);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('account_manager_profile', array('id' => $id));
        }
        $form = $this->createForm('AppBundle\Form\ResetPasswordType', $user);
        $form->handleRequest($request);
        //form for change password
        if ($form->isValid()) {
            $oldPassword = $user->getOldPassword();
            $encoder = $this->get('security.password_encoder');
            if (!$encoder->isPasswordValid($user, $oldPassword)) {
                return new JsonResponse('The old password you have given is incorrect. Please try again.');
            }
            $encoder = $this->get('security.password_encoder');
            $user->setPassword($encoder->encodePassword($user, $user->getPlainPassword()));
            $em->persist($user);
            $em->flush();
            return new JsonResponse('Your password has been successfully changed.');
        }


        $dealerPlatitum = $em->getRepository('AppBundle:UserDealer')->findBy(array('status' => User::STATUS_PLATINUM, 'userManager' => $id), array());
        $dealerGold = $em->getRepository('AppBundle:UserDealer')->findBy(array('status' => User::STATUS_GOLD, 'userManager' => $id), array());
        $dealerSilver = $em->getRepository('AppBundle:UserDealer')->findBy(array('status' => User::STATUS_SILVER, 'userManager' => $id), array());
        $dealerBronze = $em->getRepository('AppBundle:UserDealer')->findBy(array('status' => User::STATUS_BRONZE, 'userManager' => $id), array());
        $editManager = $this->createForm('AppBundle\Form\EditUserManagerType', $manager);
        $editManager->handleRequest($request);
        if ($editManager->isSubmitted() && $editManager->isValid()) {
            /** @var UploadedFile $managerFoto */
            $managerFoto = $editManager['file']->getData();
            if ($managerFoto != null) {

                $manager->setFile($managerFoto);

                $manager->preUpload();
                $manager->upload();
            }
            $em->persist($manager);
            $em->flush();
            return $this->redirectToRoute('account_manager_profile', array('id' => $id));
        }
        return $this->render('admin/manager_profile.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'form_pass' => $form->createView(),
            'manager' => $manager,
            'edit_manager' => $editManager->createView(),
            'dealerPlatitum' => $dealerPlatitum,
            'dealerGold' => $dealerGold,
            'dealerSilver' => $dealerSilver,
            'dealerBronze' => $dealerBronze,


        ));
    }


    public function changeSatus($dealer)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Notification $statusNot */
        $statusNot = $dealer->changeStatus();
        if ($statusNot !== null) {

            $sendNotification = new SendNotification();
            /** @var User $admin */
            $admin = $em->getRepository('AppBundle:User')->findByRole('["ROLE_ADMIN"]');
            // notification for admin that dealer change status
            $notificationA = $sendNotification->send($admin[0]->getNotificationToken(), $admin[0]->getId(), $statusNot->getTitle(), $statusNot->getMessage());
            $notificationA->setUsers($admin[0]);
            // notification for manager that dealer change status
            $notificationM = $sendNotification->send($dealer->getUserManager()->getNotificationToken(), $admin[0]->getId(), $statusNot->getTitle(), $statusNot->getMessage());
            $notificationM->setUsers($dealer->getUserManager());
            // notification for dealer that dealer change status
            $notificationD = $sendNotification->send($dealer->getNotificationToken(), $dealer->getId(), $statusNot->getTitle(), $statusNot->getMessage());
            $notificationD->setUsers($dealer);
            $em->persist($notificationA);
            $em->persist($notificationM);
            $em->persist($notificationD);
            $em->flush();

        }
    }
}
