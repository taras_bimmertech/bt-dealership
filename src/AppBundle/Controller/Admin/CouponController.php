<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Coupon;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Coupon controller.
 * @Security("has_role('ROLE_ADMIN')")
 * @Route("setting/coupon")
 */
class CouponController extends Controller
{
    /**
     * Lists all coupon entities.
     *
     * @Route("/", name="setting_coupon_index")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $coupons = $em->getRepository('AppBundle:Coupon')->findAll();
        $helper = $this->get('app_helper');
        $userEnter = $this->getUser();
        $editForm = $helper->getUserFormAction($request, $this);
        $formPass = $helper->getUserPassformAction($request, $this);
        return $this->render('coupon/index.html.twig', array(
            'coupons' => $coupons,
            'user' => $userEnter,
            'edit_form' => $editForm->createView(),
            'form_pass' => $formPass->createView()
        ));
    }

    /**
     * Creates a new coupon entity.
     *
     * @Route("/new", name="setting_coupon_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $coupon = new Coupon();
        $form = $this->createForm('AppBundle\Form\CouponType', $coupon);
        $form->handleRequest($request);
        $helper = $this->get('app_helper');
        $userEnter = $this->getUser();
        $editForm = $helper->getUserFormAction($request, $this);
        $formPass = $helper->getUserPassformAction($request, $this);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($coupon);
            $em->flush();

            return $this->redirectToRoute('setting_coupon_show', array('id' => $coupon->getId()));
        }

        return $this->render('coupon/new.html.twig', array(
            'coupon' => $coupon,
            'form' => $form->createView(),
            'user' => $userEnter,
            'edit_form' => $editForm->createView(),
            'form_pass' => $formPass->createView()
        ));
    }

    /**
     * Finds and displays a coupon entity.
     *
     * @Route("/{id}", name="setting_coupon_show")
     */
    public function showAction(Coupon $coupon)
    {
        $deleteForm = $this->createDeleteForm($coupon);

        return $this->render('coupon/show.html.twig', array(
            'coupon' => $coupon,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing coupon entity.
     *
     * @Route("/{id}/edit", name="setting_coupon_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Coupon $coupon)
    {
        $deleteForm = $this->createDeleteForm($coupon);
        $editForm = $this->createForm('AppBundle\Form\CouponType', $coupon);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            if ($couponFile = $editForm['file']->getData()) {
                $coupon->setFile($couponFile);
                $coupon->preUpload();
                $coupon->upload();

            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('setting_coupon_edit', array('id' => $coupon->getId()));
        }

        return $this->render('coupon/edit.html.twig', array(
            'coupon' => $coupon,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a coupon entity.
     *
     * @Route("/{id}", name="setting_coupon_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Coupon $coupon)
    {
        $form = $this->createDeleteForm($coupon);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($coupon);
            $em->flush();
        }

        return $this->redirectToRoute('setting_coupon_index');
    }

    /**
     * Creates a form to delete a coupon entity.
     *
     * @param Coupon $coupon The coupon entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Coupon $coupon)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('setting_coupon_delete', array('id' => $coupon->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
