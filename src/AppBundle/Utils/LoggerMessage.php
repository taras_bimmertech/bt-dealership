<?php


namespace AppBundle\Utils;


use Monolog\Handler\StreamHandler;
use Monolog\Logger;


class LoggerMessage
{
    private $log;

    private $orderHandler;
    private $vinCheckHandler;
    private $printHandler;
    private $infoHandler;
    private static $instance;

    private function __construct()
    {
        $this->log = new Logger('name');
        $this->orderHandler = new StreamHandler('order.log', Logger::INFO);
        $this->printHandler = new StreamHandler('vinprint.log', Logger::INFO);
        $this->vinCheckHandler = new StreamHandler('vincheck.log', Logger::INFO);
        $this->infoHandler = new StreamHandler('login.log', Logger::INFO);
    }


    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new LoggerMessage();
        }
        return self::$instance;
    }

    public function sendInfoMessage($message, $params = null)
    {
        $this->log->pushHandler($this->infoHandler);
        $this->log->info($message, ['email' => $params]);
    }

    public function LogPrint($message, $params = null)
    {
        $this->log->pushHandler($this->printHandler);
        $this->log->info($message, $params);
    }

    public function logVinCheck($message, $params = null)
    {
        $this->log->pushHandler($this->vinCheckHandler);
        $this->log->info($message, $params);
    }

    public function logOrder($message, $params = null)
    {
        $this->log->pushHandler($this->orderHandler);
        $this->log->info($message, $params);
    }

}