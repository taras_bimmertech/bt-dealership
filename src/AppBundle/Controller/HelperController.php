<?php


namespace AppBundle\Controller;

use AppBundle\Form\ResetPasswordType;
use AppBundle\Utils\Util;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\UserDealer;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @property  container
 * @Route("/helper")
 * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER') or has_role('ROLE_USER') ")
 */
class HelperController extends Controller
{


    /**
     * @Route("/user_form", name="helper_index_form")
     */
    public function getUserFormAction(Request $request, $thisP)
    {

        $userEnter = $thisP->getUser();
        $em = $thisP->getDoctrine()->getManager();
        $userId = $userEnter->getId();
        if ($userEnter->getRoles()[0] === 'ROLE_USER') {
            /** @var UserDealer $user */
            $user = $em->getRepository('AppBundle:UserDealer')->find($userId);
            $editForm = $thisP->createForm('AppBundle\Form\UserDealerType', $user);
        } else if ($userEnter->getRoles()[0] === 'ROLE_MANAGER') {
            $user = $em->getRepository('AppBundle:UserManager')->find($userId);
            $editForm = $thisP->createForm('AppBundle\Form\EditUserManagerWithFotoType', $user);
        } else {
            $user = $em->getRepository('AppBundle:User')->find($userId);
            $editForm = $thisP->createForm('AppBundle\Form\UserType', $user);
        }

        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $thisP->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }

        return $editForm;
    }


    /**
     * @Route("/pass_form", name="helper_index_pass_form")
     */
    public function getUserPassformAction(Request $request, $thisP )
    {
        $userEnter = $thisP->getUser();
        $em = $thisP->getDoctrine()->getManager();
        $form = $thisP->createForm(ResetPasswordType::class, $userEnter);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $oldPassword = $userEnter->getOldPassword();
            $encoder = $thisP->get('security.password_encoder');
            if (!$encoder->isPasswordValid($userEnter, $oldPassword)) {
                return new JsonResponse('The old password you have given is incorrect. Please try again.');
            }
            $encoder = $thisP->get('security.password_encoder');
            $userEnter->setPassword($encoder->encodePassword($userEnter, $userEnter->getPlainPassword()));
            $em->persist($userEnter);
            $em->flush();
            return new JsonResponse('Your password has been successfully changed.');
        }

        return $form;
    }
}
