<?php


namespace AppBundle\Repository;

use AppBundle\Entity\Post;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use DateTime;


class PostRepository extends EntityRepository
{
    /**
     * @param int $page
     *
     * @return Pagerfanta
     */
    public function findLatest($page = 1)
    {

        $features = $this->findLastFeature();
        if (count($features) > 0) {
            $feature = $features[0]->getId();
            $query = $this->getEntityManager()
                ->createQuery('
                SELECT p
                FROM AppBundle:Post p
                WHERE p.publishedAt <= :now AND p.id != :feature
                ORDER BY p.publishedAt DESC
            ');
            $query->setParameters(['now' => new \DateTime(), 'feature' => $feature]);
        } else {
            $query = $this->getEntityManager()
                ->createQuery('
                SELECT p
                FROM AppBundle:Post p
                WHERE p.publishedAt <= :now 
                ORDER BY p.publishedAt DESC
            ');
            $query->setParameter('now', new \DateTime());
        }


        return $this->createPaginator($query, $page);
    }

    public function findLatestByType($page = 1, $type)
    {
        $query = $this->getEntityManager()
            ->createQuery('
                SELECT p
                FROM AppBundle:Post p
                WHERE p.publishedAt <= :now AND p.type = :type
                ORDER BY p.publishedAt DESC
            ')
            ->setParameters(['now' => new \DateTime(), 'type' => $type]);

        return $this->createPaginator($query, $page);
    }

    public function findLastFeature()
    {
        $type = 'feature';
        $query = $this->getEntityManager()
            ->createQuery('
                SELECT p
                FROM AppBundle:Post p
                WHERE p.publishedAt <= :now AND p.type = :type
                ORDER BY p.publishedAt DESC
            ')
            ->setParameters(['now' => new \DateTime(), 'type' => $type]);

        return $query->getResult();
    }

    private function createPaginator(Query $query, $page)
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage(Post::NUM_ITEMS);
        $paginator->setCurrentPage($page);

        return $paginator;
    }

    public function findAllNine()
    {
        return $this->findBy(array(), array('publishedAt' => 'DESC'), 9);
    }

    public function findLatestByCategory($category)
    {
//        var_dump($category);die();
        $query = $this->findBy(array('category' => $category), array('publishedAt' => 'DESC'), 3);
//        var_dump(count($query));die();
        return $query;
//        $query = $this->getEntityManager()
//            ->createQuery('
//                SELECT p
//                FROM AppBundle:Post p
//                WHERE p.publishedAt <= :now AND p.category = :category
//                ORDER BY p.publishedAt DESC
//            ')
//            ->setParameters(['now' => new \DateTime(), 'category' => $category])
////            ->setFirstResult(3)
//            ->getResult();
        //   return $this->findBy(array(), array('publishedAt' => 'DESC'), 9);
        var_dump($query);
        die();
        return $query;
    }
}
