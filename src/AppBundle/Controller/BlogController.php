<?php


namespace AppBundle\Controller;


use AppBundle\Entity\Post;
use AppBundle\Events;

use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller used to manage blog contents in the public part of the site.
 *
 * @Route("/blog")
 * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER') or has_role('ROLE_USER') ")
 */
class BlogController extends Controller
{
    /**
     * @Route("/", defaults={"page": "1", "_format"="html"}, name="blog_index")
     * @Route("/rss.xml", defaults={"page": "1", "_format"="xml"}, name="blog_rss")
     * @Route("/page/{page}", defaults={"_format"="html"}, requirements={"page": "[1-9]\d*"}, name="blog_index_paginated")
     *
     *
     * @Cache(smaxage="10")
     */
    public function indexAction(Request $request, $page, $_format)
    {
        $userEnter = $this->getUser();
        $type = $request->get('type');
        if ($type === 'news') {
            $posts = $this->getDoctrine()->getRepository(Post::class)->findLatestByType($page, $type);
        } elseif ($type === 'promotion') {
            $posts = $this->getDoctrine()->getRepository(Post::class)->findLatestByType($page, $type);
        } else {
            $posts = $this->getDoctrine()->getRepository(Post::class)->findLatest($page);
        }
        $features = $this->getDoctrine()->getRepository(Post::class)->findLastFeature();
        $helper = $this->get('app_helper');
        $editForm = $helper->getUserFormAction($request, $this);
        $formPass = $helper->getUserPassformAction($request, $this);

        $feature = null;

        if (count($features) > 0 and $page == 1) {
            $feature = $features[0];
        }
        // Every template name also has two extensions that specify the format and
        // engine for that template.
        // See https://symfony.com/doc/current/templating.html#template-suffix
        return $this->render('blog/index.' . $_format . '.twig', ['posts' => $posts,
            'edit_form' => $editForm->createView(),
            'user' => $userEnter,
            'feature' => $feature,
            'form_pass' => $formPass->createView()]);
    }

    /**
     * @Route("/posts/{slug}", name="blog_post")
     * @Method("GET")
     *
     */
    public function postShowAction(Request $request, Post $post)
    {
        // Symfony provides a function called 'dump()' which is an improved version
        // of the 'var_dump()' function. It's useful to quickly debug the contents
        // of any variable, but it's not available in the 'prod' environment to
        // prevent any leak of sensitive information.
        // This function can be used both in PHP files and Twig templates. The only
        // requirement is to have enabled the DebugBundle.

        $back = $request->headers->get('referer');

        $helper = $this->get('app_helper');
        $userEnter = $this->getUser();
        $editForm = $helper->getUserFormAction($request, $this);
        $formPass = $helper->getUserPassformAction($request, $this);
        $category = $post->getCategory();
        // var_dump($category);die();
        $latestArticles = $this->getDoctrine()->getRepository(Post::class)->findLatestByCategory($category);
//var_dump($latestArticles);die();
        if ('dev' === $this->getParameter('kernel.environment')) {
            dump($post, $this->get('security.token_storage')->getToken()->getUser(), new \DateTime());
        }

        return $this->render('blog/post_show.html.twig', ['post' => $post,
            'user' => $userEnter,
            'back' => $back,
            'edit_form' => $editForm->createView(),
            'form_pass' => $formPass->createView(),
            'articles' => $latestArticles]);
    }


    /**
     *
     * @Route("/send/comment", name="blog_send_comment")
     */
    public function sendCommentAction(Request $request)
    {
        $mail = $request->get('mail');
        $message = $request->get('message');
        $article = $request->get('article');
        $toEmail = $this->container->getParameter('mailer_user');

        try {
            $this->get('app_mailer')->sendCommentToArticle($toEmail, $article, $message, $mail);
            return new JsonResponse('Comment sent.');
        } catch (Exception $e) {
            return new JsonResponse('Error sending comment. Please try again.');
        }

    }


}
