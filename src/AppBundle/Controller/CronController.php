<?php


namespace AppBundle\Controller;

use AppBundle\Entity\Currency;
use AppBundle\Entity\User;
use AppBundle\Form\ResetPasswordType;
use AppBundle\Utils\SendNotification;
use AppBundle\Utils\Util;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\UserDealer;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Date;

/**
 * @property  container
 * @Route("/cron/run")
 */
class CronController extends Controller
{

    /**
     *
     * @Route("/check/expiredDate", name="check_expired_date")
     */
    public function checkExpiredDateAction()
    {
        $expiredDate1 = $this->container->getParameter('expired_date1');
        $expiredDate2 = $this->container->getParameter('expired_date2');
        $em = $this->getDoctrine()->getManager();

        $dealers = $em->getRepository('AppBundle:UserDealer')->findAll();
        /** @var UserDealer $dealer */
        foreach ($dealers as $dealer) {

            $statusNot = $dealer->changeStatus();
            if ($statusNot !== null) {
                $sendNotification = new SendNotification();
                /** @var User $admin */
                $admin = $em->getRepository('AppBundle:User')->findByRole('["ROLE_ADMIN"]');
                // notification for admin that dealer change status
                $notificationA = $sendNotification->send($admin[0]->getNotificationToken(), $admin[0]->getId(), $statusNot->getTitle(), $statusNot->getMessage());
                $notificationA->setUsers($admin[0]);
                // notification for manager that dealer change status
                $notificationM = $sendNotification->send($dealer->getUserManager()->getNotificationToken(), $admin[0]->getId(), $statusNot->getTitle(), $statusNot->getMessage());
                $notificationM->setUsers($dealer->getUserManager());
                // notification for dealer that dealer change status
                $notificationD = $sendNotification->send($dealer->getNotificationToken(), $dealer->getId(), $statusNot->getTitle(), $statusNot->getMessage());
                $notificationD->setUsers($dealer);
                $em->persist($notificationA);
                $em->persist($notificationM);
                $em->persist($notificationD);
                $em->flush();

            }

            $em->persist($dealer);
            $em->flush();
            $expiredDate = $dealer->calcExpiredDate();

            if ($expiredDate === $expiredDate1) {
                $sendNotification = new SendNotification();
                /** @var User $admin */
                $admin = $em->getRepository('AppBundle:User')->findByRole('["ROLE_ADMIN"]');
                // notification for admin that dealer change status
                $notificationA = $sendNotification
                    ->send($admin[0]->getNotificationToken(), $admin[0]->getId(), $dealer->getCompany(), 'Status will expire in ' . $expiredDate1);
                $notificationA->setUsers($admin[0]);
                // notification for manager that dealer change status
                $notificationM = $sendNotification
                    ->send($dealer->getUserManager()->getNotificationToken(), $admin[0]->getId(), $dealer->getCompany(), 'Status will expire in ' . $expiredDate1);
                $notificationM->setUsers($dealer->getUserManager());
                // notification for dealer that dealer change status
                $notificationD = $sendNotification
                    ->send($dealer->getNotificationToken(), $dealer->getId(), $dealer->getCompany(), 'Status will expire in  ' . $expiredDate1);
                $notificationD->setUsers($dealer);
                $em->persist($notificationA);
                $em->persist($notificationM);
                $em->persist($notificationD);
                $em->flush();
                $discount = User::DISCOUNT_OF_STATUS[$dealer->getStatus()];
                $status = $dealer->getStatus();
                $expiredTime = $dealer->calcExpiredTime();

                $data = date("Y-m-d H:m:s", strtotime("-60 day"));
                $dateT = new \DateTime($data);
                date_add($dateT, date_interval_create_from_date_string($expiredDate1));
                date_add($dateT, date_interval_create_from_date_string('1 day'));
                $dateT = $dateT->format('Y-m-d H:i:s');
                $newstatus = $dealer->getFutureStatus($dateT);
                $newdiscount = User::DISCOUNT_OF_STATUS[$newstatus];

                $this->get('app_mailer')->sendStatusChange($dealer->getEmail(), $expiredTime, $dealer->getUsername(),
                    $dealer->getCompany(), $status, $newstatus, $discount, $newdiscount);
            } elseif ($expiredDate === $expiredDate2) {
                $sendNotification = new SendNotification();
                /** @var User $admin */
                $admin = $em->getRepository('AppBundle:User')->findByRole('["ROLE_ADMIN"]');
                // notification for admin that dealer change status
                $notificationA = $sendNotification->send($admin[0]->getNotificationToken(), $admin[0]->getId(), $dealer->getCompany(), 'Status will expire in ' . $expiredDate2);
                $notificationA->setUsers($admin[0]);
                // notification for manager that dealer change status
                $notificationM = $sendNotification->send($dealer->getUserManager()->getNotificationToken(), $admin[0]->getId(), $dealer->getCompany(), 'Status will expire in ' . $expiredDate2);
                $notificationM->setUsers($dealer->getUserManager());
                // notification for dealer that dealer change status
                $notificationD = $sendNotification->send($dealer->getNotificationToken(), $dealer->getId(), $dealer->getCompany(), 'Status will expire in ' . $expiredDate2);
                $notificationD->setUsers($dealer);
                $em->persist($notificationA);
                $em->persist($notificationM);
                $em->persist($notificationD);
                $em->flush();
            }
        }
        return new JsonResponse('Cron job was successfully done.');
    }


    /**
     *
     * @Route("/update/currency", name="update_currency")
     */
    function updateCurrency()
    {
        $em = $this->getDoctrine()->getManager();
        $currencies = $em->getRepository('AppBundle:Currency')->findAll();
        $util = new Util();
        $this->container->getParameter('expired_date1');
        $exchangeUrl = $this->container->getParameter('exchange_api');;

        $sendReques = $util->HTTPQuery($exchangeUrl);
        $sendReques = json_decode($sendReques, true);
        $currenciesApi = $sendReques['rates'];
        /** @var Currency $currency */
        foreach ($currencies as $currency) {
            $code = $currency->getCode();
            if (($code !== 'USD' || $code !== 'AED') && isset($currenciesApi[$code])) {
                $currency->setRate($currenciesApi[$code]);
                $em->persist($currency);
                $em->flush();
            }
        }

        return new JsonResponse('Cron job was successfully done.');
    }


}
