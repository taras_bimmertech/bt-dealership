<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CurrencyType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Name',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Name'
                )
            ))
            ->add('code', TextType::class, array(
                'label' => 'Code',

                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Code'
                )
            ))
            ->add('symbol', TextType::class, array(
                'label' => 'Symbol',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Symbol'
                )
            ))
            ->add('rate', NumberType::class, array(
                'label' => 'Rate',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Rate'
                )
            ))
            ->add('priority', NumberType::class, array(
                'label' => 'Priority',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Priority'
                )
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Currency'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_currency';
    }


}
