<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use libphonenumber\PhoneNumberFormat;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewUserDealerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('company', TextType::class, array(
                'label' => 'Company name',
                'attr' => array(
                    'class' => 'form-control required',
                    'placeholder' => 'Company name'
                )
            ))
            ->add('address', TextType::class, array(
                'label' => 'Address',
                'attr' => array(
                    'class' => 'form-control required',
                    'placeholder' => 'Address'
                )
            ))
            ->add('city', TextType::class, array(
                'label' => 'City',
                'attr' => array(
                    'class' => 'form-control required',
                    'placeholder' => 'City'
                )
            ))
            ->add('zip', TextType::class, array(
                'label' => 'ZIP/Postal code',
                'attr' => array(
                    'class' => 'form-control required',
                    'placeholder' => 'ZIP/Postal code'
                )
            ))
//            ->add('country', TextType::class, array(
//                'label' => 'Country',
//                'attr' => array(
//                    'class' => 'form-control required',
//                    'placeholder' => 'Country '
//                )
//            ))

            ->add('country', ChoiceType::class, array(
                'label' => 'Country ',
                'choices' => array(
                    'Choose a Country...' => '',
                    'Afghanistan' => 'Afghanistan',
                    'Albania' => 'Albania',
                    'Algeria' => 'Algeria',
                    'Andorra' => 'Andorra',
                    'Angola' => 'Angola',
                    'Antigua and Barbuda' => 'Antigua and Barbuda',
                    'Argentina' => 'Argentina',
                    'Armenia' => 'Armenia',
                    'Aruba' => 'Aruba',
                    'Australia' => 'Australia',
                    'Austria' => 'Austria',
                    'Azerbaijan' => 'Azerbaijan',
                    'Bahamas, The' => 'Bahamas, The',
                    'Bahrain' => 'Bahrain',
                    'Bangladesh' => 'Bangladesh',
                    'Barbados' => 'Barbados',
                    'Belarus' => 'Belarus',
                    'Belgium' => 'Belgium',
                    'Belize' => 'Belize',
                    'Benin' => 'Benin',
                    'Bhutan' => 'Bhutan',
                    'Bolivia' => 'Bolivia',
                    'Bosnia and Herzegovina' => 'Bosnia and Herzegovina',
                    'Botswana' => 'Botswana',
                    'Brazil' => 'Brazil',
                    'Brunei' => 'Brunei',
                    'Bulgaria' => 'Bulgaria',
                    'Burkina Faso' => 'Burkina Faso',
                    'Burma' => 'Burma',
                    'Burundi' => 'Burundi',
                    'Cambodia' => 'Cambodia',
                    'Cameroon' => 'Cameroon',
                    'Canada' => 'Canada',
                    'Cabo Verde' => 'Cabo Verde',
                    'Central African Republic' => 'Central African Republic',
                    'Chad' => 'Chad',
                    'Chile' => 'Chile',
                    'China' => 'China',
                    'Colombia' => 'Colombia',
                    'Comoros' => 'Comoros',
                    'Congo, Democratic Republic of the' => 'Congo, Democratic Republic of the',
                    'Congo, Republic of the' => 'Congo, Republic of the',
                    'Costa Rica' => 'Costa Rica',
                    'Cote d Ivoire' => 'Cote d Ivoire',
                    'Croatia' => 'Croatia',
                    'Cuba' => 'Cuba',
                    'Curacao' => 'Curacao',
                    'Cyprus' => 'Cyprus',
                    'Czechia' => 'Czechia',
                    'Denmark' => 'Denmark',
                    'Djibouti' => 'Djibouti',
                    'Dominica' => 'Dominica',
                    'Dominican Republic' => 'Dominican Republic',
                    'East Timor' => 'East Timor',
                    'Ecuador' => 'Ecuador',
                    'Egypt' => 'Egypt',
                    'El Salvador' => 'El Salvador',
                    'Equatorial Guinea' => 'Equatorial Guinea',
                    'Eritrea' => 'Eritrea',
                    'Estonia' => 'Estonia',
                    'Ethiopia' => 'Ethiopia',
                    'Fiji' => 'Fiji',
                    'Finland' => 'Finland',
                    'France' => 'France',
                    'Gabon' => 'Gabon',
                    'Gambia' => 'Gambia',
                    'Georgia' => 'Georgia',
                    'Germany' => 'Germany',
                    'Ghana' => 'Ghana',
                    'Greece' => 'Greece',
                    'Grenada' => 'Grenada',
                    'Guatemala' => 'Guatemala',
                    'Guinea' => 'Guinea',
                    'Guinea-Bissau' => 'Guinea-Bissau',
                    'Guyana' => 'Guyana',
                    'Haiti' => 'Haiti',
                    'Holy See' => 'Holy See',
                    'Honduras' => 'Honduras',
                    'Hong Kong' => 'Hong Kong',
                    'Hungary' => 'Hungary',
                    'Iceland' => 'Iceland',
                    'India' => 'India',
                    'Indonesia' => 'Indonesia',
                    'Iran' => 'Iran',
                    'Iraq' => 'Iraq',
                    'Ireland' => 'Ireland',
                    'Israel' => 'Israel',
                    'Italy' => 'Italy',
                    'Jamaica' => 'Jamaica',
                    'Japan' => 'Japan',
                    'Jordan' => 'Jordan',
                    'Kazakhstan' => 'Kazakhstan',
                    'Kenya' => 'Kenya',
                    'Kiribati' => 'Kiribati',
                    'Korea, North' => 'Korea, North',
                    'Korea, South' => 'Korea, South',
                    'Kosovo' => 'Kosovo',
                    'Kuwait' => 'Kuwait',
                    'Kyrgyzstan' => 'Kyrgyzstan',
                    'Laos' => 'Laos',
                    'Latvia' => 'Latvia',
                    'Lebanon' => 'Lebanon',
                    'Lesotho' => 'Lesotho',
                    'Liberia' => 'Liberia',
                    'Libya' => 'Libya',
                    'Liechtenstein' => 'Liechtenstein',
                    'Lithuania' => 'Lithuania',
                    'Luxembourg' => 'Luxembourg',
                    'Macau' => 'Macau',
                    'Macedonia' => 'Macedonia',
                    'Madagascar' => 'Madagascar',
                    'Malawi' => 'Malawi',
                    'Malaysia' => 'Malaysia',
                    'Maldives' => 'Maldives',
                    'Mali' => 'Mali',
                    'Malta' => 'Malta',
                    'Marshall Islands' => 'Marshall Islands',
                    'Mauritania' => 'Mauritania',
                    'Mauritius' => 'Mauritius',
                    'Mexico' => 'Mexico',
                    'Micronesia' => 'Micronesia',
                    'Moldova' => 'Moldova',
                    'Monaco' => 'Monaco',
                    'Mongolia' => 'Mongolia',
                    'Montenegro' => 'Montenegro',
                    'Morocco' => 'Morocco',
                    'Mozambique' => 'Mozambique',
                    'Namibia' => 'Namibia',
                    'Nauru' => 'Nauru',
                    'Nepal' => 'Nepal',
                    'Netherlands' => 'Netherlands',
                    'New Zealand' => 'New Zealand',
                    'Nicaragua' => 'Nicaragua',
                    'Niger' => 'Niger',
                    'Nigeria' => 'Nigeria',
                    'North Korea' => 'North Korea',
                    'Norway' => 'Norway',
                    'Oman' => 'Oman',
                    'Pakistan' => 'Pakistan',
                    'Palau' => 'Palau',
                    'Palestinian Territories' => 'Palestinian Territories',
                    'Panama' => 'Panama',
                    'Papua New Guinea' => 'Papua New Guinea',
                    'Paraguay' => 'Paraguay',
                    'Peru' => 'Peru',
                    'Philippines' => 'Philippines',
                    'Poland' => 'Poland',
                    'Portugal' => 'Portugal',
                    'Qatar' => 'Qatar',
                    'Romania' => 'Romania',
                    'Russia' => 'Russia',
                    'Rwanda' => 'Rwanda',
                    'Saint Kitts and Nevis' => 'Saint Kitts and Nevis',
                    'Saint Lucia' => 'Saint Lucia',
                    'Saint Vincent and the Grenadines' => 'Saint Vincent and the Grenadines',
                    'Samoa' => 'Samoa',
                    'San Marino' => 'San Marino',
                    'Sao Tome and Principe' => 'Sao Tome and Principe',
                    'Saudi Arabia' => 'Saudi Arabia',
                    'Senegal' => 'Senegal',
                    'Serbia' => 'Serbia',
                    'Seychelles' => 'Seychelles',
                    'Sierra Leone' => 'Sierra Leone',
                    'Singapore' => 'Singapore',
                    'Sint Maarten' => 'Sint Maarten',
                    'Slovakia' => 'Slovakia',
                    'Slovenia' => 'Slovenia',
                    'Solomon Islands' => 'Solomon Islands',
                    'Somalia' => 'Somalia',
                    'South Africa' => 'South Africa',
                    'South Korea' => 'South Korea',
                    'South Sudan' => 'South Sudan',
                    'Spain' => 'Spain',
                    'Sri Lanka' => 'Sri Lanka',
                    'Sudan' => 'Sudan',
                    'Suriname' => 'Suriname',
                    'Swaziland' => 'Swaziland',
                    'Sweden' => 'Sweden',
                    'Switzerland' => 'Switzerland',
                    'Syria' => 'Syria',
                    'Taiwan' => 'Taiwan',
                    'Tajikistan' => 'Tajikistan',
                    'Tanzania' => 'Tanzania',
                    'Thailand' => 'Thailand',
                    'Timor-Leste' => 'Timor-Leste',
                    'Togo' => 'Togo',
                    'Tonga' => 'Tonga',
                    'Trinidad and Tobago' => 'Trinidad and Tobago',
                    'Tunisia' => 'Tunisia',
                    'Turkey' => 'Turkey',
                    'Turkmenistan' => 'Turkmenistan',
                    'Tuvalu' => 'Tuvalu',
                    'Uganda' => 'Uganda',
                    'Ukraine' => 'Ukraine',
                    'United Arab Emirates' => 'United Arab Emirates',
                    'United Kingdom' => 'United Kingdom',
                    'United States' => 'United States',
                    'Uruguay' => 'Uruguay',
                    'Uzbekistan' => 'Uzbekistan',
                    'Vanuatu' => 'Vanuatu',
                    'Venezuela' => 'Venezuela',
                    'Vietnam' => 'Vietnam',
                    'Yemen' => 'Yemen',
                    'Zambia' => 'Zambia',
                    'Zimbabwe' => 'Zimbabwe',
                ),
                'attr' => array(
                    'class' => 'form-control required chosen-select',
                    'data-placeholder' => 'Choose a Country...'
                )
            ))
            ->add('state', TextType::class, array(
                'label' => 'State/Region',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control ',
                    'placeholder' => 'State/Region'
                )
            ))
            ->add('username', TextType::class, array(
                'label' => 'Contact Person',
                'attr' => array(
                    'class' => 'form-control required',
                    'placeholder' => 'Contact Person '
                )
            ))
            ->add('email', EmailType::class, array(
                'label' => 'E-mail address',
                'attr' => array(
                    'class' => 'form-control required',
                    'placeholder' => 'E-mail address'
                )
            ))
            ->add('phone', TextType::class, array(
                'label' => 'Phone number',
                'attr' => array(
                    'class' => 'form-control required',
                    'placeholder' => 'Phone number '
                )
            ))
            ->add('supportEmail', EmailType::class, array(
                'label' => 'Support e-mail address',
                'attr' => array(
                    'class' => 'form-control required',
                    'placeholder' => 'Support e-mail address'
                )
            ))
            ->add('website', TextType::class, array(
                'label' => 'Website',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Website'
                )
            ))
            ->add('password', PasswordType::class, array(
                'invalid_message' => 'The password fields must match.',
                'required' => true,
                'label' => 'Password',
                'attr' => array(
                    'placeholder' => 'Password',
                    'class' => 'form-control required'
                )
            ))
            ->add('userManager', EntityType::class, [
                'class' => 'AppBundle:UserManager',
                'choice_label' => 'username',
                'label' => 'Manager',
                'placeholder' => 'Choose Manager',
                'required' => true,
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control required'),
                'query_builder' => function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('c')
                        ->select('c')
                        ->leftJoin('AppBundle:UserManager', 'co', 'WITH', 'co.username = c.username')
                        ->orderBy('co.username', 'ASC');
                }

            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\UserDealer'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_new_userdealer';
    }


}
