<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 17.04.2017
 * Time: 16:12
 */

namespace AppBundle\Utils;


class Util
{

    private $ch;

    public function __construct()
    {
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_HEADER, false);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($this->ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:11.0) Gecko/20100101 Firefox/11.0');
    }

// Method: POST, PUT, GET etc
// Data: array("param" => "value") ==> index.php?param=value
// headers : array('HeaderName: HeaderValue','HeaderName2: HeaderValue2'))

    function CallAPI($method, $url, $data = false, $headers = false)
    {
        $curl = curl_init();

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        if ($headers != false) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }


    public function HTTPQuery($url, $postdata = NULL, $headers = NULL, $cookies = NULL)
    {
        if ($postdata) {
            curl_setopt($this->ch, CURLOPT_POST, true);
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postdata);
        } else {
            curl_setopt($this->ch, CURLOPT_POST, false);
        }
        if ($headers) {
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);
        }
        if ($cookies) {
            curl_setopt($this->ch, CURLOPT_COOKIE, $cookies);
        }
        curl_setopt($this->ch, CURLOPT_URL, $url);
        $data = curl_exec($this->ch);
        return $data;
    }
}