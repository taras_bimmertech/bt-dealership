var addons = {
    'smarthdflex': 'SmartView HD Flex',
    'front': 'Front Camera',
    'dynamiclines': 'Dynamic Parking Lines',
    'toolkit': 'Trim Tool Kit',
    'interface_hd': 'Multimedia Interface HD upgrade',
    'smarthd': 'SmartView HD',
    'vividscreen': 'VividScreen',
    'vividscreen_smarthd': 'VividScreen+SmartView HD',
    'warranty': 'Extended Warranty',
    'rear_cam': 'Rear View Camera'
};

var mainUrl = window.location.origin;

var blockAccount = $('#account');
var accountName = $('#showAccount');
$(window).load(function () {
    $('#loading').hide();
});


(function ($) {
    $(document).ready(function () {

        changeTypeToNumber();
        hljs.initHighlightingOnLoad();
        getNotifications();
        // login();
        showForgot();
        hideForgot();
        showAccount();
        showChangePass();
        hideAccount();
        showEditAcoount();
        showCheckProduc();
        showCheckMMI();
        showWhatCheck();
        spotErrorForm();
        changeAccountPass();
        editDownloadFiler();
        showNewDelaer();
        showEditDealer();
        showNewOrder();
        showChangeManeger();
        showCreateFile();
        deleteDealer();
        // selectAlldealers();
        showEditOrder();
        showCreateManager();
        deleteDealerFormShow();
        newDealerFormShow();
        newManagerFormShow();
        deleteManagerFormShow();
        vinCheck();
        askDelManager();
        askDelDealer();
        seeRecentlyAllDealers();
        showDealersBtn();
        showSecurityMainForm();
        showProfileInfoForm();
        showManagerDealers();
        squarePhoto('.profileFotoDesktop');
        showTableExport('.table');
        showExportData();
        showConfirmDelete();
        managerListShow();
        squareNews();
        latestNews();
        sendComment();
        setFilter();
        sendToManager();
        showOrder();
        showDealerCoupon();
        vinValidate();
        textToALL();
        tinymce.init({
            selector: '#appbundle_post_content',
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste imagetools"
            ],
            setup: function (editor) {
                editor.on('change', function () {
                    editor.save();
                });
            },
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        });

        tinymce.init({
            selector: '#appbundle_post_edit_content',
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste imagetools"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        });


        // $('body').niceScroll();
        // capitalizeFirstLetter(string);
        // Datetime picker initialization.
        // See http://eonasdan.github.io/bootstrap-datetimepicker/
        loadDatePicker();
        $(".chosen-select").chosen({
            width: "100%",
            no_results_text: "Oops, nothing found!",
            placeholder_text_single: 'Choose from list'
        });

        // Bootstrap-tagsinput initialization
        // http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/
        var $input = $('input[data-toggle="tagsinput"]');
        if ($input.length) {
            var source = new Bloodhound({
                local: $input.data('tags'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                datumTokenizer: Bloodhound.tokenizers.whitespace
            });
            $input.tagsinput({
                trimValue: true,
                focusClass: 'focus',
                typeaheadjs: {
                    name: 'tags',
                    source: source
                }
            });
        }


        $('#spinner').remove();

    });

    // Handling the modal confirmation message.
    $(document).on('submit', 'form[data-confirmation]', function (event) {
        var $form = $(this),
            $confirm = $('#confirmationModal');

        if ($confirm.data('result') !== 'yes') {
            //cancel submit event
            event.preventDefault();

            $confirm
                .off('click', '#btnYes')
                .on('click', '#btnYes', function () {
                    $confirm.data('result', 'yes');
                    $form.find('input[type="submit"]').attr('disabled', 'disabled');
                    $form.submit();
                })
                .modal('show');
        }
    });

    $('#fogotForm').submit(function (e) {
        e.preventDefault();
        var email = $('#usernameFogot').val();
        // $('.checkForgot').css("display","block");
        var mesBlock = $('.checkForgot');
        mesBlock.css("display", "block");

        $.ajax({
            url: 'api/resetting/request',
            method: 'POST',
            timeout: 5000,
            data: {
                email: email
            },
            error: function () {
                console.log('error');
            },
            success: function (data) {
                $(mesBlock).find('p').text(data);
            }
        });
    });

    function askDelManager() {
        var btn = $('#mYesBtn');
        $('#appbundle_delete_usermanager_manager option').click(function (e) {
            e.preventDefault();
            btn.hide();
        });
        $('#mDelBtn').click(function (e) {
            btn.hide();
            e.preventDefault();
            // $.playSound(mainUrl + '/sound/sure.wav');
            var v = $('#appbundle_delete_usermanager_manager').val();
            if (v != '') {
                btn.hide();
                btn.show();
            }
        });

    }

    function seeRecentlyAllDealers() {
        $('.seeRecentlyAllDealers').click(function (e) {
            e.preventDefault();
            var btnAll = $('#seeAllDealers');
            var btnRecently = $('#seeRecentlyDealers');
            var resently = $(this).data('resently');
            var showDealers = $('#showDealers');
            showDealers.removeClass('tableOverflow');

            showDealers.find('tbody').getNiceScroll().remove();
            $.ajax({
                url: '/admin/account/all/dealers',
                method: 'GET',
                timeout: 5000,
                data: {
                    resently: resently
                },
                error: function () {
                    console.log('error');
                },
                success: function (data) {
                    // showDealers.find('table').html(data);
                    showDealers.html(data);
                    var total = 0;
                    $('.dealerValue').each(function (index, elem) {

                        total += parseInt($(elem).text());

                    });
                    btnAll.removeClass('allDealersOptActive');
                    btnAll.addClass('allDealersOptDisable');
                    btnRecently.removeClass('allDealersOptActive');
                    btnRecently.removeClass('allDealersOptDisable');
                    $('#totalOrderSum').html('$ ' + total);
                    // added/remove color scroll
                    $('[data-toggle="table"]').bootstrapTable();
                    setFilter();

                    if (!resently) {
                        showDealers.find('tbody').niceScroll({
                            cursorcolor: "#00838f",
                            cursorwidth: "15px",
                            background: "#e0e0e0",
                            cursorborder: "1px solid #e0e0e0",
                            cursorborderradius: 0,
                            autohidemode: false

                        })
                        //     .rail.css({
                        //     "z-index": 9999,
                        //     "margin-left": "2px"
                        // })
                        ;
                        showDealers.find('tbody').addClass('tableOverflow');
                    } else {
                        showDealers.find('tbody').getNiceScroll().remove();
                    }
                    textToALL();
                    showTableExport('.table');
                }
            });
        });
    }

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function showDealersBtn() {

        $('#seeAllDealersBtn').on("click", function () {

            $('#recentlyAddedDealersBtn').removeClass('hidden');
            $('#seeAllDealersBtn').addClass('hidden');
            $('#seeAllDealersP').removeClass('hidden');
            $('#recentlyAddedDealersP').addClass('hidden');
        });
        $('#recentlyAddedDealersBtn').on("click", function () {
            $('#seeAllDealersBtn').removeClass('hidden');
            $('#seeAllDealersP').addClass('hidden');
            $('#recentlyAddedDealersP').removeClass('hidden');
            $('#recentlyAddedDealersBtn').addClass('hidden');
        });
    }

    function showManagerDealers() {

        $('.seeManagerDealers').click(function (e) {
            e.preventDefault();
            var resently = $(this).data('resently');
            var showDealers = $('#showDealers');
            showDealers.find('tbody').getNiceScroll().remove();
            $.ajax({
                url: '/manager/account/all/dealers',
                method: 'GET',
                timeout: 5000,
                data: {
                    resently: resently
                },
                error: function () {
                    console.log('error');
                },
                success: function (data) {


                    showDealers.html(data);
                    var total = 0;
                    $('.dealerValue').each(function (index, elem) {
                        total += parseInt($(elem).text());
                    });
                    $('#managerRecDealers').on("click", function () {
                        $(this).addClass('allDealersOptActive');
                        $('#managerAllDealers').removeClass('allDealersOptActive ');
                    });
                    $('#managerAllDealers').on("click", function () {
                        $(this).addClass('allDealersOptActive');
                        $('#managerRecDealers').removeClass('allDealersOptActive ');
                    });
                    $('#totalOrderSum').html('$ ' + total);
                    // // added/remove color scroll
                    $('[data-toggle="table"]').bootstrapTable();
                    setFilter();
                    if (!resently) {
                        showDealers.find('tbody').niceScroll({
                            cursorcolor: "#00838f",
                            cursorwidth: "15px",
                            background: "#e0e0e0",
                            cursorborder: "1px solid #e0e0e0",
                            cursorborderradius: 0,
                            autohidemode: false

                        });
                        showDealers.find('tbody').addClass('tableOverflow');
                    } else {
                        showDealers.find('tbody').getNiceScroll().remove();
                    }
                    textToALL();
                    showTableExport('.table');
                }
            });
        });
    }

    function showSecurityMainForm() {
        var account = $('#accountMainForm');
        var security = $('#securityMainForm');
        $('#securityMainFormBtn').on("click", function () {
            account.css("display", "none");
            security.css("display", "block");
        });
        $('#accountMainFormBtn').on("click", function () {
            account.css("display", "block");
            security.css("display", "none");
        });
        $('#changeOldPassBtn').on("click", function () {
            $('#confirmPass').removeClass('hidden');
        });
    }

    function showProfileInfoForm() {
        var personal = $('#accountMainForm');
        var account = $('#securityMainForm');
        var editform = $('.editForm');
        // $('#personalInfoBtn').css({'background-color': 'transparent', 'border': '1px solid #00838f'});
        $('#personalInfoBtn').addClass('persInfoActive');
        $('#personalAccountBtn').removeClass('persInfoDsiable');
        $('#personalInfoBtn').on("click", function () {
            personal.css("display", "block");
            account.css("display", "none");
            editform.css("display", "none");
            $('#accountEditBtn').css("display", "block");
            $('#personalAccountBtn').removeClass('persInfoActive');
            $('#personalAccountBtn').addClass('persInfoDsiable');
            $(this).addClass('persInfoActive');
        });
        $('#personalAccountBtn').on("click", function () {
            account.css("display", "block");
            personal.css("display", "none");
            editform.css("display", "none");
            $('#accountForm').css("display", "none");
            $('#personalInfoBtn').removeClass('persInfoActive');
            $('#personalInfoBtn').removeClass('persInfoDisable');
            $(this).removeClass('persInfoDisable');
            $(this).addClass('persInfoActive');
        });
        $('#changeOldPassBtn').on("click", function () {
            $('#confirmPass').removeClass('hidden');
        });
    }

    $(document).click(function (event) {
        var mesBlock = $('.messageBlock');
        if (!$(event.target).closest(mesBlock).length) {
            if ($(mesBlock).is(":visible")) {
                hideMessage(mesBlock);
            }
        }
    });

})(window.jQuery);

function loadDatePicker() {
    $('[data-toggle="datetimepicker"]').datetimepicker({
        maxDate: 'now',
        widgetPositioning: {
            horizontal: 'left',
            vertical: 'bottom'
        },
        icons: {
            time: 'fa fa-clock-o',
            date: 'fa fa-calendar',
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down',
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-check-circle-o',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    });
}


function showForgot() {
    $('#showHide').click(function () {
        $('#disableBlock').removeClass('hidden');
        $('#fogotForm').fadeIn('slow');
        $('#authForm').css("display", "none");
        $('#disableBlock').css("display", "none");
    })
}
function hideForgot() {
    $('#hideForgot').click(function () {
        $('#disableBlock').addClass('hidden');
        $('#fogotForm').css("display", "none");
        $('#authForm').css("display", "block");
        $('.badPass').css("display", "none");
    })
}

function showMessage(block) {
    block.css("opacity", 1);
    $('.homeContaiter').addClass('hidebg fadeIn');
}
function hideMessage(block) {
    block.css("opacity", 0);
    $('.homeContaiter').removeClass('hidebg');
}


function askDelDealer() {
    var btn = $('#dYesBtn');
    $('#appbundle_delete_userdealer_dealer option').click(function (e) {
        e.preventDefault();
        btn.hide();
    });
    $('#dDelBtn').click(function (e) {
        btn.hide();
        e.preventDefault();
        // $.playSound(mainUrl + '/sound/sure.wav');
        var v = $('#appbundle_delete_userdealer_dealer').val();
        if (v != '') {
            btn.hide();
            btn.show();
        }
    });
}

function seeAllDealers() {
    $('#seeAllDealers').click(function (e) {
        e.preventDefault();
    });
}
//
function showAccount() {
    $('#showAccount').click(function (e) {
        e.preventDefault();
        // $.playSound(mainUrl + '/sound/showAccount.wav');
        blockAccount.removeClass('slideOutRight');
        blockAccount.removeClass('hidden');
        // $('#personalInfoBtn').css({'background-color': 'transparent', 'border': '1px solid #00838f'});
        $('#personalInfoBtn').addClass('persInfoActive');
        // $('#personalAccountBtn').css({'background-color': '#00bcd4', 'border': 'none'});
        $('#personalAccountBtn').addClass('persInfoDisable');
        $('#newDealerFormBtn').removeClass('persInfoDisable');
        $('#newDealerFormBtn').addClass('persInfoActive');
        $('#deleteDealerFormBtn').removeClass('persInfoActive');
        $('#deleteDealerFormBtn').addClass('persInfoDisable');
        $('#securityMainForm').css("display", "none");
        squarePhoto('.profileManagerFoto');

        closeAny('#account', '#showAccount', 'slideOutRight');
    })
}

function showNewDelaer() {
    $('#addNewDealer').click(function (e) {
        e.preventDefault();
        $('#addNewDealerBlock').removeClass('slideOutRight');
        $('#addNewDealerBlock').removeClass('hidden');

        closeAny('#addNewDealerBlock', '#addNewDealer', 'slideOutRight');
    })
}
function showNewOrder() {
    $('#newOrderBtn').click(function (e) {
        e.preventDefault();
        $('#newOrder').removeClass('slideOutRight');
        $('#newOrder').removeClass('hidden');
        closeAny('#newOrder', '#newOrderBtn', 'slideOutRight');
    })
}


function showDealerCoupon() {
    $('#dealerCouponBtn').click(function (e) {
        e.preventDefault();
        $('#dealerCouponBlock').removeClass('slideOutRight');
        $('#dealerCouponBlock').removeClass('hidden');
        closeAny('#dealerCouponBlock', '#dealerCouponBtn', 'slideOutRight');
    });

}
function showChangeManeger() {
    $('#changeManagerBtn').click(function (e) {
        e.preventDefault();
        $('#changeManager').removeClass('slideOutRight');
        $('#changeManager').removeClass('hidden');
        closeAny('#changeManager', '#changeManagerBtn', 'slideOutRight');
    })
}
function showCreateFile() {
    $('.createFileBlockBtn').click(function (e) {
        e.preventDefault();
        $('#newFileBlock').removeClass('slideOutDown');
        $('#newFileBlock').removeClass('hidden');
        closeAny('#newFileBlock', '.createFileBlockBtn', 'slideOutDown');
        $('.closeNewfile').click(function (e) {
            e.preventDefault();
            $('#newFileBlock').addClass('slideOutDown');
            setTimeout(function () {
                $('#newFileBlock').addClass('hidden');

            }, 1000)
        })
    })
}
function showCreateManager() {
    $('#addNewManagerBtn').click(function (e) {
        e.preventDefault();
        $('#addNewManager').removeClass('slideOutRight');
        $('#addNewManager').removeClass('hidden');

        closeAny('#addNewManager', '#addNewManagerBtn', 'slideOutRight');
    })
}


function hideAccount() {
    $('.closeAccount').click(function (e) {
        e.preventDefault();
        // $.playSound(mainUrl + '/sound/closeAccount.wav');
        blockAccount.addClass('slideOutRight');
        $('#addNewDealerBlock').addClass('slideOutRight');
        $('#editBlock').addClass('slideOutRight');
        $('#newOrder').addClass('slideOutRight');
        $('#changeManager').addClass('slideOutRight');
        $('#createFile').addClass('slideOutRight');
        $('#editOrder').addClass('slideOutRight');
        $('#addNewManager').addClass('slideOutRight');
        $('#editManager').addClass('slideOutRight');
        $('#dealerCouponBlock').addClass('slideOutRight');
        setTimeout(function () {
            blockAccount.addClass('hidden');
            $('#addNewDealerBlock').addClass('hidden');
            $('#editBlock').addClass('hidden');
            $('#newOrder').addClass('hidden');
            $('#changeManager').addClass('hidden');
            $('#createFile').addClass('hidden');
            $('#editOrder').addClass('hidden');
            $('#addNewManager').addClass('hidden');
            $('#editManager').addClass('hidden');
            $('#dealerCouponBlock').addClass('hidden');
            hideEditAcoount();
        }, 1000)

    })
}
function showChangePass() {
    $('#changePassBtn').click(function (e) {
        e.preventDefault();
        $('#accountForm').toggle("slow", function () {
        });
    });
}

function showEditAcoount() {
    $('#accountEditBtn').click(function (e) {
        e.preventDefault();
        $('.editForm').show();
        // $('#accountEditBtn').fadeOut();
        // $('.accountInfo').fadeOut();
        $('#accountEditBtn').css("display", "none");
        $('.accountInfo').css("display", "none");
    })
}


function hideEditAcoount() {
    $('.editForm').hide();
    $('#accountEditBtn').fadeIn();
    $('.accountInfo').fadeIn();
}

function showCheckProduc() {
    $('#checkProduct').click(function () {
        $('#checkProductBlock').show();
        $('#whatCheck').hide();
    })
}

function showWhatCheck() {
    $('.goBack').click(function () {
        $('#whatCheck').show();
        $('#checkProductBlock').hide();
        $('#checkMMIBlock').hide();
        $('.vincheckError').hide();
        $('.vincheckProducts').children('.row').remove();
        $('#printVincheck').hide();
    })
}
function showCheckMMI() {
    $('#checkMMI').click(function () {
        $('#checkMMIBlock').show();
        $('#whatCheck').hide();
    })
}

function getProducts(vin) {
    var productsBlock = $('.vincheckProducts');
    var detaisBlock = $('.vincheckDetails');
    $('.vincheckErrorProducts').hide();
    $('.prodDetailBlock').remove();

    var role = $('#userRole').data('role');
    var dealerRole = "ROLE_USER";
    var isDealer = dealerRole === role;
    var company = $('.dataCompany').data('company');
    if (!company) {
        company = 'BimmerTech';
    }
    productsBlock.children('.row').remove();
    productsBlock.html('<div><img src="../../web/img/loader.gif"> Finding  products</div>');

    $.ajax({
        url: 'check/products/vin',
        method: 'GET',
        timeout: 15000,
        data: {
            vin: vin,
            company: company
        },
        error: function () {
            productsBlock.html('');
            $('.vincheckErrorProducts').show().text('Error connecting to the server. Please try again.')
        },
        success: function (data) {
            $('.spotErrorBlock').show();
            productsBlock.html('');
            data = jQuery.parseJSON(data);
            // console.log(data);
            productsBlock.html('');
            detaisBlock.html('');
            if (data.error != null) {
                data = Object.values(data.error);
                $('.vincheckErrorProducts').show().text('No compatible products found for this VIN.');
            } else {
                var serverErrorBlock = '';
                if (data.serverError) {
                    serverErrorBlock = '<div class="row  animated flipInX">' + data.serverError + '</div>';
                }
                var model = data.vinData["E series"].split(' ');

                printVinCheck();

                detaisBlock.append('<div class="row "><h5><b>VIN DETAILS</b></h5></div>');
                detaisBlock.append('<div class="row padB5"><p class="invisible greyTxt">VIN DETAILS</p></div>');
                detaisBlock.append('<div class="row" id="longVin" data-model="' + model[0] + '">' + data.vinData["VIN"] + '</div>');
                detaisBlock.append('<div class="row" data-model="' + model[0] + '">Model: ' + model[0] + '</div>');
                detaisBlock.append('<div class="row">Series: ' + data.vinData["Series"] + '</div>');
                detaisBlock.append('<div class="row">Type: ' + data.vinData["Type"] + '</div>');
                detaisBlock.append('<div class="row">Prod. date: ' + data.vinData["Prod.date"] + '</div>');
                var html = $('.vincheckProducts');
                html.show();

                html.append('<div class="row"><h5><b>PRODUCTS COMPATIBLE WITH YOUR VIN</b></h5></div>');
                html.append('<div class="row padB5"><p class="greyTxt">If you want to order the product tick it.</p></div>');
                if (data.products) {
                    data.products.forEach(function (item, i, data) {
                        var show = true;
                        var priority = 100;
                        item.custom.forEach(function (p1, p2, p3) {
                            if (p1.error !== undefined) {
                                show = false;
                            }
                            if (p1.priority) {
                                priority = p1.priority;
                            }
                        });
                        if (show) {
                            var updown;
                            if (hasAddons(item.custom)) {
                                updown = "upDown";
                            }
                            else {
                                updown = "";
                            }
                            generateProdBlock(html, i, updown, item, isDealer);
                            var productDetail = generateProdDetail(i, item.name, 'product', priority);


                            $('#detailBlock' + i).addClass('hidden');
                            var detail = 'Contact us for more information.', price = 0, discount = null,
                                instalTime = 'Contact us';

                            item.custom.forEach(function (product, j, data) {
                                console.log(product);
                                if (product.details) {
                                    detail = product.details;
                                }
                                if (product.price) {
                                    price = product.price;
                                }
                                if (product.discount) {
                                    discount = product.discount;
                                }
                                if (product.time) {
                                    instalTime = product.time;
                                }
                            });

                            $.ajax({
                                url: 'get/discount',
                                method: 'GET',
                                timeout: 0,
                                data: {
                                    discount: discount
                                },
                                error: function (data) {
                                    console.error(data);
                                    console.error('fail discount');
                                },
                                success: function (dataDisc) {
                                    generateDetaiBlock(productDetail, i, detail, instalTime, price, dataDisc, isDealer);
                                    if (item.customObj) {
                                        priority = 100;
                                        $.each(item.customObj, function (index, addon) {
                                            if (addon['dealerPortalShow']) {

                                                var addonIndex = addon['key'] + i;
                                                var addonPrice = addon['price'];
                                                var addonName = addon['name'];
                                                var addonDetail = addon['details'];
                                                var addonInstallTime = addon['time'];
                                                addonDetail = (addonDetail) ? addonDetail : 'Contact us for more information.';
                                                addonInstallTime = (addonInstallTime) ? addonInstallTime : 'Contact us.';
                                                generateDetaiBlock(generateProdDetail(addonIndex, addonName, 'addon', priority), addonIndex, addonDetail, addonInstallTime, addonPrice, dataDisc, isDealer);
                                            }
                                        });
                                    }
                                    changeCurrency();
                                }
                            });
                        }
                    });
                }
                infoIconClick(0);

                $(".addonsWrap").sort(asc_sort).appendTo('.vincheckProducts');
                getMMI(vin);
                html.append(serverErrorBlock);
                $('#printVincheck').removeClass('hidden');
                $('.exchangeBlock').removeClass('hidden');
                upDownClick();
                if (isDealer) {
                    showOrder();
                    sendOrders();
                }

            }

        }
    });
}


function changeCurrency() {
    var currencyLink = $('.currency li a');
    var activeCurrecncyBlock = $('.currency').find(".active");
    var currentCurrency = activeCurrecncyBlock.data('currency');
    var currentRate = activeCurrecncyBlock.data('rate');
    if (currentCurrency === "USD") {
        currencyUSD(currentCurrency);
    } else {
        currencyCurrent(currentCurrency, currentRate);
    }
    changeExchangeFee();
    currencyLink.click(function (e) {
        e.preventDefault();
        var currentCurrency = $(this);
        // var itemBlock = currentCurrency.parent().parent().parent();
        currencyLink.removeClass('active');
        currentCurrency.addClass('active');
        var currency = currentCurrency.data('currency');
        var rate = currentCurrency.data('rate');
        var name = currentCurrency.data('name');
        $('.currency .dropdown-toggle').html(name + '<span class="caret"></span>');
        if (currency === "USD") {
            currencyUSD(currency);
        } else {
            currencyCurrent(currency, rate);
        }
    })
}

function currencyUSD(currency) {

    $('.exchangeFeeBlock ').addClass('hidden');
    $('.prodDetailBlock').each(function (index, value) {
        changeCurrecyPrice($(value), 1, currency);
        changeCurrencySymbol($(value), currency);
        $('.usdPrice').remove();
    });
    // exchangeToZero();
}

// function currencyEUR(currency) {
//     $('.exchangeFeeBlock ').removeClass('hidden');
//     if (eurRate) {
//         changeEUR(currency);
//     } else {
//         getEurCurrency().then(function (res) {
//             eurRate = res;
//             changeEUR(currency)
//         });
//     }
// }


function changeExchangeFee() {
    var exchangeInput = $('.exchangeFeeBlock  input');
    exchangeInput.change(function () {
        changeExchangeFeeNow(exchangeInput)
    })
}

function changeExchangeFeeNow(exchangeInput) {
    var inputValue = exchangeInput.val();

    if (inputValue > 10) {
        inputValue = 10;
        exchangeInput.val(10);
    } else if (inputValue < 0) {
        inputValue = 0;
        exchangeInput.val(0);
    }
    $('.RegularPrice').each(function (index, value) {
        var elm = $(value);
        var currencyPrice = parseFloat(elm.data('currencyPrice'));
        var exchangePrice = currencyPrice + ((currencyPrice * inputValue) / 100);
        exchangePrice = exchangePrice.round(2);
        elm.text(exchangePrice);
    })
}

function currencyCurrent(currency, rate) {
    $('.exchangeFeeBlock ').removeClass('hidden');
    $('.prodDetailBlock').each(function (index, value) {
        changeCurrecyPrice($(value), rate, currency);
        changeCurrencySymbol($(value), currency);
    });
    changeExchangeFeeNow($('.exchangeFeeBlock  input'));
}
//
// function changeEUR(currency) {
//     $('.prodDetailBlock').each(function (index, value) {
//         changeCurrecyPrice($(value), eurRate, currency);
//         changeCurrencySymbol($(value), currency);
//     });
//     changeExchangeFeeNow($('.exchangeFeeBlock  input'));
// }
//
// function getEurCurrency() {
//     return new Promise(function (resolve, reject) {
//         $.ajax({
//             url: 'get/currency',
//             method: 'GET',
//             timeout: 5000,
//             error: function () {
//                 console.error('error');
//             },
//             success: function (data) {
//                 return resolve(data);
//             }
//         });
//     });
// }


function changeCurrencySymbol(itemBlock, currency) {
    itemBlock.find('.currencySymbol').text(currency.toUpperCase());
}


function showExchangeText() {
    $('.exchangeFeeTextBtn').click(function (e) {
        e.preventDefault();
        $('.exchangeFeeText').toggleClass('hidden');
    })
}


function changeCurrecyPrice(itemBlock, rate, currency) {
    var yourPriceBlock = itemBlock.find('.yourPrice');
    var regularPriceBlock = itemBlock.find('.RegularPrice');
    var yourPriceUSD = yourPriceBlock.data('price');
    var regularPriceUSD = regularPriceBlock.data('price');
    var yourPrice = parseFloat(yourPriceUSD) * rate;
    yourPrice = yourPrice.round(2);
    yourPrice = yourPrice.toFixed(2);
    var regularPrice = parseFloat(regularPriceUSD) * rate;
    regularPrice = regularPrice.round(2);
    regularPrice = regularPrice.toFixed(2);
    yourPriceBlock.data('currencyPrice', yourPrice);
    regularPriceBlock.data('currencyPrice', regularPrice);
    yourPriceBlock.text(yourPrice);
    regularPriceBlock.text(regularPrice);
    if (!regularPriceBlock.parent().find('.usdPrice').length > 0) {
        yourPriceBlock.parent().append('<span class="usdPrice"> (' + yourPriceUSD + ' USD)</span>');
        regularPriceBlock.parent().append('<span class="usdPrice"> (' + regularPriceUSD + ' USD)</span>');
    }
    $('.rateValue').text(rate);
    $('.rateSymbol ').text(currency.toUpperCase());
}

Number.prototype.round = function (p) {
    p = p || 10;
    return parseFloat(this.toFixed(p));
};

function addons_sort(a, b) {
    return (parseInt(a.priority) > parseInt(b.priority)) ? 1 : -1;
}

function addonsSort(addons) {
    var arrayAddons = $.map(addons, function (value, index) {
        return [value];
    });
    arrayAddons = arrayAddons.sort(addons_sort);
    return arrayAddons;
}

// accending sort
function asc_sort(a, b) {
    return ($(b).text()) < ($(a).text()) ? 1 : -1;
}

// decending sort
function dec_sort(a, b) {
    return ($(b).text()) > ($(a).text()) ? 1 : -1;
}

function generateProdBlock(html, i, updown, item, isDealer) {

    html.append('<div class="row addonsWrap "id="addonsWrap' + i + '"><b><span class="' + updown + ' pointable"></span></b>' +
        '</span><b class="prodTitle" onclick=infoIconClick(' + i + ')>' + item.name + '</b></div>');
    var htmlAddon = html.find("#addonsWrap" + i);
    if (isDealer) {
        var inputCustomDatas = generateProductData(item['custom'], false);
        htmlAddon.append('<input type="checkbox"   id="check' + i + '" data-product="' + item.name + '" class="productCheckBox" ' + inputCustomDatas + ' onclick=infoIconClick(' + i + ')>' +
            '<label for="check' + i + '"></label>');
    }
    htmlAddon.last().attr('id', "detailBlockBtn" + i);
    if (item.customObj) {
        var addons = addonsSort(item.customObj);
        $.each(addons, function (index, addon) {
            if (addon['dealerPortalShow']) {
                var isAddonDepend = checkAddonsDeped(item.custom) ? 'disabled' : '';
                htmlAddon.append('<div  class="row vincheckAddon  hidden animated flipInX " id="addon' + addon['key'] + i + '" onclick=infoIconClick("' + addon['key'] + i + '")>' + addon['name'] + ' </div>');
                if (isDealer) {
                    var inputCustomObjDatas = generateProductData(addon, true);
                    var htmldealerAddon = htmlAddon.find('#addon' + addon['key'] + i);
                    if (addon['price'] != 0) {
                        htmldealerAddon.append('<input type="checkbox" ' + isAddonDepend + '  ' + inputCustomObjDatas + '  id="check' + addon['key'] + i + '" data-product="' + addon['name'] + ' " class="productCheckBox" onclick=infoIconClick("' + addon['key'] + i + '")>' +
                            '<label for="check' + addon['key'] + i + '"></label>');
                    } else {
                        htmldealerAddon.append('<div class="pull-right">Included</div>');
                    }
                }
            }
        });
    }
}


function checkAddonsDeped(custom) {
    var hasAddonDepend = false;
    $.each(custom, function (index, value) {
        if (value['addonsDepend']) {
            hasAddonDepend = true;
        }
    });
    return hasAddonDepend;
}

function generateProductData(item, isCustomObj) {
    var prodData = '';

    $.each(item, function (index, custom) {
        if (isCustomObj) {

            if (index === 'key' || index === 'requireMMI') {
                prodData += 'data-' + index + "=" + custom + " ";
            }
        } else {
            if (custom['containsAddonKey'] || custom['mmiPrice'] || custom['product_key'] || custom['containsProductKey'] || custom['addonsDepend'] || custom['priority'] || custom['requireMMI']) {
                var key = (Object.keys(custom)[0]);
                var value = custom[key];
                prodData += 'data-' + key + "=" + value + " ";
            }
        }
    });
    // console.log(prodData);
    return prodData;
}
function generateProdDetail(i, name, item, priority) {
    var productDetail = $('#prodDetailBlock .vincheckProdDetails').append('<div class="prodDetailBlock hidden" id="detailBlock' + i + '' +
        '" data-item="' + item + '" data-priority="' + priority + '"  data-name="' + name + '">' +
        '<div class="row"><h5><b>PRODUCT DETAILS</b></h5></div>' +
        '<div class="row padB5 "><p class="greyTxt invisible">PRODUCT DETAILS</p></div>' +
        '<p class="prodDetailTitle p16"><b>' + name + '</b></p>' +
        '<div class="searchImg"><img src="../../web/img/loader.gif"> Finding  detail</div></div>');
    return productDetail;
}

function generateDetaiBlock(productDetail, i, detail, instalTime, price, dataDisc, isDealer) {
    if (isDealer) {
        yourPrice = (parseFloat(price) - parseFloat(price) * parseFloat(dataDisc) / 100);
        yourPrice = yourPrice.toFixed(2);
        productDetail.find('#detailBlock' + i + '').append(' <p class="prodDetailDetail">' + detail + '</p>');
        productDetail.find('#detailBlock' + i + '').append(' <div class="installTimeBlock"><p class="installTime p16"> <b>Estimated installation time:   <span class="">' + instalTime + '</span></b></p>' +
            ' <p class="installText">This installation time is an estimate and can vary. Please allow extra time when installing unfamiliar products.</p></div>');
        productDetail.find('#detailBlock' + i + '').append(' <div class="itemPriceBlock"><p class="yourPriceTxt"> <b>Your price: <span class="yourPrice" data-price="' + yourPrice + '" data-discount="' + dataDisc + '">' + yourPrice + '</span><span class="currencySymbol">USD</span></b></p>' +
            ' <p class="regularPriceBlock">Regular price <span class="RegularPrice" data-price="' + price + '">' + price + '</span><span class="currencySymbol">USD</span></p></div>');
        productDetail.find('.searchImg').remove();
    } else {
        productDetail.find('#detailBlock' + i + '').append(' <p class="prodDetailDetail">' + detail + '</p>');
        productDetail.find('#detailBlock' + i + '').append(' <p>Regular price <span class="RegularPrice"  data-price="' + price + '">' + price + '</span><span class="currencySymbol">USD</span></p>');
        productDetail.find('.searchImg').remove();
    }
}

function backBtnOrder() {
    var requestOrderBtn = $('#requestOrder');
    requestOrderBtn.css('width', 'auto');
    requestOrderBtn.text("ORDER");
    requestOrderBtn.hide();
    $('.orderResponse ').addClass('hidden');
}

function sendOrders() {
    var requestOrderBtn = $('#requestOrder');
    $("#orderModalForm").submit(function (e) {
        $('#modalOrder').modal('hide');
        e.preventDefault();
        requestOrderBtn.animate({
            width: '36px'
        }, 'slow', function () {
            requestOrderBtn.text('');
        });
        var products = [];
        var comment = $("#orderModalForm textarea").val();
        var vin = $('#longVin').text();
        $('.orderDetailsBlock  ').each(function (index, elem) {
            //product name
            var name = $(elem).find('.orderItemName ').text();
            // product price
            var price = $(elem).find('.ordPrice').text();
            price = price.substr(1);
            products.push({
                name: name,
                price: parseFloat(price)
            });
        });
        $.ajax({
            url: 'send/orders',
            method: 'GET',
            timeout: 10000,
            data: {
                vin: vin,
                comment: comment,
                products: products
            },
            error: function () {
                console.error('error');
            },
            success: function (data) {
                requestOrderBtn.hide();
                $('.orderResponse').removeClass('hidden');

            }
        })
    })


}

function showOrder() {

    var ordersBlock = $('.vinCheckOrder');
    var checkboxes = $('.productCheckBox');
    var orderBtn = $('#requestOrder');
    checkboxes.change(function (e) {
        checkAddonDepend();
        if (!orderBtn.is(":visible")) {
            backBtnOrder();

        }
        checkboxes.each(function (index) {
            if ($(checkboxes[index]).is(":checked")) {
                ordersBlock.removeClass('fadeOut');
                orderBtn.removeClass('fadeOut');
                orderBtn.show();
                orderBtn.css('display', 'inline-block');
                ordersBlock.show();
                ordersBlock.css('display', 'inline-block');
                addOrderTolIst(checkboxes, ordersBlock);
                return false;
            } else {
                orderBtn.addClass('fadeOut');
                orderBtn.css('display', 'none');
                ordersBlock.addClass('fadeOut');
                ordersBlock.css('display', 'none');
            }
        });

    })
}
function addOrderTolIst(checkboxes) {
    var ordersBlock = $('.vincheckOrderDetail');
    checkboxes.each(function (index, elem) {

        if ($(elem).is(":checked")) {
            if ($(elem).data('addonsdepend')) {
                var add = $(elem).parent().find('.vincheckAddon ');
                $.each(add, function (index, value) {
                    $(value).find('.productCheckBox').prop("disabled", false);
                })
            }
            var elemId = $(elem).attr('id');
            if ($('#orderDetailsBlock' + elemId).length > 0) {
            } else {
                var prodName = $(elem).data('product');
                var splitElem = elemId.split('check');

                var priceBlock = $('#detailBlock' + splitElem[1]).find('.yourPrice');
                var price = priceBlock.data('price');
                // var price = priceBlock.text();
                var discount = priceBlock.data('discount');
                var orderPriceBlock = genOrderPriceBlock(elemId, prodName, price, discount, $(elem).data());
                ordersBlock.append(orderPriceBlock);
            }
        } else {
            var elemId = $(elem).attr('id');
            $('#orderDetailsBlock' + elemId).remove();
        }
    });
    calcTotalOrder();
}

function genOrderPriceBlock(elemId, prodName, price, discount, datas) {
    var dataStr = generateDatas(datas);
    var priceBlock = '<div id="orderDetailsBlock' + elemId + '" class="orderDetailsBlock animated flipInY" >' +
        '<div class="pull-left width80 orderItemName">' + prodName + '</div>' +
        '<div class="pull-right ordPrice" data-discount="' + discount + '" data-price="' + price + '" ' + dataStr + '>$' + price + '</div>' +
        '</div>';

    return priceBlock;
}

function textToALL() {
    $('.fht-cell select option[value=""]').text('ALL');
}

function generateDatas(datas) {
    var dataString = '';
    $.each(datas, function (index, value) {
        dataString += 'data-' + index + '="' + value + '" ';
    });
    return dataString;
}
function calcTotalOrder() {

    var total = 0;
    var ordPrices = $('.ordPrice');

    reCalcProductPrice(ordPrices);
    ordPrices.each(function (index, elem) {
        var price = $(elem).text();
        price = price.substr(1);
        price = parseFloat(price);
        total += price;
    });
    total = total.toFixed(2);
    $('#ordersTotal').text('$' + total);
}


function reCalcProductPrice(ordPrices) {
    setCustomProduct(ordPrices);

    var isMMI = checkIsMMI(ordPrices);
    if (isMMI) {
        ordPrices.each(function (index, elem) {
            var mmiPrice = $(elem).data('mmiprice');
            if (mmiPrice) {
                var price = $(elem).text();
                var discount = $(elem).data('discount');
                var prodNameField = $(elem).parent().find('.pull-left');
                var prodName = prodNameField.text();
                if (prodName.includes(" + MMI")) {
                    prodNameField.text(prodName.slice(0, -5));
                }
                price = price.substr(1);
                price = parseFloat(price);
                price = price - mmiPrice * ((100 - discount) / 100);
                price = price.toFixed(2);
                $(elem).text('$' + price);
            }
        });
    } else if (checkAreMMI(ordPrices)) {
        var biggestOne = checkAreMMI(ordPrices);
        ordPrices.each(function (index, elem) {
            var containProductKey = $(elem).data('containsproductkey');
            if (containProductKey !== biggestOne) {
                var price = $(elem).text();
                var discount = $(elem).data('discount');
                var mmiPrice = $(elem).data('mmiprice');
                if (mmiPrice) {
                    price = price.substr(1);
                    price = parseFloat(price);
                    price = price - mmiPrice * ((100 - discount) / 100);
                    price = price.toFixed(2);
                    $(elem).text('$' + price);
                }
            }
        });
    }

    var includeAdd = checkIncludAddon(ordPrices);
    if (includeAdd.length > 0) {
        ordPrices.each(function (index, elem) {
            var add = $(elem).data('key');
            if (includeAdd.includes(add)) {
                var el = $(elem);
                el.text('$' + 0);
                el.parent().addClass('moreOnce');
            }
        });
    }
    var hasMMI = checkIncludedMMI(ordPrices);
    if (!(hasMMI || isMMI)) {
        ordPrices.each(function (index, elem) {
            if ($(elem).data('key') && $(elem).data('requiremmi')) {

                $(elem).parent().append('<span class="warningAddon"><span>Warning</span><br><span> Requires MMI. Confirm customer vehicle has MMI.</span></span>');
            }
        });
    }
}


function checkIncludAddon(ordPrices) {

    var includeAdd = [];
    var products = [];
    var addons = [];
    ordPrices.each(function (index, elem) {
        if ($(elem).data('containsproductkey')) {
            products.push($(elem).data('containsproductkey'));
        } else if ($(elem).data('key')) {
            addons.push($(elem).data('key'));
        }
    });

    if (products.length > 0 || addons.length > 0) {
        products.forEach(function (value, index) {
            if (addons.includes(value)) {
                includeAdd.push(value);
            }
        })
    }
    return includeAdd;
}


function checkIncludedMMI(ordPrices) {
    var hasMMI = false;
    ordPrices.each(function (index, elem) {
        if ($(elem).data('mmiprice')) {
            hasMMI = true;
        }
    });
    return hasMMI;
}

function setCustomProduct(ordPrices) {

    ordPrices.each(function (index, elem) {

        var el = $(elem);
        el.parent().find('.warningAddon').remove();
        var customPrice = el.data('price');
        var prodName = el.data('product');
        customPrice = parseFloat(customPrice);
        customPrice = customPrice.toFixed(2);
        el.text('$' + customPrice);
        el.parent().find('.pull-left').text(prodName);
        el.parent().removeClass('moreOnce');
    });

}


function checkAddonDepend() {
    var addonsDepends = $('*[data-addonsdepend="yes"]');
    addonsDepends.each(function (i, value) {
        if (!$(value).is(":checked")) {
            var addonsInput = $(value).parent().find('.vincheckAddon');
            $.each(addonsInput, function (ind, inputAdd) {
                $(inputAdd).find('.productCheckBox').attr('disabled', true);
                $(inputAdd).find('.productCheckBox').attr('checked', false);
            })
        }
    });
}


function checkAreMMI(ordPrices) {
    var arrMMI = [];
    var biggestOne = false;
    ordPrices.each(function (index, elem) {
        if ($(elem).data('mmiprice')) {
            arrMMI.push($(elem));
        }
    });
    if (arrMMI.length > 1) {
        var biggestObj = arrMMI[0];
        var price = $(arrMMI[0]).data('mmiprice');
        $(arrMMI).each(function (i, val) {
            var valPrice = $(val).data('mmiprice');
            if (valPrice > price) {
                price = valPrice;
                biggestObj = val;
            }
        });
        biggestOne = biggestObj.data('containsproductkey')
    }
    return biggestOne;
}

function checkIsMMI(ordPrices) {
    var hasMMI = false;
    ordPrices.each(function (index, elem) {
        var productKey = $(elem).data('product_key');
        if (productKey === 'mmi') {
            hasMMI = true;
            return false;
        }
    });
    return hasMMI;
}

function hasAddons(arr) {
    var hasA = false;
    arr.forEach(function (product, j, data) {
        if (product.addon_key) {
            hasA = true;
            return hasA;
        }
    });
    return hasA;
}


function upDownClick() {
    $('.upDown').click(function () {
        $(this).parent().parent().find('.vincheckAddon').toggleClass('hidden');
        $(this).toggleClass('toggleIcon');
    })
}
function infoIconClick(i) {
    var prodDetailBlock = $('#prodDetailBlock');
    var vincheckProducts = $('.vincheckProducts');

    prodDetailBlock.find('.prodDetailBlock').addClass('hidden');
    vincheckProducts.find('.addonsWrap').find('.prodTitle').removeClass('afterIconBlue').removeClass('afterIconBlack').addClass('afterIconBlue');
    prodDetailBlock.find('#detailBlock' + i).toggleClass('hidden');
    vincheckProducts.find('#detailBlockBtn' + i).find('.prodTitle').toggleClass('afterIconBlue').toggleClass('afterIconBlack');

    vincheckProducts.find('.vincheckAddon').removeClass('afterIconBlue').removeClass('afterIconBlack').addClass('afterIconBlue');
    vincheckProducts.find('#addon' + i).toggleClass('afterIconBlue').toggleClass('afterIconBlack');
}
function getMMI(vin) {
    var productsBlock = $('.vincheckMMI');
    productsBlock.children('.row').remove();
    $('.vincheckErrorMMI').hide();
    productsBlock.html('<div><img src="../../web/img/loader.gif"> Finding parts</div>');
    $.ajax({
        url: 'check/mmi/vin',
        method: 'GET',
        timeout: 0,
        data: {
            vin: vin
        },
        error: function (data) {
            console.error('error');
            console.error(data);
            productsBlock.html('');
            $('.vincheckErrorMMI').show().text('Error connecting to the server. Please try again.')
        },
        success: function (data) {
            productsBlock.html('');
            data = jQuery.parseJSON(data);
            // console.log(data);
            if (data.error != null) {
                console.log(data.error);
                $('.vincheckErrorMMI').show().text('No compatible MMI found for this VIN');
            } else {
                var html = $('.vincheckMMI');
                html.show();
                html.append('<div class="row"><h5><b>MMI COMPATIBLE WITH YOUR VIN</b></h5></div>');
                html.append('<div class="row">' + data.parts[0].name + '</div>');

            }
        }
    })
    ;
}


function vinCheck() {

    $('#vinCheckForm').submit(function (e) {
        if ($(this).valid()) {
            e.preventDefault();
            $('.orderDetailsBlock').remove();
            $('.vinCheckOrder').hide();
            $('.orderResponse ').addClass('hidden');
            backBtnOrder();
            $('.spotErrorBlock').hide();
            $('.vincheckErrorDetails').children('.row').remove();
            $('.vincheckDetails').children('.row').remove();
            $('.vincheckErrorProducts').children('.row').remove();
            $('.vincheckProducts').children('.row').remove();
            $('.vincheckErrorMMI').children('.row').remove();
            $('.vincheckMMI').children('.row').remove();
            $('#printVincheck').addClass('hidden');
            $('.exchangeBlock').addClass('hidden');
            var vin = $('#inputCheckVin').val();
            $(".textVinInfo").show();
            showExchangeText();
            getProducts(vin);
        }
    })
}


function exchangeToZero() {
    $('.exchangeFeeBlock  input').val(0);
}

function spotErrorForm() {
    $('#spotErrorForm').submit(function (e) {
        e.preventDefault();
        var vin = $('#inputCheckVin').val();
        var name = $('.dataCompany').data('company');
        var message = $('#spotErrorForm textarea').val();

        $.ajax({
            url: 'send/mistake',
            method: 'POST',
            timeout: 5000,
            data: {
                vin: vin,
                name: name,
                message: message
            },
            error: function () {
                console.log('error');

            },
            success: function (data) {
                showToastr('Message', data, "toast-top-full-width", 2000);
                $('#spotErrorForm textarea').val('');
            }
        })

    })
}


function sendToManager() {
    $('#sendManager').submit(function (e) {
        e.preventDefault();
        var message = $('#sendManager textarea').val();
        var mail = $('#sendManager input').val();
        $.ajax({
            url: 'send/manager',
            method: 'POST',
            timeout: 5000,
            data: {
                mail: mail,
                message: message
            },
            error: function () {
                console.log('error');

            },
            success: function (data) {
                showToastr('Message', data, "toast-top-full-width", 2000);
                $('#sendManager textarea').val('');
            }
        })

    })
}


function sendComment() {
    $('#tellNewsForm').submit(function (e) {
        e.preventDefault();
        var article = $('.contentNewsBlock h1').text();
        var mail = $('.dataEmail').data('email');
        var message = $('#tellNewsForm textarea').val();

        $.ajax({
            url: mainUrl + '/blog/send/comment',
            method: 'POST',
            timeout: 5000,
            data: {
                mail: mail,
                article: article,
                message: message
            },
            error: function () {
                console.log('error');

            },
            success: function (data) {
                showToastr('Message', data, "toast-top-full-width", 2000);
                $('#tellNewsForm textarea').val('');
            }
        })

    })
}


function changeAccountPass() {
    $('form[name="reset_password"]').submit(function (e) {
        e.preventDefault();
        var formSerialize = $(this).serialize();
        $.post('', formSerialize, function (response) {
            //your callback here
            showToastr('Password', response, 'toast-bottom-right');
            $('#accountForm').hide();
            $("form").trigger("reset");
        }, 'JSON');
    })
}


function showEditDealer() {
    $('.editBlockBtn').click(function (e) {
        e.preventDefault();
        var user = $(this).data('id');
        var editBlock = $('#editBlock');
        var mainUrl = window.location.origin;
        editBlock.html('<div class="accountBlock"><img src="' + mainUrl + '/web/img/loader.gif"> Finding User</div>');
        $.ajax({
            url: '',
            method: 'POST',
            timeout: 5000,
            data: {
                user: user
            },
            error: function () {

                editBlock.html('<div class="accountBlock">Error connecting to the server. Please try again.</div>')
            },
            success: function (data) {
                var editblock = $(data).find('div#wrapperEdit');
                editBlock.html(editblock);
                hideAccount();
            }
        });
        editBlock.removeClass('slideOutRight');
        editBlock.removeClass('hidden');

    });
}


function showEditOrder() {
    $('.editBlockAjaxBtn').click(function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var url = $(this).data('url');
        var user = $(this).data('userid');
        var role = url.split("/")[0];
        var editBlock = $('.editBlockAjax');
        editBlock.html('<div class="accountBlock"><img src="' + mainUrl + '/web/img/loader.gif"> Finding ' + role + '</div>');
        $.ajax({
            url: url + '/edit/' + id,
            method: 'POST',
            timeout: 5000,
            data: {
                id: id
            },
            error: function () {
                editBlock.html('<div class="accountBlock">Error connecting to the server. Please try again.</div>')
            },
            success: function (data) {

                closeAny('#editOrder', '.editOrderBtnbtn', 'slideOutRight');
                editBlock.html(data);
                hideAccount();
                loadDatePicker();
                if (role == 'order') {
                    editOrder(url, id, user)
                } else if (role == 'managers') {
                    editManager(url, id, user)
                }

            }
        });
        editBlock.removeClass('slideOutRight');
        editBlock.removeClass('hidden');
    });
}

function editOrder(url, id, user) {
    var editForm = $('form[name="appbundle_edit_dealer_order"]');
    editForm.submit(function (e) {
        e.preventDefault();
        var fields = $('form[name="appbundle_edit_dealer_order"] input').map(function () {
            return $(this).val();

        }).get();
        $.ajax({
            url: url + '/edit/do/' + id,
            method: 'POST',
            timeout: 5000,
            data: {
                user: user,
                id: id,
                createAt: fields[0],
                value: fields[1],
                cost: fields[2],
                itemNumber: fields[3]
            },
            error: function () {
                console.log('error');
            },
            success: function (data) {
                location.reload();
            }
        });
    })
}


function editManager(url, id, user) {
    var editForm = $('form[name="appbundle_edit_usermanager"]');
    editForm.submit(function (e) {
        e.preventDefault();
        var fields = $('form[name="appbundle_edit_usermanager"] input').map(function () {
            return $(this).val();
        }).get();
        $.ajax({
            url: url + '/edit/do/' + id,
            method: 'POST',
            timeout: 5000,
            data: {
                user: user,
                id: id,
                fullName: fields[1],
                email: fields[2],
                phone: fields[3]
            },
            error: function () {
                console.log('error');
            },
            success: function (data) {
                location.reload();
            }
        });
    })
}


function deleteDealer() {
    $('.deleteBtn').click(function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var url = $(this).data('url');
        var block = $(this);
        $.ajax({
            url: url + '/delete/' + id,
            method: 'POST',
            timeout: 5000,
            data: {
                id: id
            },
            error: function () {

            },
            success: function (data) {
                var deleteElement = block.parent().parent().prev();
                deleteElement.addClass('hinge');
                $('#confirmRow' + id).hide();
                setTimeout(function () {
                    deleteElement.addClass('hidden')

                }, 2000)
            }
        });

    })
}


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        console.log('image');
        reader.onload = function (e) {
            $('#image').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        $('.upload-file-container-text').addClass('changeAvatart');
    }
}

$("#appbundle_usermanager_file").change(function () {
    readURL(this);
});

function readURLEdit(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        console.log('image');
        reader.onload = function (e) {
            $('#image').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
        $('.upload-file-container-text').addClass('changeAvatart');
    }
}

$("#appbundle_edit_usermanager_file").change(function () {
    readURLEdit(this);
});


function deleteDealerFormShow() {
    var deleteBtn = $('#deleteDealerFormBtn');
    var newBtn = $('#newDealerFormBtn');
    deleteBtn.click(function (e) {
        e.preventDefault();
        $('#newDealerForm').fadeOut();
        $('#deleteDealerForm').fadeIn();
        deleteBtn.removeClass('persInfoDisable');
        deleteBtn.addClass('persInfoActive');
        newBtn.removeClass('persInfoActive');
        newBtn.addClass('persInfoDisable');

    })
}


function newDealerFormShow() {
    var deleteBtn = $('#deleteDealerFormBtn');
    var newBtn = $('#newDealerFormBtn');
    newBtn.click(function (e) {
        e.preventDefault();
        $('#newDealerForm').fadeIn();
        $('#deleteDealerForm').fadeOut();
        deleteBtn.removeClass('persInfoActive');
        deleteBtn.addClass('persInfoDisable');
        newBtn.removeClass('persInfoDisable');
        newBtn.addClass('persInfoActive');

    })
}


function deleteManagerFormShow() {
    var deleteBtn = $('#deleteManagerFormBtn');
    var newBtn = $('#newManagerFormBtn');
    var managerListBtn = $('#managerListBtn');
    deleteBtn.click(function (e) {
        e.preventDefault();
        $('#newManagerForm').hide();
        $('#deleteManagerForm').show();
        $('#managerListForm').hide();
        deleteBtn.removeClass('persInfoDisable');
        deleteBtn.addClass('persInfoActive');
        newBtn.removeClass('persInfoActive');
        newBtn.addClass('persInfoDisable');
        managerListBtn.addClass('persInfoDisable');
        managerListBtn.removeClass('persInfoActive');

    })
}


function newManagerFormShow() {
    var deleteBtn = $('#deleteManagerFormBtn');
    var newBtn = $('#newManagerFormBtn');
    var managerListBtn = $('#managerListBtn');
    managerListBtn.click(function (e) {
        e.preventDefault();
        $('#managerListForm').show();
        $('#newManagerForm').hide();
        $('#deleteManagerForm').hide();
        newBtn.removeClass('persInfoActive');
        newBtn.addClass('persInfoDisable');
        deleteBtn.removeClass('persInfoActive');
        deleteBtn.addClass('persInfoDisable');
        managerListBtn.addClass('persInfoActive');
        managerListBtn.removeClass('persInfoDisable');

    })
}


function managerListShow() {
    var deleteBtn = $('#deleteManagerFormBtn');
    var newBtn = $('#newManagerFormBtn');
    var managerListBtn = $('#managerListBtn');
    newBtn.click(function (e) {
        e.preventDefault();
        $('#newManagerForm').show();
        $('#deleteManagerForm').hide();
        $('#managerListForm').hide();
        deleteBtn.removeClass('persInfoActive');
        deleteBtn.addClass('persInfoDisable');
        newBtn.removeClass('persInfoDisable');
        newBtn.addClass('persInfoActive');
        managerListBtn.addClass('persInfoDisable');
        managerListBtn.removeClass('persInfoActive');

    })
}


function getNotificationCount(id) {
    $.ajax({
        url: mainUrl + '/notification/all/count/' + id,
        method: 'POST',
        timeout: 5000,
        data: {
            id: id
        },
        error: function () {
            console.log('error');
        },
        success: function (data) {
            $('.notification a').addClass('danger-block').text(data);
        }
    });
}

function showToastr(title, message, position, timeOut) {

    if (position === undefined) {
        position = "toast-bottom-left"
    }
    if (timeOut === undefined) {
        timeOut = "0"
    }
    $.playSound(mainUrl + '/sound/message2.wav');
    toastr.options.onclick = function () {
        $.playSound(mainUrl + '/sound/messageClose.wav');
    };
    toastr.success(message, title, {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": position,
        // "showDuration": "3000",
        "preventDuplicates": false,
        "timeOut": timeOut,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "slideDown",
        "hideMethod": "slideUp"

    });
}

function IsSafari() {
    isSafari = navigator.vendor.indexOf("Apple") == 0 && /\sSafari\//.test(navigator.userAgent); // true or false
    return isSafari;
}


function addDealerToFile() {
    $('.addDealerToFile').click(function (e) {
        $.ajax({
            url: 'add_new_file/dealers',
            method: 'POST',
            timeout: 5000,
            error: function () {
                console.log('error');
            },
            success: function (data) {
            }
        });
    });
}
function showExportData() {
    $('#showExportData').click(function (e) {
        e.preventDefault();
        $('.tableexport-caption').toggleClass('hidden');
        setTimeout(function () {
            closeAny('.tableexport-caption', '#account', null, 100);
        }, 100);
        $('#showExportData').click(function (e) {
            e.stopPropagation();
        });
    });

}

function showTableExport(tableExport) {
    $(tableExport).tableExport({
        headings: true,                    // (Boolean), display table headings (th/td elements) in the <thead>
        footers: true,                     // (Boolean), display table footers (th/td elements) in the <tfoot>
        formats: ["xls", "csv", "txt"],    // (String[]), filetypes for the export
        fileName: "id",                    // (id, String), filename for the downloaded file
        bootstrap: true,                   // (Boolean), style buttons using bootstrap
        position: "top",                   // (top, bottom), position of the caption element relative to table
        ignoreRows: null,                  // (Number, Number[]), row indices to exclude from the exported file(s)
        ignoreCols: null,                  // (Number, Number[]), column indices to exclude from the exported file(s)
        ignoreCSS: "",                       // (selector, selector[]), selector(s) to exclude from the exported file(s)
        emptyCSS: "",                     // (selector, selector[]), selector(s) to replace cells with an empty string in the exported file(s)
        trimWhitespace: false              // (Boolean), remove all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s)
    });
    $('.tableexport-caption').addClass('hidden');
}
//manager photo make circle
function squarePhoto(profileFoto) {
    $(profileFoto).height($(profileFoto).width());
}


function squareNews() {
    var firtsNews = $('.firstArticle ');
    var otherNews = $('.otherArticle ');
    var widthFirts = firtsNews.width() * 1.22;
    var widthOther = otherNews.width();
    firtsNews.find('.thumbnailPhoto').height(widthFirts * 5 / 14);
    otherNews.find('.thumbnailPhoto').height(widthOther * 17 / 37);
}

function latestNews() {
    var firtsNews = $('.sliderNews ');
    var widthFirts = firtsNews.width();
    firtsNews.find('.thumbnailPhoto').height(widthFirts * 9 / 16);
}


function getNotifications() {
    $('.notification').click(function (e) {
        e.preventDefault();
        var notList = $('#notificationList');
        $.ajax({
            url: mainUrl + '/notification/all/notification',
            method: 'POST',
            timeout: 5000,

            error: function () {
                console.log('error');
            },
            success: function (data) {
                notList.html('');
                data = jQuery.parseJSON(data);
                notList.append('<li class="notTitle"> YOUR NOTIFICATIONS</li>');
                data.forEach(function (item, i, arr) {
                    var isRead = '';
                    if (!item.has_read) {
                        isRead = "notRead";
                    }
                    notList.append('<li class="' + isRead + '" data-id="' + item.id + '">' +
                        '<div class="row greyTxt"> ' + MDFormat(item.create_at) + '</div>' +
                        '<div class="row colText"> ' + item.title + '</div>' +
                        '<div class="row">' + item.message + '</div>' +
                        '</li>')
                });
                notList.show();
                notList.niceScroll({
                    cursorcolor: "#212121",
                    cursorwidth: "15px",
                    background: "#757575",
                    cursorborder: "1px solid #757575",
                    cursorborderradius: 0

                }).rail.css({
                    "z-index": 9999,
                    "margin-left": "2px"
                });
                readNot();
                anyCliclClose('#notificationList');

                if (notList.is(':visible')) {
                    $('.notification a').addClass('notTriangle');
                } else {
                    $('.notification a').removeClass('notTriangle');
                }
            }
        });

    })
}

function readNot() {
    $('.notRead').click(function (e) {
        e.preventDefault();
        var id = $(this).data('id');

        $.ajax({
            url: mainUrl + '/notification/all/notification/read/' + id,
            method: 'POST',
            timeout: 5000,
            data: {
                id: id
            },
            error: function () {
                console.log('error');
            },
            success: function (data) {
                $(this).removeClass('notRead');
                console.log(data);
                if (data > 0) {
                    $('.notification a').addClass('danger-block').text(data);
                } else {
                    $('.notification a').removeClass('danger-block').text(data);
                }


            }
        });
    })
}


function MDFormat(MMDD) {
    MMDD = new Date(MMDD.replace('+', '.'));
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "June", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var strDate = "";
    var today = Date.today();
    today.setHours(0, 0, 0, 0);
    var yesterday = Date.parse('t - 1 d');
    yesterday.setHours(0, 0, 0, 0);
    if (today.getTime() <= MMDD.getTime()) {
        strDate = "Today";
    } else if (yesterday.getTime() <= MMDD.getTime()) {
        strDate = "Yesterday";
    } else {
        strDate = months[MMDD.getMonth()] + "-" + MMDD.getDate();
    }
    return strDate;
}


function anyCliclClose(closeBlock) {
    if (IsSafari()) {
        $(document).bind("touchstart", function (event) {
            if (!$(event.target).closest(closeBlock).length) {
                if ($(closeBlock).is(":visible")) {
                    $(closeBlock).hide();
                    $('.notification a').removeClass('notTriangle');
                    $('#notificationList').getNiceScroll().remove();
                }
            }
        });
    } else {
        $(document).click(function (event) {
            if (!$(event.target).closest(closeBlock).length) {
                if ($(closeBlock).is(":visible")) {
                    $(closeBlock).hide();
                    $('.notification a').removeClass('notTriangle');
                    $('#notificationList').getNiceScroll().remove();
                }
            }
        });
    }
}


function ifIOS() {
    var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    return iOS;
}
function closeAny(closeBlock, clickBlock, animate, time) {
    $(document).click(function (e) {
        if (!$(e.target).parents().andSelf().is(clickBlock) && $(closeBlock).is(":visible")) {
            if (animate != undefined) {
                $(closeBlock).addClass(animate);
            }
            if (time === undefined) {
                time = 1000;
            }
            setTimeout(function () {
                $(closeBlock).addClass('hidden');
            }, time);
            $(document).off('click');
        }
    });
    $(closeBlock).click(function (e) {
        e.stopPropagation();
    });
}

function toggleDealerCheckbox(selectId) {
    $(selectId).toggle();
}


function selectAlldealers(id) {
    $(id + ' .allDealer').click(function (e) {
        if ($(id + ' .allDealer').is(':checked')) {
            $(id + ' .roundedOne input').each(function () {
                $(this).prop('checked', true)
            })
        } else {
            $(id + ' .roundedOne input').each(function () {
                $(this).prop('checked', false)
            })
        }
    });
}
var editDownloadIndex = 10;
function editDownloadFiler() {

    $('.editFileBtn').click(function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var selectId = '#selectDealer' + id;
        var dealerFileCount = $('#dealerFileCount' + id);
        var dealersListA = null;
        var dealersListR = null;
        var save = $(selectId).is(":visible");
        if ($(selectId).find('.row').length !== 0) {
            var dealerA = $(selectId).find('.dealerCheckboxInput:checked');
            dealersListA = [];
            dealerA.each(function (index) {
                dealersListA.push(
                    $(this).val()
                )
            });
            var dealerR = $(selectId).find('.dealerCheckboxInput').not(':checked');
            dealersListR = [];
            dealerR.each(function (index) {
                dealersListR.push(
                    $(this).val()
                )
            });

        }

        $.ajax({
            url: mainUrl + '/download/edi_file/dealers/' + id,
            method: 'POST',
            timeout: 5000,
            data: {
                id: id,
                dealersAdd: dealersListA,
                dealersRemove: dealersListR,
                save: save
            },
            error: function () {
                console.log('error');
            },
            success: function (data) {
                var count = data.fileDealers.length;
                dealerFileCount.html(count);
                toggleDealerCheckbox(selectId);
                var s = $(selectId);
                s.css('z-index', editDownloadIndex);
                editDownloadIndex++;
                s.append('<div class="overflow"></div> ');
                if (s.find('div.row').length === 0) {
                    s.find('.overflow').append('<div class="row">' +
                        '<div class="roundedOne">' +
                        ' <input type="checkbox"value="" class="allDealer" id="allDealer' + selectId + '" name="dealerCheckbox"  />' +
                        ' <label for="allDealer' + selectId + '"></label></div>Choose all dealers</div><br>');
                    for (var val in data.userDealers) {

                        var dealerId = data.userDealers[val].id;
                        var dealerName = data.userDealers[val].company;
                        var checked = '';
                        for (var file in data.fileDealers) {
                            if (dealerId === data.fileDealers[file].id) {
                                checked = 'checked';
                            }
                        }
                        s.find('.overflow').append('<div class="row">' +
                            '<div class="roundedOne">' +
                            ' <input type="checkbox"value="' + dealerId + '" class="dealerCheckboxInput" id="roundedOne' + selectId + val + '" name="dealerCheckbox" ' + checked + '  />' +
                            ' <label for="roundedOne' + selectId + val + '"></label></div>' + dealerName + '</div>');
                    }

                }
                $(selectId).find('.overflow').niceScroll({
                    cursorcolor: "#00838f",
                    cursorwidth: "15px",
                    background: "#e0e0e0",
                    cursorborder: "1px solid #e0e0e0",
                    cursorborderradius: 0

                }).rail.css({
                    "z-index": 9999,
                    "margin-left": "2px"
                });

                selectAlldealers(selectId);
            }
        });
    })
}

function printVinCheck() {
    var role = $('#userRole').data('role');

    $('#printVincheck').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        var company = $('.dataCompany').data('company');
        var printStr = company;
        if ($('.dataCity').data('city') != undefined) {
            var city = $('.dataCity').data('city');
            var phone = $('.dataPhone').data('phone');
            printStr = company + ', ' + city + ', ' + phone;
        } else if ($('.dataPhoneNUmber').data('phone_number') != undefined) {
            var email = $('.dataEmail').data('email');
            printStr = company + ', ' + email + ', ' + $('.dataPhoneNUmber').data('phone_number');
        }

        if (phone === undefined) {
            phone = '+1 646-439-7920'
        }
        if (city === undefined) {
            city = '';
        } else {
            city = city + ', '
        }

        if (role === 'ROLE_USER' || role === 'ROLE_MANAGER') {
            company = company + ', ';
        } else {
            company = 'BimmerTech, ';
        }


        function products_sort(a, b) {
            var aPriority = $(a).data('priority');
            var bPriority = $(b).data('priority');
            if (aPriority != bPriority) {
                return (aPriority > bPriority) ? 1 : -1;
            } else {
                aPriority = $(a).data('name');
                bPriority = $(b).data('name');
                return (aPriority > bPriority) ? 1 : -1;
            }
        }

        function printoutSort(products) {
            var arrayProducts = $.map(products, function (value, index) {
                return [value];
            });
            arrayProducts = arrayProducts.sort(products_sort);
            return arrayProducts;
        }


        var vin = $('#longVin').text();
        var products = [];
        var modelVin = $('#longVin').data('model');
        var model = modelVin.toUpperCase();

        var productsBlock = $('.prodDetailBlock');
        productsBlock = printoutSort(productsBlock);
        $(productsBlock).each(function (index, elem) {
            var el = $(elem);
            if (el.data('item') === 'product') {
                var prodName = el.find('.prodDetailTitle b').text();
                var prodDetail = el.find('.prodDetailDetail').text();
                var currency = el.find('.regularPriceBlock .currencySymbol').text();
                var prodPrice = el.find('.RegularPrice').text();
                // prodPrice = prodPrice + currency;
                products.push({
                    columns: [
                        {
                            text: prodName,
                            bold: true,
                            width: 300
                        },
                        {
                            text: 'List price: ' + prodPrice + ' ' + currency,
                            bold: true,
                            alignment: 'right',
                            width: 200
                        }
                    ]
                    , margin: [0, 5]
                });
                products.push({
                    columns: [
                        {
                            text: '+installation',
                            fontSize: 9,
                            alignment: 'right',
                            width: 500
                        }
                    ]
                });

                products.push({
                    columns: [
                        {
                            text: prodDetail,
                            fontSize: 9,
                            width: 250
                        }
                    ]
                });
                if (index < $('.prodDetailBlock').size() - 1) {
                    products.push({
                        canvas: [
                            {
                                type: 'line',
                                x1: 0, y1: 10,
                                x2: 500, y2: 10,
                                lineWidth: 1,
                                lineColor: '#7e7e7e'
                            }
                        ]
                    })
                }
            }


        });

        var docDefinition = {

            footer: {
                columns: [
                    {
                        image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHYAAAAqCAYAAABm66+dAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpBQkI1QkQ2ODNGRjhFNjExQTBCNkY4QkUzMTJBM0I0RiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDowNDYzQ0VEMEY4NDAxMUU2OEFGQUMyNzJFMjJDMkY4OSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDowNDYzQ0VDRkY4NDAxMUU2OEFGQUMyNzJFMjJDMkY4OSIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkFEQjVCRDY4M0ZGOEU2MTFBMEI2RjhCRTMxMkEzQjRGIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkFCQjVCRDY4M0ZGOEU2MTFBMEI2RjhCRTMxMkEzQjRGIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+4nQeowAAB8hJREFUeNrsXAlsVEUYnoW1xXKVFmsLFUFKBayoUNFqjFU5PQAPFEEDxABGkWhUDo9gvGJMFETTiAfSiAh4oFGJpZyKChIUU8ohEtSKGgWUS7RQ6v9nvw3D8M/bfW/fQlnen3zp7rw3897MN/PPf8w2VF9frwJJPWkUDEFqSjjRBkKhUAb96U3oRehG6ELIJJxC2E/YSfiBsIqwgrCQ8F8w9P6IVePyBS8gOZfwCuEfbsYFmOgywtkBLf4QK/LjgdDTCTMIh1wSaoLrv0o4LaDnOBNLci1he4KEmviTcHVA0XEiFnvmAp9J1TEpoOkYE4tBL2BbiTCecCBJ5D4TUHWMiCW5H4POlm0OykoIPyeJ3AcDupJMLElfY9DXEJrjWivC/CQQWwf3yUnOJ4wNaPVALEku4Q9h4CsJabiHVfM4+KV+kruN0NKhP6WEqXH2PYtweUDsYWLLHQb+bSZVc457ELb4TO5Un4idAJsgIJakexx+6hQj6tGCMMcHQvcQphE6+URsa0KfgNgIWW/GScJ44VljEEp0S+hWwn0xVLAXYoM9FqTynvSvC0KGC8/jcOPGOOsvI1yvjk5IdCTMhv/sRGwzws2Eh7Dfn2Pc24TQGZ85ht0en88i3I16pcIqH0l4mDBIeLdi7dlDCY8QhuFZUeE4/ADCRMJoQraFmzzCHYRHCSOM+7iNIm08HlCReHxMYqXsDkeB0l1MGg4L9jfKqtD5ckudWsJMwgUY1PlQ/QrP5k5WE24VBl2XPpgYOfhbQ3iK8DKhMe7JxeCyFBLuhVaZTvhFRRITQwgv4Z7BhA8Iewmf4x0rQWJUpmDAF7GXQFgKQhZhUvCkWY6/3MZBQoU2IaIyDu/6G+Ej3Pcp4Qpt4jxJuInwAmEzInXulzIe5FaNciKgp+URwzFIfN/vhMkgQhLOEG0y2n7WsmJ5oL6A22XKE4Tn8bk9JlHUTVpJmKsRH5UybC2cfUozrt1DeFr7/hUmUr7w/jNBamdBA62FJ6EwmWZp35U2ETdAu4Qxad4V3te1Kv4mgZhvoYUw7uRtwoDpnZltaXephdh6qHxJGmMQ8wVi91smVieHNvm912vff4TalGQdNI4k8+BBhKGRMi33PYZtQkGTXex2j5VUcRuP+3hrzHap/kbMzlqjvBE6sAlqV5IiS/lnUPmS1MEAHCBcWwH/3JTNMOCqLFvH34SmWtn7lmfzhFpiufa1iuSrWbutRpuSLNTI3AUt40qkRHsiabQz0eFLtD3TJj2g9ovjmDBpwqSojlFvNdTdAqN8m0Od3Q7XdkDt7wMhtnsP4h5bG9lYqW2xMiXJhBZj+ckLERKxXo/L7MA+9WIMUlvAwLnLxbOyYWCYq0jFICnDR8+iTiV+4qQee2oWDK0Kh3v3JvKgsKXBZi7a2ApDZQaMKCe5BW5Krsv3lCZKKEYd3kd3NlD3cy9cyrXJeoC0YvbFWXcNVF0hXAUnUjvBZZjjgVSFQTClfYw6xckcuARlnZNBlCxi18eow35WL7K8iglzsafYpAn2kSq4AsojqbssPmyepQ4HNW4kfNhAiWVX6UJsS8eM2HVC2QFYtd2IzP6ExXG03RuETnYZ8DBli6V8OYIM6YKK5oT965YJ0RCkFoGH1yyRtYQlbDHXdWG/dhCRWRNnm3nYc4f49I6rhbJ0qPYa+LnlcJnawF+uhCHXkOUtkMpG1Dtwt7hfXVUkzXgDfG5TTrWUx4w8tROCBKMc0nt6UGAsVomf6buRwmu31NyyphgEDs/dLgQf0tCn6KC0cxgOpyOxHbQAi9N97fAcSbIEd5IN1YEYO37/y4xVbD5rqqfIE8haoY4+1TDMgdg0ON/JOE2RrQJxTazNj5wu7MVvhEKhAQ57xsokdOJj+MepIj0t0TDfJST9RIAIDCMM2FEwovpRnSXavdGPzWFR5/v4flc5hOdONOF9czDUdBUWAoNDpv2wpSwwbAq+dg00Yg3cRTYMJ0Jt52PM59WbRDrsnQOVPZNTIuyxCrPRLzW8WKWWZMGgHA0fnMOGnO6cBCM2DNKibuEo7LtRq5+TExzS5DDsNM2PZ54muD2lONcy6Gwgdbf8IOg9H0hlzXBeCu6PpSqSuI9ub2a+ujFcINaUj1vaqFRaogUclJncxYp93qki558KjHJ2rCtIDfOLmsH4sZh1iTjfnE/9LsWNoC4ILZYa5dlYhbMs9b4n/GqU7YknQKGr6b+gXrcLlznrUiGQzsH6CQl0+BMVSRKkurDbI6UPnyOcoexZHenE5X5XxIJczuZfqeQjGZx6WigYTPzzyi89dJZdpqFwc1JddmDFLjPArua2GP62p5CiRC5bcSVKjiN3gKGjO96HYCS4Oc/LESSO/+5OYTJrteAFb2HFFg74DNiYpBMLcjlmexE2d1MKQa5+dLRaxf8jK7by+qqGG9v1S6rRz9aIOHGwoQxuDZddigXEY70JtgYfiMuDJstRLgjz8uNnPkX3rWDNrlJH5nKbqKMPp+nYoA6fyDtZpCtcmQ743kpzg3qrI5MarI5HANFjpwUmf+z6JPyLdo1cjkxcpyJBbP1XAxxQyDBMfJNQnhTDlQ//A+NkFxs/oUT/HRAiT23hbPPq45OAnPrjA9QHNVVbBNI5TLg2oMQ/YuMOKQZy4kvwf55SVP4XYABoXn2OkeIy7QAAAABJRU5ErkJggg==',
                        width: 75,
                        margin: [0, 0, 0, 30]
                    },
                    {
                        text: 'Make your BMW perfected',
                        alignment: 'right',
                        fontSize: 9,
                        width: 415,
                        margin: [0, 10, 0, 0]
                    }
                ],
                margin: [40, 0]
            },

            content: [
                {
                    text: [
                        {text: company, bold: true, style: 'header'},
                        {text: city + '\n', style: 'header'},
                        {text: 'phone number: ' + phone, style: 'header'}
                    ],
                    alignment: 'right',
                    margin: [0, 30]
                },
                {
                    text: 'VIN: ' + vin, fontSize: 9
                },
                {
                    text: [
                        {text: 'BimmerTech upgrades for your ', fontSize: 16},
                        {text: 'BMW ' + model, bold: true, fontSize: 16}
                    ],
                    margin: [0, 5, 0, 10]
                },
                {
                    canvas: [
                        {
                            type: 'line',
                            x1: 0, y1: 10,
                            x2: 500, y2: 10,
                            lineWidth: 2,
                            lineColor: '#7e7e7e'
                        }
                    ]
                }

            ]
        };
        docDefinition['content'].push(products);

        $.ajax({
            url: 'log/print',
            method: 'GET',
            timeout: 5000,
            data: {
                vin: vin,
                company: company
            },
            error: function () {
                console.error('error');
            },
            success: function (data) {
                pdfMake.createPdf(docDefinition).download(vin + '_products.pdf');
                // pdfMake.createPdf(docDefinition).print();
            }
        });


    });
    window.printVinCheck = function () {
        return false;
    }
}

function showConfirmDelete() {
    $('.confirmDeleteBtn').click(function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        $('#confirmRow' + id).css("display", "table-row");
    })
    $('.cancelDelete1').click(function () {
        var id = $(this).data('id');
        $('#confirmRow' + id).hide();

    });
}


function setFilter() {
    // var managerList = $('.managerList');
    // var statusList = $('.statusList');
    // $('#showStatusList').click(function () {
    //     managerList.addClass('hidden');
    //     statusList.removeClass('hidden');
    //     closeAny(statusList, $('#showStatusList'), null, 10);
    // });
    // $('#showManagerList').click(function () {
    //     statusList.addClass('hidden');
    //     managerList.removeClass('hidden');
    //     closeAny(managerList, $('#showManagerList'), null, 10);
    // });
    // $('.tableFilter').click(function (event) {
    //     event.preventDefault();
    //     var filterName = $(this).data('name');
    //     var filterType = $(this).data('type');
    //     if (filterType === 'status') {
    //         $('.statusColumn .filterControl input').val(filterName);
    //         $('.statusColumn .filterControl input').change();
    //         // $('.statusColumn .filterControl input').bootstrapTable('resetSearch', filterName);
    //
    //     } else if (filterType === 'manager') {
    //         $('.managerColumn .filterControl input').val(filterName);
    //     }
    //     // $('.table').bootstrapTable('resetSearch', filterName);
    //     $(this).parent().addClass('hidden');
    // })
}

function nameSorter(a, b) {

    a = $(a).text().toLowerCase();
    b = $(b).text().toLowerCase();
    if (a < b) return 1;
    if (a > b) return -1;
    return 0;
}
function onlyNumberSort(a, b) {
    a = a.substr(1);
    b = b.substr(1);
    a = parseFloat(a);
    b = parseFloat(b);
    if (a < b) return 1;
    if (a > b) return -1;
    return 0;
}
function onlyDateSort(a, b) {
    a = stringToDate(a, 'dd-mm-yyyy', '-');
    b = stringToDate(b, 'dd-mm-yyyy', '-');
    if (a < b) return 1;
    if (a > b) return -1;
    return 0;
}


function stringToDate(_date, _format, _delimiter) {
    var formatLowerCase = _format.toLowerCase();
    var formatItems = formatLowerCase.split(_delimiter);
    var dateItems = _date.split(_delimiter);
    var monthIndex = formatItems.indexOf("mm");
    var dayIndex = formatItems.indexOf("dd");
    var yearIndex = formatItems.indexOf("yyyy");
    var month = parseInt(dateItems[monthIndex]);
    month -= 1;
    var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
    return formatedDate;
}

function onlyNumberInNumberSort(a, b) {
    a = parseFloat(a);
    b = parseFloat(b);
    if (a < b) return 1;
    if (a > b) return -1;
    return 0;
}
function expiredSorter(a, b) {

    a = parseInt(a.replace('days', ''));
    if (isNaN(a)) {
        a = -1;
    }
    b = parseInt(b.replace('days', ''));
    if (isNaN(b)) {
        b = -1;
    }
    if (a < b) return 1;
    if (a > b) return -1;
    return 0;
}

$.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results) {
        return results[1] || 0;
    } else {
        return null;
    }

};


function vinValidate() {
    $("#vinCheckForm").validate({
        rules: {
            inputCheckVin: {
                required: true,
                minlength: 7,
                maxlength: 17
            }
        },
        messages: {
            inputCheckVin: {
                required: "Please enter VIN",
                minlength: "Please enter the last 7 characters of the VIN",
                maxlength: "VIN incorrect. All VINs have 17 characters."
            }
        }
    });
}

function changeTypeToNumber() {
    $('.changeTypeToNumber ').attr('type', 'number');
}



