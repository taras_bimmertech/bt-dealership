<?php

namespace AppBundle\Form;

use AppBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class EditOrderType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', HiddenType::class, array())
            ->add('createAt', DateTimePickerType::class, array(
                'label' => 'Date',
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'class' => 'form-control input-inline',
                    'data-provide' => 'datepicker',
                    'data-format' => 'dd-mm-yyyy',
                    'placeholder' => 'Date'
                ),
            ))
            ->add('value', NumberType::class, array(
                'label' => 'List value',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'List value',
                    'min' => 0
                )
            ))
            ->add('cost', NumberType::class, array(
                'label' => 'Order cost',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Order cost',
                    'min' => 0
                )
            ))
            ->add('itemNumber', NumberType::class, array(
                'label' => 'Items',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Number of item',
                    'min' => 0
                )
            ));

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\DealerOrder'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_edit_dealer_order';
    }


}
