<?php


namespace AppBundle\Entity;

use DateInterval;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;
//use Symfony\Component\Validator\Constraints\DateTime;
use JMS\Serializer\Annotation as Serializer;

/**
 * Dealer
 *
 * @ORM\Table(name="user_dealer")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserDealerRepository")
 */
class UserDealer extends User
{

//    /**
//     * @var int
//     *
//     * @ORM\Id
//     * @ORM\GeneratedValue
//     * @ORM\Column(type="integer")
//     */
//    protected $id;
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $status;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $company;

    /**
     * @var datetime $createdAt
     *
     * @Serializer\Exclude
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createAt;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $address;


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $zip;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $country;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $state;


    /**
     * @ORM\Column(name="phone", type="string", nullable=true)
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $supportEmail;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $website;
    /**
     *
     * @ORM\Column(type="float", nullable=true)
     * @Assert\Range(
     *      min = 0,
     *      max = 10,
     *      minMessage = "Min % is 0",
     *      maxMessage = "Max % is 10"
     * )
     */
    protected $exchangeFee = 0;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Currency", inversedBy="dealer" , cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id")
     * @Serializer\Exclude
     */
    protected $currency;

    /**
     * @ORM\ManyToOne(targetEntity="UserManager", inversedBy="userDealer", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @Serializer\Exclude
     */
    protected $userManager;


    /**
     * @ORM\OneToMany(targetEntity="DealerOrder", mappedBy="dealer", cascade={"persist","remove"})
     * @ORM\OrderBy({"createAt" = "DESC"})
     * @Serializer\Exclude
     */
    protected $orders;


    /**
     * @ORM\ManyToMany(targetEntity="DownloadFile", inversedBy="dealers",cascade={"persist"})
     * @ORM\OrderBy({"name" = "ASC"})
     * @Serializer\Exclude
     */
    protected $files;

    /**
     * @var datetime $statusDate
     *
     * @Serializer\Exclude
     * @ORM\Column(name="status_date", type="datetime", nullable=true)
     */
    protected $statusDate;


    public function __construct()
    {
        parent::__construct();
        $this->roles[] = 'ROLE_USER';
        $this->status = parent::STATUS_BRONZE;
        $this->createAt = new \DateTime();
        $this->orders = new ArrayCollection();
        $this->files = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getExchangeFee()
    {
        return $this->exchangeFee;
    }

    /**
     * @param string $exchangeFee
     */
    public function setExchangeFee($exchangeFee)
    {
        $this->exchangeFee = $exchangeFee;
    }


    /**
     * @return DateTime
     */
    public
    function getStatusDate()
    {
        return $this->statusDate;
    }

    /**
     * @param DateTime $statusDate
     */
    public
    function setStatusDate($statusDate)
    {
        $this->statusDate = $statusDate;
    }

    /**
     * @return string
     */
    public
    function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public
    function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public
    function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string $company
     */
    public
    function setCompany($company)
    {
        $this->company = $company;
    }


    /**
     * @return string
     */
    public
    function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public
    function setAddress($address)
    {
        $this->address = $address;
    }


    /**
     * @return string
     */
    public
    function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public
    function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public
    function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     */
    public
    function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public
    function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public
    function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public
    function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public
    function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public
    function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public
    function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public
    function getSupportEmail()
    {
        return $this->supportEmail;
    }

    /**
     * @param string $supportEmail
     */
    public
    function setSupportEmail($supportEmail)
    {
        $this->supportEmail = $supportEmail;
    }

    /**
     * @return string
     */
    public
    function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param string $website
     */
    public
    function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     * @return DateTime
     */
    public
    function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * @param DateTime $createAt
     */
    public
    function setCreateAt($createAt)
    {
        $this->createAt = $createAt;
    }

    /**
     * @return mixed
     */
    public
    function getUserManager()
    {
        return $this->userManager;
    }

    /**
     * @param mixed $userManager
     */
    public
    function setUserManager($userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @return mixed
     */
    public
    function getOrders()
    {
        return $this->orders;
    }


    /**
     * @param mixed $orders
     */
    public
    function setOrders($orders)
    {
        $this->orders = $orders;
    }


    /**
     * Add orders
     *
     * @param \AppBundle\Entity\DealerOrder $orders
     *
     * @return UserDealer
     */
    public
    function addOrders(DealerOrder $orders)
    {
        $this->orders[] = $orders;

        return $this;
    }

    /**
     * Remove orders
     *
     * @param \AppBundle\Entity\DealerOrder $orders
     */
    public
    function removeOrders(DealerOrder $orders)
    {
        $this->orders->removeElement($orders);
    }

    /**
     * @return Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }


    /**
     * Add files
     *
     * @param \AppBundle\Entity\DownloadFile $files
     *
     * @return UserDealer
     */
    public
    function addFiles(\AppBundle\Entity\DownloadFile $files)
    {
        $this->files[] = $files;
        return $this;
    }

    /**
     * Remove files
     *
     * @param \AppBundle\Entity\DownloadFile $files
     */
    public
    function removeFiles(\AppBundle\Entity\DownloadFile $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public
    function getFiles()
    {
        return $this->files;
    }

    /**
     * @param mixed $files
     */
    public
    function setFiles($files)
    {
        $this->files = $files;
    }

    public
    function getOrdersTotalSum()
    {
        $totalSum = 0;
        $orders = $this->getOrders();
        foreach ($orders as $order) {
            $totalSum += $order->getCost();
        }
        return $totalSum;
    }


    public function changeStatus()
    {
        $orders = $this->getOrders();
        $statusNot = null;
        if (count($orders) > 0) {
            $oldStatus = $this->getStatus();
            $data = date("Y-m-d H:m:s", strtotime("-60 day"));
            $date = date_create($data);
            $order_count = [];
            /** @var DealerOrder $order */
            foreach ($orders as $order) {
                if ($order->getCreateAt() >= $date and $order->getValue() >= 299) {
                    array_push($order_count, $order);
                }
            }
            $order_count = array_reverse($order_count);
            if (count($order_count) > 0) {
                $maxItemNum = $order_count[0];

                for ($i = 0; $i < count($order_count); $i++) {
                    $countOrd = $this->calcStatusByProd($order_count[$i]->getItemNumber());
                    $countMax = $this->calcStatusByProd($maxItemNum->getItemNumber());

                    if (($countOrd >= $countMax) and ($order_count[$i]->getValue() >= 299)
                        and ($order_count[$i]->getCreateAt() >= $maxItemNum->getCreateAt())
                    ) {
                        $maxItemNum = $order_count[$i];
                    }
                }
                /** @var DealerOrder $byProducts */
                $byProducts = $maxItemNum;
                $byOrders = count($order_count);

                $statusProd = $this->statusCalcByProd($byProducts);
                $statusOrd = $this->statusCalcByOrder($byOrders);

                $newStatus = null;
                if (User::NUMBER_OF_STATUS[$statusProd] > User::NUMBER_OF_STATUS[$statusOrd]) {
                    $this->status = $statusProd;
                    $this->statusDate = clone $byProducts->getCreateAt();
                    $newStatus = $statusProd;

                } else {
                    $this->status = $statusOrd;
//                $this->statusDate = clone $byOrders[count($order_count) - 1]->getCreateAt();
                    $this->statusDate = clone $order_count[count($order_count) - 1]->getCreateAt();
                    $newStatus = $statusOrd;
                }

                $statusNot = $this->statusNotification($oldStatus, $newStatus);
            } else {
                $statusNot = $this->statusNotification($this->status, User::STATUS_BRONZE);
                $this->status = User::STATUS_BRONZE;
                $this->statusDate = null;

            }

        } else {
            $statusNot = $this->statusNotification($this->status, User::STATUS_BRONZE);
            $this->status = User::STATUS_BRONZE;
            $this->statusDate = null;
        }

        return $statusNot;
    }

    public
    function getFutureStatus($data)
    {
        $status = null;
        $orders = $this->getOrders();
        $statusNot = null;
        if (count($orders) > 0) {
            $oldStatus = $this->getStatus();
//            $data = date("Y-m-d H:m:s", strtotime("-50 day"));
            $date = date_create($data);
            $order_count = [];
            /** @var DealerOrder $order */
            foreach ($orders as $order) {
                if ($order->getCreateAt() >= $date and $order->getValue() >= 299) {
                    array_push($order_count, $order);
                }
            }
            $order_count = array_reverse($order_count);
            if (count($order_count) > 0) {
                $maxItemNum = $order_count[0];

                for ($i = 0; $i < count($order_count); $i++) {
                    $countOrd = $this->calcStatusByProd($order_count[$i]->getItemNumber());
                    $countMax = $this->calcStatusByProd($maxItemNum->getItemNumber());
                    if (($countOrd >= $countMax) and ($order_count[$i]->getValue() >= 299)
                        and ($order_count[$i]->getCreateAt() >= $maxItemNum->getCreateAt())
                    ) {
                        $maxItemNum = $order_count[$i];
                    }
                }
                /** @var DealerOrder $byProducts */
                $byProducts = $maxItemNum;
                $byOrders = count($order_count);
                $statusProd = $this->statusCalcByProd($byProducts);
                $statusOrd = $this->statusCalcByOrder($byOrders);
                $newStatus = null;
                if (User::NUMBER_OF_STATUS[$statusProd] > User::NUMBER_OF_STATUS[$statusOrd]) {
                    $status = $statusProd;
                } else {
                    $status = $statusOrd;
                }
            } else {
                $status = User::STATUS_BRONZE;
            }
        } else {
            $status = User::STATUS_BRONZE;
        }

        return $status;
    }


    private function calcStatusByProd($count)
    {
        if ($count >= 5) {
            return 5;
        } elseif ($count >= 3) {
            return 3;
        } elseif ($count >= 2) {
            return 2;
        } else {
            return 1;
        }
    }

    private
    function statusCalcByProd($byProducts)
    {
        if (($byProducts->getItemNumber() >= 5)) {
            return User::STATUS_PLATINUM;
        } elseif (($byProducts->getItemNumber() >= 3)) {
            return User::STATUS_GOLD;
        } elseif (($byProducts->getItemNumber() >= 2)) {
            return User::STATUS_SILVER;
        } else {
            return User::STATUS_BRONZE;
        }
    }

    private
    function statusCalcByOrder($byOrders)
    {
        if (($byOrders >= 7)) {
            return User::STATUS_PLATINUM;
        } elseif (($byOrders >= 4)) {
            return User::STATUS_GOLD;
        } elseif (($byOrders >= 3)) {
            return User::STATUS_SILVER;
        } else {
            return User::STATUS_BRONZE;
        }
    }


    public function calcDealerTitleStatus($statusName)
    {

        if (User::NUMBER_OF_STATUS[$statusName] < User::NUMBER_OF_STATUS[$this->getStatus()]) {
            return "ALREADY SURPASSED";
        } elseif (User::NUMBER_OF_STATUS[$statusName] > User::NUMBER_OF_STATUS[$this->getStatus()]) {
            $orders = $this->getOrders();
            $data = date("Y-m-d H:m:s", strtotime("-60 day"));
            $date = date_create($data);
            $order_count = [];
            /** @var DealerOrder $order */
            foreach ($orders as $order) {
                if ($order->getCreateAt() >= $date and $order->getValue() >= 299) {
                    array_push($order_count, $order);
                }
            }
            $needOrder = User::NUMBER_OF_STATUS[$statusName] - count($order_count);

            return "WITH <b>" . $needOrder . "</b> MORE ORDERS";
        } else {
            return false;
        }
    }

    private
    function statusNotification($oldStatus, $newStatus)
    {
        if ($oldStatus !== $newStatus) {
            /** @var Notification $notification */
            $notification = new Notification();
            $notification->setTitle($this->company);
            $notification->setMessage('Status changed to ' . $newStatus);

            return $notification;
        } elseif ($newStatus === User::STATUS_BRONZE) {
            $this->statusDate = null;
            return null;
        } else {
            return null;
        }
    }

    public
    function calcExpiredTime()
    {
        /** @var DateTime $time */
        $time = $this->statusDate;
        if ($time !== null) {
            $add_date = $time->format('Y-m-d');
            $expiry_date = new DateTime($add_date);
            $expiry_date->modify("+60 days");
            return $expiry_date;
        }
        return null;
    }

    public
    function calcExpiredDate()
    {
        $expiredDate = $this->calcExpiredTime();

        if ($expiredDate != null) {
            $now = new DateTime();
            $expiredTime = $now->diff($expiredDate)->format('%R%a days');

            if ($expiredTime[0] === '-') {
                $expiredTime = ' ';
            } else {
                $pieces = explode('+', $expiredTime);
                $expiredTime = $pieces[1];
            }

            return $expiredTime;
        } else {
            return ' ';
        }
    }
}