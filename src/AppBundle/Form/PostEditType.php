<?php


namespace AppBundle\Form;

use AppBundle\Entity\Post;
use AppBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class PostEditType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        // By default, form fields include the 'required' attribute, which enables
        // the client-side form validation. This means that you can't test the
        // server-side validation errors from the browser. To temporarily disable
        // this validation, set the 'required' attribute to 'false':
        // $builder->add('title', null, ['required' => false, ...]);

        $builder
            ->add('title', null, [
                'attr' => ['autofocus' => true],
                'label' => 'Title',
            ])
//            ->add('summary', TextareaType::class, [
//                'label' => 'label.summary',
//            ])
            ->add('file', FileType::class, array(
                'label' => 'Image ',
                'required' => false,
                'attr' => array(
                    'accept' => 'image/png, image/jpeg, image/gif',
                    'class' => 'photo'
                )
            ))
            ->add('content', null, [
                'attr' => ['rows' => 20],
                'label' => 'Content',
            ])
            ->add('publishedAt', DateTimePickerType::class, [
                'label' => 'Published at',
                'format' => 'dd-MM-yyyy',
            ])
            ->add('type', ChoiceType::class, array(
                'label' => 'Type ',
                'choices' => array(
                    'promotion' => 'promotion',
                    'news' => 'news',
                    'feature' => 'feature'
                )
            ));
    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Post'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_post_edit';
    }
}
